var express     = require('express')
  , bodyParser  = require('body-parser')
  , config      = require('./config/config')
  , database    = require('./helpers/database');

var routes = require('./routes/index');

var app = express();

app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }));
app.use(bodyParser.json({ limit: '50mb' }));



app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
  res.header("X-powered-by", "Tore eida koite hoibo?")
  res.header("Access-Control-Max-Age", "1000");
  next();
});

app.use('/static', express.static(__dirname + '/public'));
app.use(function(req, res, next) {
  console.log(' ');
  console.log(' ');
  console.log(' ');
  console.log('=====================================================================================');
  console.log(req.url, req.method, new Date());
  console.log(req.body);
  console.log('_____________________________________________________________________________________');
  next();
});
app.use('/api', routes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Api Not Found');
  err.status = 404;
  next(err);
});

// error handlers
// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.json({
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.json({
    message: err.message,
    error: {}
  });
});

module.exports = app;
