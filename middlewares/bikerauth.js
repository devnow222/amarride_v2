var jwt           = require('jwt-simple')
  , config        = require('../config/config')
  , database      = require('../helpers/database')
  , errorMessage  = require('../helpers/errormessage')
  , secret        = config.env.secret;

module.exports.simpleauth = function(req, res, next) {
  if(!req.body.phone_num || !req.body.password){
    errorMessage.sendErrorMessage('Please provide all info.', res);
  } else {
    console.log(req.body.phone_num);
    database.query('SELECT * from bikers WHERE phone_num = ?', req.body.phone_num, function(err, biker, fields) {
      if(err) {
        errorMessage.sendErrorMessage('server error', res);
      } else if(biker == '') {
        errorMessage.sendErrorMessage('phone_num error', res);
      } else if(biker[0].loggedIn == 1) {
        errorMessage.sendErrorMessage('already loggedin', res);
      } else if(biker[0].password !== req.body.password) {
        errorMessage.sendErrorMessage('password mismatch', res);
      } else {
        next();
      }
    });
  }
}

module.exports.tokenauth = function(req, res, next) {
  if(!req.body.token){
    errorMessage.sendErrorMessage('Please provide all info.', res);
  } else {
    var token = req.body.token
    var reqBiker = jwt.decode(token, secret);
    database.query('SELECT * from bikers WHERE phone_num = ?', reqBiker.phone_num, function(err, biker, fields) {
      if(err) {
        errorMessage.sendErrorMessage('server error', res);
      } else if(biker == '') {
        errorMessage.sendErrorMessage('phone_num error', res);
      } else if(biker[0].password !== reqBiker.password) {
        errorMessage.sendErrorMessage('password mismatch', res);
      } else {
        next();
      }
    });
  }
}

module.exports.gettoken = function(phone_num, password) {
  var biker = {
    phone_num : phone_num,
    password  : password
  };
  return token = jwt.encode(biker, secret);
}

module.exports.getbiker = function(token) {
  return biker = jwt.decode(token);
}
