var jwt           = require('jwt-simple')
  , md5           = require('md5')
  , config        = require('../config/config')
  , database      = require('../helpers/database')
  , errorMessage  = require('../helpers/errormessage')
  , secret        = config.env.secret;

module.exports.simpleauth = function(req, res, next) {
  console.log('email <---> ', req.body.email);
  if(!req.body.email || !req.body.password){
    errorMessage.sendErrorMessage('Please provide all info.', res);
  } else {
    var password = md5(req.body.password);
    database.query('SELECT * from riders WHERE email = ?', req.body.email, function(err, rider, fields) {
      if(err) {
        errorMessage.sendErrorMessage('server error', res);
      } else if(rider == ''){
        errorMessage.sendErrorMessage('Sorry, AmarRide doesn\'t recognize that email.', res);
      } else if(rider[0].password !== password){
        errorMessage.sendErrorMessage('The password that you\'ve entered is incorrect.', res);
      } else {
        next();
      }
    });
  }
}

module.exports.tokenauth = function(req, res, next) {
	
  // if(!req.body.token){
  //   errorMessage.sendErrorMessage('Please provide all info.', res);
  // } else {
  //   var token = req.body.token;
  //   var reqRider = jwt.decode(token, secret);
  //   database.query('SELECT * from riders WHERE email = ?', reqRider.email, function(err, rider, fields) {
  //     if(err) {
  //       errorMessage.sendErrorMessage('server error', res);
  //     } else if(rider == ''){
  //       errorMessage.sendErrorMessage('Invalid credentials. Please logout and login again.', res);
  //     } else if(rider[0].password !== reqRider.password){
  //       errorMessage.sendErrorMessage('Invalid credentials. Please logout and login again.', res);
  //     } else {
  //       next();
  //     }
  //   });

      
  // }
   next();
	
	 
}

module.exports.gettoken = function(email, password) {
  var rider = {
    email: email,
    password: md5(password)
  }
  return token = jwt.encode(rider, secret);
}

module.exports.getrider = function(token) {
  return rider = jwt.decode(token);
}
