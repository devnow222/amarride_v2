var Constants = require('./constants');
var request = require('request');
var parseString = require('xml2js').parseString;

function Sender(options) {
  if (!(this instanceof Sender) && options) {
    this.options = options;
    return new Sender(options);
  }
}

Sender.prototype.send = function(options, receiver, message, callback) {
  if (typeof(message) == 'function') {
    callback = message;
    message = null;
  } else if (typeof(receiver) == 'function') {
    callback = receiver;
    receiver = undefined;
  } else if (typeof(options) == 'function') {
    callback = options;
    options = undefined;
  } else if (!callback) {
    callback = function() {};
  }

  if (!options) {
    callback('Unauthorized (401). Credentials are not set.');
  } else if(!receiver) {
    callback('Receiver not set.');
  } else {
    options.addNewOption('receiver', receiver);
    options.addNewOption('message', message);
    options.addNewOption('randomNumber', (new Date()).getTime());

    var body = options.toJson();
    var sslwireless_uri = 'https://'+Constants.SSLWIRELESS_ADDRESS+'/'+Constants.SSLWIRELESS_CONNECTOR+'/'+Constants.SSLWIRELESS_ENDPATH;
    var contentType = body['Content-Type'] ? body['Content-Type'] : 'json';
    makeRequest(sslwireless_uri, body, contentType, callback);
  }
}

function makeRequest(uri, body, contentType, callback) {
  var request_options = {
    method: 'POST',
    uri: uri,
    formData: body,
    json: true
  };
  request(request_options, function (err, res, resBody) {
    if (err) {
      return callback(err);
    }
    if(contentType == 'json') {
      parseString(resBody, {trim: true}, function (err, result) {
        resBody = result;
        return callback(null, result.REPLY);
      });
    }
    callback(null, resBody);
  });
}

module.exports = Sender;
