var Constants = {
  'SSLWIRELESS_ADDRESS' : 'sms.sslwireless.com',
  'SSLWIRELESS_CONNECTOR' : 'pushapi',
  'SSLWIRELESS_ENDPATH' : 'dynamic/server.php',
  'SSLWIRELESS_DEFALUT_URI' : 'http://sms.sslwireless.com/pushapi/dynamic/server.php'
}

module.exports = Constants;
