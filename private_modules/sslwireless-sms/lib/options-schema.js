/**
 * This module defines all the arguments that may be passed to a request as request params.
 *
 * Each argument may contain a field `__argName`, if the name of the field
 * should be different when sent to the server.
 *
 * The argument may also contain a field `__argType`, if the given
 * argument must be of that type. The types are the strings resulting from
 * calling `typeof <arg>` where `<arg>` is the argument.
 *
 * Other than that, the arguments are expected to follow the indicated
 * structure.
 */

module.exports = {
  username: {
    __argName: "user",
    __argType: "string"
  },
  password: {
    __argName: "pass",
    __argType: "string"
  },
  serviceId: {
    __argName: "sid",
    __argType: "string"
  },
  randomNumber : {
    __argName: "sms[0][2]",
    __argType: "number"
  },
  receiver: {
    __argName: "sms[0][0]",
    __argType: "string"
  },
  message: {
    __argName: "sms[0][1]",
    __argType: "string"
  },
  responseType: {
    __argName: 'Content-Type',
    __argType: "string"
  }
};
