# node-bkash

The goal of this project is provide the best and most easy way to use bKash Platform Connector (Real Time Pull Service) in node.js environment.

See the [official bKash Platform Connector documentation](http://mymensinghttc.gov.bd/public/upload/routine/routinefile.pdf) for more information.

We are currently working on version 1.0.0 of the project, and it is available in an early alpha version.

## Installation

```bash
$ npm install node-bkash --save
```

## Requirements

This library provides the server-side implementation of bKash Connector Api.

You need to get a **merchant account** from [bKash](https://www.bkash.com/).

## Example application

According to below **Usage** reference, we could create such application:

```js
var bkash = require('node-bkash');

var options = new bkash.Options({
    username: 'username',
    password: 'password',
    merchantNumber: '01XXX-XXXXXX',
    responseType: 'json'
});

// Set up the Connector, prepare your transactionId.
var connector = new bkash.Connector();

connector.checkByTransactionId(options, transactionId, function (err, response) {
    if(err) console.error(err);
    else console.log(response);
});
```

## Usage

```js
var bkash = require('node-bkash');

// Create options
// ... with blank value
var options = new bkash.Options();

// ... or some given values
var options = new bkash.Options({
    username: 'username',
    password: 'password',
    merchantNumber: '01XXX-XXXXXX',
    responseType: 'json'
});

// Change the options data
// ... as key-value
options.addNewOption('username','username');
options.addNewOption('password','password');

// Set up the Connector
var connector = new bkash.Connector();

// Check transaction status using transaction id
connector.checkByTransactionId(options, transactionId, function(err, response) {
  if(err) console.error(err);
  else console.log(response);
});

// ... or using reference
connector.checkByReference(options, reference, function (err, response) {
  if(err) console.error(err);
  else console.log(response);
});

// ... or using lastPullTime
connector.checkByLastPullTime(options, lastPullTime, function (err, response) {
  if(err) console.error(err);
  else console.log(response);
});
```
## Options

You have to provide information for the following keys in options:

|Key|Type|Required|Description|
|---|---|---|---|
|```username```|String|true|bKash merchant account username.|
|```password```|String|true|bKash merchant account password.|
|```merchantNumber```|String|true|11 digits mobile number which is to be verified whether it is eligible for remittance or not.|
|```responseType```|String|false|Indicates type of response you want to get. You can chose either 'json' or 'xml'. The default value is 'json'.|

If you do not provide the required fields an `Error` object will be returned to your callback.

## Transaction Checking

You can check a transaction in either of the way bellow. You will have to provide information for the field required for a particular checking.

|Check|Required field|Description|
|---|---|---|---|
|```checkByTransactionId()```|```transactionId``` _(string)_|bKash merchant account username.|
|```checkByReference()```|```reference``` _(string)_|bKash merchant account password.|
|```checkByLastPullTime()```|```lastPullTime``` _(string)_|bKash merchant account number.|

If you do not provide the required field it will be taken as _null_.

## Response Fields

|Field Name|Type|Description|
|---|---|---|
|```trxId ```|Numeric|Unique transaction reference generated by bKash platform upon successful transaction. (ex: 5652845)|
|```trxStatus```|String|Status of the transaction. (ex: 0000)|
|```reversed```|String|Is the transaction reversed. (ex: 0)|
|```service```|String|bKash service type, in this case: Payment. (ex: Payment)|
|```sender```|Numeric|Customer bKash Account No. (ex: 01717171717)|
|```receiver```|Numeric|Merchant bKash Account No. (ex: 01818181818)|
|```currency```|String|Currency format. (ex: BDT)|
|```amount```|Decimal|Transaction amount. (ex: 1999.50)|
|```reference```|String|Reference input by Customer when doing transaction. (ex: abc)|
|```counter```|Numeric|Merchant’s counter no. (ex: 1)|
|```trxTimestamp```|Timestamp|Transaction timestamp. (ex: 2012-04-25 19:06:35.0)|

## Transaction Status Codes

|Code|Message|Interpretation|
|---|---|---|
|0000 |trxID is valid and transaction is successful.|Transaction Successful|
|0010, 0011|trxID is valid but transaction is in pending state.|Transaction Pending|
|0100|trxID is valid but transaction has been reversed.|Transaction Reversed|
|0111|trxID is valid but transaction has failed.|Transaction Failure|
|1001|Invalid MSISDN input. Try with correct mobile no.|Format Error|
|1002|Invalid trxID, it does not exist.|Invalid Reference|
|1003|Access denied. Username or Password is incorrect.|Authorization Error|
|1004|Access denied. trxID is not related to this username.|Authorization Error|
|9999|Could not process request.|System Error|

## Donate

 Bitcoin: [13iTQf7tDhrKgibw2Y3U5SyPJa7R8sQmHQ](https://blockchain.info/address/13iTQf7tDhrKgibw2Y3U5SyPJa7R8sQmHQ)
