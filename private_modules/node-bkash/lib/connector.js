var Constants = require('./constants');
var request = require('request');

function Connector() {
  if (!(this instanceof Connector)) {
    return new Connector();
  }
}

Connector.prototype.checkByTransactionId = function(options, transactionId, callback) {
  if (typeof(transactionId) == 'function') {
    callback = transactionId;
    transactionId = null;
  } else if (typeof(options) == 'function') {
    callback = options;
    options = undefined;
  } else if (!callback) {
    callback = function() {};
  }

  if (!options) {
    callback('Unauthorized (401). Credentials are not set.');
  } else {
    options.addNewOption('transactionId', transactionId);

    if (options.params.reference) {
      options.deleteOption('reference');
    }
    if (options.params.lastPullTime) {
      options.deleteOption('lastPullTime');
    }

    var body = options.toJson();
    var bkash_uri = 'https://'+Constants.BKASH_ADDRESS+'/'+Constants.BKASH_CONNECTOR+'/'+Constants.BKASH_ENDPATH+'/'+Constants.BKASH_REQUEST_SENDMSG;
    var contentType = body['Content-Type'] || 'json';

    makeRequest(bkash_uri, body, contentType, callback);
  }
}

Connector.prototype.checkByReference = function(options, reference, callback) {
  if (typeof(reference) == 'function') {
    callback = reference;
    reference = null;
  } else if (typeof(options) == 'function') {
    callback = options;
    options = undefined;
  } else if (!callback) {
    callback = function() {};
  }

  if (!options) {
    callback('Unauthorized (401). Credentials are not set.');
  } else {
    options.addNewOption('reference', reference);

    if (options.params.transactionId) {
      options.deleteOption('transactionId');
    }
    if (options.params.lastPullTime) {
      options.deleteOption('lastPullTime');
    }

    var body = options.toJson();
    var bkash_uri = 'https://'+Constants.BKASH_ADDRESS+'/'+Constants.BKASH_CONNECTOR+'/'+Constants.BKASH_ENDPATH+'/'+Constants.BKASH_REQUEST_REFMSG;
    var contentType = body['Content-Type'] || 'json';

    makeRequest(bkash_uri, body, contentType, callback);
  }
}

Connector.prototype.checkByLastPullTime = function(options, lastPullTime, callback) {
  if (typeof(lastPullTime) == 'function') {
    callback = lastPullTime;
    lastPullTime = null;
  } else if (typeof(options) == 'function') {
    callback = options;
    options = undefined;
  } else if (!callback) {
    callback = function() {};
  }

  if (!options) {
    callback('Unauthorized (401). Credentials are not set.');
  } else {
    options.addNewOption('lastPullTime', lastPullTime);

    if (options.params.transactionId) {
      options.deleteOption('transactionId');
    }
    if (options.params.reference) {
      options.deleteOption('reference');
    }

    var body = options.toJson();
    var bkash_uri = 'https://'+Constants.BKASH_ADDRESS+'/'+Constants.BKASH_CONNECTOR+'/'+Constants.BKASH_ENDPATH+'/'+Constants.BKASH_REQUEST_PULLMSG;
    var contentType = body['Content-Type'] || 'json';

    makeRequest(bkash_uri, body, contentType, callback);
  }
}

function makeRequest(uri, body, contentType, callback) {
  var acceptType = 'application/json';
  if (contentType == 'xml') {
    acceptType = 'application/xml';
  }
  var request_options = {
    method: 'POST',
    headers: { 'Accept': acceptType },
    uri: uri,
    json: body
  };
  request(request_options, function (err, res, resBody) {
    if (err) {
      return callback(err);
    }
    callback(null, resBody);
  });
}

module.exports = Connector;
