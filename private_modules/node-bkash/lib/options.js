var checkOptions = require("./options-structure");

function Options(raw) {
  if (!(this instanceof Options)) {
    return new Options(raw);
  }
  this.params = cleanParams(raw || {});
}

function cleanParams(raw) {
  var params = {};
  Object.keys(raw).forEach(function(param) {
    if(checkOptions[param]) {
      params[param] = raw[param];
    }
  });
  return params;
}

Options.prototype.addNewOption = function() {
  if(arguments.length == 2) {
    if(!this.params[arguments[0]]) {
      this.params[arguments[0]] = '';
    }
    return this.params[arguments[0]] = arguments[1];
  }
  throw new Error("Invalid number of arguments given to for setting " + arguments[0] + " (" + arguments.length + ")");
};

Options.prototype.deleteOption = function() {
  if(arguments.length == 1) {
    if(!this.params[arguments[0]]) {
      return;
    }
    return delete this.params[arguments[0]];
  }
  throw new Error("Invalid number of arguments given to for setting " + arguments[0] + " (" + arguments.length + ")");
};

Options.prototype.toJson = function() {
  var json = {};

  Object.keys(this.params).forEach(function(param) {
    var optionDescription = checkOptions[param];
    if(!optionDescription) {
      return;
    }
    var key = optionDescription.__argName || param;
    json[key] = this.params[param];
  }.bind(this));

  return json;
};

module.exports = Options;
