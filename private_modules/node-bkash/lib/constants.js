var Constants = {
  'BKASH_ADDRESS' : 'www.bkashcluster.com:9081',
  'BKASH_CONNECTOR' : 'dreamwave',
  'BKASH_ENDPATH' : 'merchant/trxcheck',
  'BKASH_REQUEST_SENDMSG' : 'sendmsg',
  'BKASH_REQUEST_REFMSG' : 'refmsg',
  'BKASH_REQUEST_PULLMSG' : 'periodicpullmsg',
  'BKASH_DEFALUT_URI' : 'https://www.bkashcluster.com:9081/dreamwave/merchant/trxcheck/sendmsg'
}

module.exports = Constants;
