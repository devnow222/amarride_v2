var gulp             = require('gulp')
  , gulpLoadPlugins  = require('gulp-load-plugins')
  , path             = require('path')
  , del              = require('del')
  , merge            = require('merge-stream')
  , exec             = require('child_process').exec
  , deploy_path      = require('./config/config').env.deploy_path
  , deploy_path_test = require('./config/config').env.deploy_path_test
  , restart_id       = require('./config/config').env.restart_id;

var plugins = gulpLoadPlugins();

var paths = {
  privateModules: ['private_modules/**'],
  publicFiles: ['./public/**'],
  jsFiles: ['./**/*.js', '!routes/junk/**', '!src/**', '!node_modules/**', '!private_modules/**', '!gulpfile.js'],
  nonJsFiles: ['./package.json', '!.env']
};

gulp.task('clean', function() {
  return del('src/**');
});

gulp.task('copy', ['clean'], function() {
  var privateModules = gulp.src(paths.privateModules)
                           .pipe(plugins.newer('src'))
                           .pipe(gulp.dest('src/private_modules'));
  var publicFiles = gulp.src(paths.publicFiles)
                        .pipe(gulp.dest('src/public'));
  var jsFiles = gulp.src(paths.jsFiles)
                    .pipe(gulp.dest('src'));
  var nonJsFiles = gulp.src(paths.nonJsFiles)
                       .pipe(gulp.dest('src'));
  return merge(privateModules, publicFiles, jsFiles, nonJsFiles);
});

gulp.task('deploy', ['copy'], function () {
  return exec('rsync -r ./src/ ' + deploy_path, function (err) {
    console.log(deploy_path);
    if(err) {
      console.log(err);
    } else {
      console.log('rsync done...');
      exec('ssh root@188.166.180.131 pm2 restart ' + restart_id, function (err) {
        if(err) console.log(err);
        console.log('restart done...');
      });
    }
  });
});

gulp.task('deploytest', ['copy'], function () {
  return exec('rsync -r ./src/ ' + deploy_path_test, function (err) {
    console.log(deploy_path_test);
    if(err) {
      console.log(err);
    } else {
      console.log('rsync done...');
      exec('ssh root@139.59.37.169 pm2 restart ' + restart_id, function (err) {
       if(err) console.log(err);
       console.log('restart done...');
     });
    }
  });
});

gulp.task('default', ['deploy'], function() {
  console.log('all task done...');
});
