module.exports.sendErrorMessage = function(err, res) {
  console.log("Error: " + err);
  res.json({
    status: 'error',
    message: err
  });
}
