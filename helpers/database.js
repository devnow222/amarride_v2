var mysql = require('mysql')
  , db    = require('../config/dbconfig');

var connection = mysql.createConnection({
  host     : db.env.host,
  user     : db.env.user,
  password : db.env.password,
  database : db.env.database
});

connection.connect();

module.exports = connection;