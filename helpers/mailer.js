var nodemailer    = require('nodemailer')
  , config        = require('../config/config')
  , email_receipt = config.env.mailer_email_receipt
  , pass_receipt  = config.env.mailer_pass_receipt
  , email_info    = config.env.mailer_email_info
  , pass_info     = config.env.mailer_pass_info

var transporter_receipt = nodemailer.createTransport({
  service: 'Gmail',
  auth: {
    user: email_receipt,
    pass: pass_receipt
  }
});

var transporter_noreply = nodemailer.createTransport({
  service: 'Gmail',
  auth: {
    user: email_info,
    pass: pass_info
  }
});

function getMailBodyInHtml(tripInfo){
  return "<html><body><div style='width: 600px; background: white; border: 1px solid green; padding: 20px; height: 450px;'><div style='text-align:center'><i>Thanks for Choosing Amarride, <strong>"
  + tripInfo.rider.name +
  "</strong></i></div><hr style='width:600px;'><div style='width:280px;float:left'><div style='height:250px;border:1px solid red'><img src="
  + tripInfo.trip.screenshot_url +
  " width='278' height='250'></div><div style='width:280px;float:left'><p><img src='http://amarride.com/amarride/img/pick.png' width='13px;float:left'>"
  + tripInfo.trip.trip_start +
  "<br><span style='padding-left: 13px;'>"
  + tripInfo.trip.from_area +
  "</span></p><p><img src='http://amarride.com/amarride/img/drop.png' width='13px;float:left'>"
  + tripInfo.trip.trip_end +
  "<br><span style='padding-left: 13px;'>"
  + tripInfo.trip.to_area +
  "</span></p></div></div><div style='width:300px;float:right;padding:10px'><table><tr><td style='width:200px;'>Base Fare: </td><td style='width:100px;text-align:right'><strong>"
  + tripInfo.trip.base_price +
  "</strong> Tk</td></tr><tr><td style='width:200px;'>Cost Per Min: </td>  <td style='width:100px;text-align:right'><strong>"
  + tripInfo.trip.cost_per_min +
  "</strong> Tk</td></tr><tr><td style='width:200px;'>Trip Time: </td><td style='width:100px;text-align:right'><strong>"
  + tripInfo.trip.total_trip_time +
  "</strong></td></tr><tr><td colspan='2'><hr></td></tr><tr><td style='width:200px;'>Surge Fare: </td><td style='width:50px;text-align:right'> <strong>"
  + tripInfo.trip.surge_factor +
  "</strong> x</td></tr><tr><td colspan='2'><hr></td></tr><tr><td style='width:200px;'>Total Bill: </td><td style='width:100px;text-align:right'><strong>"
  + tripInfo.trip.total_bill +
  "</strong> Tk</td></tr></table></div><div style='clear:both'></div><div style='background:green;color:white;text-align:center;padding:5px'><p>Copyright @ amarride.com</p></div></div></body></html>";
}

function getMailBodyInText(tripInfo) {
  return "From: " + tripInfo.trip.from_area + " | To: " + tripInfo.trip.to_area + " | Cost Per Min:" + tripInfo.trip.cost_per_min + "Tk. | Trip Time: " + tripInfo.trip.total_trip_time + " | Surge Price: " + tripInfo.trip.surge_factor + " | Total Bill: " + tripInfo.trip.total_bill;
}

module.exports.sendMail = function(recipients, tripInfo, fn){
  mailOptions = {
    from: '"AmarRide" <reciept@amarride.com>',
    to: recipients,
    subject: "AmarRide Bill - user's copy",
    text: getMailBodyInText(tripInfo),
    html: getMailBodyInHtml(tripInfo)
  }

  transporter_receipt.sendMail(mailOptions, function(err, info) {
    fn(err, info)
  });
}



function getPasswordBodyInHtml(name, password) {
  return "<html><body><div style='width: 600px; background: white; border: 1px solid green; padding: 20px; height: 450px;'><div style='text-align:center'><i>Thanks for Choosing Amarride, <strong>"
  + name +
  "</strong></i></div><hr style='width:600px;'><div><p>Hey there,</p><p>Can't remember your password, hun? It happens to the best of us.</p><p>Here is your new password :</p><div style='border: 1px solid red; float:left'><h3 style='color:green; padding-left: 10px; padding-right: 10px'>"
  + password +
  "</h3></div><div style='clear:both'></div><br><br><i>note: use this password to login and change it to something you'll remember.</i><br><br><p>Yours,</p><p>Amarride Team</p></div><div style='clear:both'></div><div style='background:green; color:white;text-align:center ; padding:5px;'><p>Copyright @ amarride.com</p></div></div></body></html>"
}

function getPasswordBodyInText(password) {
  return "Your new password for Amarride is : '" + password + "'. Use this password to login and change it to something you'll remember."
}

module.exports.sendPassword = function(recipients, name, password, fn) {
  mailOptions = {
    from: '"AmarRide" <no-reply@amarride.com>',
    to: recipients,
    subject: "AmarRide - reset password",
    text: getPasswordBodyInText(password),
    html: getPasswordBodyInHtml(name, password)
  }

  transporter_noreply.sendMail(mailOptions, function(err, info) {
    fn(err, info)
  });
}


module.exports.sendTestMail = function(recipients, fn){
  mailOptions = {
    from: '"AmarRide" <reciept@amarride.com>',
    to: recipients,
    subject: "AmarRide Bill - user's copy",
    text: "this is a test text",
    html: "this is a test html"
  }

  transporter_receipt.sendMail(mailOptions, function(err, info) {
    fn(err, info);
  });
}
