module.exports.stringToDate = function(s) {
  var dateParts = s.split(' ')[0].split('-');
  var timeParts = s.split(' ')[1].split(':');
  var d = new Date(dateParts[0], --dateParts[1], dateParts[2]);
  d.setHours(timeParts[0], timeParts[1], timeParts[2])
  return d
}

module.exports.dateFormat = function(datetime) {
  var now     = new Date(datetime)
    , year    = now.getFullYear()
    , month   = now.getMonth()+1
    , day     = now.getDate()
    , hour    = now.getHours()
    , minute  = now.getMinutes()
    , second  = now.getSeconds();

  var month = (month.toString().length == 1 ? '0' + month : month);
  var day = (day.toString().length == 1 ? '0' + day : day);
  var hour = (hour.toString().length == 1 ? '0' + hour : hour);
  var minute = (minute.toString().length == 1 ? '0' + minute : minute);
  var second = (second.toString().length == 1 ? '0' + second : second);

  var formatedDateTime = year+'-'+month+'-'+day+' '+hour+':'+minute+':'+second;
  return formatedDateTime;
}

module.exports.getDateTime = function(s) {
  var now     = s ? new Date(s) : new Date()
    , year    = now.getFullYear()
    , month   = now.getMonth()+1
    , day     = now.getDate()
    , hour    = now.getHours()
    , minute  = now.getMinutes()
    , second  = now.getSeconds();

  var month = (month.toString().length == 1 ? '0' + month : month);
  var day = (day.toString().length == 1 ? '0' + day : day);
  var hour = (hour.toString().length == 1 ? '0' + hour : hour);
  var minute = (minute.toString().length == 1 ? '0' + minute : minute);
  var second = (second.toString().length == 1 ? '0' + second : second);

  var dateTime = year+'-'+month+'-'+day+' '+hour+':'+minute+':'+second;
  return dateTime;
}

module.exports.getTime = function() {
  var now     = new Date()
    , hour    = now.getHours()
    , minute  = now.getMinutes()
    , second  = now.getSeconds();

  var hour = (hour.toString().length == 1 ? '0' + hour : hour);
  var minute = (minute.toString().length == 1 ? '0' + minute : minute);
  var second = (second.toString().length == 1 ? '0' + second : second);

  var time = hour+':'+minute+':'+second;
  return time;
}

module.exports.getHourMinuteOnly = function(datetime) {
  var timeParts = datetime.split(':');
  var hourMinute;
  if(timeParts[0] >= 12)
    return hourMinute = (parseInt(timeParts[0]) - 12) + ':' + timeParts[1] + ' PM';
  else
    return hourMinute = timeParts[0] + ':' + timeParts[1] + ' AM';
}

module.exports.getDateOnly = function(datetime) {
    var now     = new Date(datetime)
      , year    = now.getFullYear()
      , month   = now.getMonth()+1
      , day     = now.getDate();

    var month = (month.toString().length == 1 ? '0' + month : month);
    var day = (day.toString().length == 1 ? '0' + day : day);

    var date = day + '-' + month + '-' + year;
    return date;
}

var stringToTime = function(s) {
  var timeParts = s.split(' ')[0].split(':');
  t.setHours(timeParts[0], timeParts[1], timeParts[2])

  return t
}

module.exports.timeDifference = function(date1, date2) {
   //var date1 = new Date("08/05/2015 23:41:20");
  // var date2 = new Date("08/06/2015 02:56:32");
  date1 = new Date(date1);
  date2 = new Date(date2);
  var diff = date2.getTime() - date1.getTime();

  var msec = diff;
  var hh = Math.floor(msec / 1000 / 60 / 60);
  msec -= hh * 1000 * 60 * 60;
  var mm = Math.floor(msec / 1000 / 60);
  mm = mm < 10 ? '0' + mm.toString() : mm;
  msec -= mm * 1000 * 60;
  var ss = Math.floor(msec / 1000);
  ss = ss < 10 ? '0' + ss.toString() : ss;
  msec -= ss * 1000;

  oDiff = hh + ":" + mm + ":" + ss;

  return oDiff;
}

module.exports.modifyDate = function(date) {
  var date_string = date.toString();
  var date_array = date_string.split(':');

  var hh = parseInt(date_array[0]);
  var mm = date_array[1];
  var ss = date_array[2];

  modifiedDate = hh.toString() + ":" + mm + ":" + ss;

  return modifiedDate;
}

module.exports.getMinutesOnly = function(date) {
  var date_string = date.toString();
  var date_array = date_string.split(':');

  var hh = (parseInt(date_array[0]) * 60);
  var mm = parseInt(date_array[1]);
  var ss = 1;

  modifiedDate = hh + mm + ss;

  return modifiedDate;
}




      