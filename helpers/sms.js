var Promise = require('bluebird')
  , sms     = require('../private_modules/sslwireless-sms/lib/sslwireless-sms')
  , config  = require('../config/config');

var options = new sms.Options({
  username: config.env.sslwireless_user,
  password: config.env.sslwireless_pass,
  serviceId: config.env.sslwireless_sid,
  responseType: 'json'
});

var Sender = new sms.Sender(options);

module.exports.send = function(receiver, message, fn) {
  return new Promise(function (resolve, reject) {
    Sender.send(options, receiver, message, function(err, response) {
      if(err) reject('failed to send sms.');
      else if(response.PARAMETER && response.PARAMETER[0] == 'OK') resolve(response);
      else reject('failed to send sms');
    });
  });
}

function getPasswordBody(password) {
  return "Your new password for AmarRide is: " + password + ". Use this password to login and change it to something you'll remember."
}

module.exports.sendPassword = function(receiver, password) {
  return new Promise(function (resolve, reject) {
    var message = getPasswordBody(password);
    Sender.send(options, receiver, message, function(err, response) {
      if(err) reject('failed to send sms.');
      else if(response.PARAMETER && response.PARAMETER[0] == 'OK') resolve(response);
      else reject('failed to send sms');
    });
  });
}

function getVerificationBody(code) {
  return "AmarRide verification code: " + code;
}

module.exports.sendVerification = function(receiver, code) {
  return new Promise(function (resolve, reject) {
    var message = getVerificationBody(code);
    Sender.send(options, receiver, message, function(err, response) {
      if(err) reject('failed to send sms.');
      else if(response.PARAMETER && response.PARAMETER[0] == 'OK') resolve(response);
      else reject('failed to send sms');
    });
  });
}

module.exports.sendBikerBillInfo = function(receiver, totalBill, tatalTime) {
  return new Promise(function (resolve, reject) {
    var message = 'The bill of your current trip is BDT ' + totalBill + ' for ' + tatalTime + ' mins.'
    Sender.send(options, receiver, message, function(err, response) {
      if(err) reject('failed to send sms.');
      else if(response.PARAMETER && response.PARAMETER[0] == 'OK') resolve(response);
      else reject('failed to send sms');
    });
  });
}

module.exports.sendRiderBillInfo = function(receiver, riderName, totalBill, totalTime) {
  return new Promise(function (resolve, reject) {
    var message = 'Dear ' + riderName + ', your bill for AmarRide  is BDT' + totalBill + ' for ' + totalTime + ' mins.'
    Sender.send(options, receiver, message, function(err, response) {
      if(err) reject('failed to send sms.');
      else if(response.PARAMETER && response.PARAMETER[0] == 'OK') resolve(response);
      else reject('failed to send sms');
    });
  });
}

module.exports.sendBulk = function(receiver, text) {
  return new Promise(function (resolve, reject) {
    Sender.send(options, receiver, text, function(err, response) {
      if(err) reject('failed to send sms.');
      else if(response.PARAMETER && response.PARAMETER[0] == 'OK') resolve(response);
      else reject('failed to send sms');
    });
  });
}
