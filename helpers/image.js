var Promise = require('bluebird')
  , fs      = require('fs')
  , random  = require('./random');

module.exports.saveRiderImage = function(image) {
  return new Promise(function (resolve, reject) {
    var imageExtention = 'jpg';
    if(image.charAt(0)=='/'){
      imageExtention = 'jpeg';
    }else if(image.charAt(0)=='R'){
      imageExtention = 'gif';
    }else if(image.charAt(0)=='i'){
      imageExtention = 'png';
    }

    var imageName = random.imageName() + '.' + imageExtention;
    var path = __dirname + '/../public/rider/profile/' + imageName;
    image = image.replace(/ /g,'+');

    fs.writeFile(path, image, 'base64', function(err) {
      if(err) reject('failed to save image');
      else resolve(imageName);
    });
  });
}

module.exports.saveBikerImage = function(image) {
  return new Promise(function (resolve, reject) {
    var imageExtention = image.match(/^data:image\/(.*);/);
    var imageName = random.imageName() + '.' + imageExtention[1];
    var path = __dirname + '/../public/biker/profile/' + imageName;
    image = image.replace(/^(.*)base64,/, "");

    fs.writeFile(path, image, 'base64', function(err) {
      if(err) reject('failed to save image');
      else resolve(imageName);
    });
  });
}

module.exports.saveNidImage = function(image) {
  return new Promise(function (resolve, reject) {
    var imageExtention = image.match(/^data:image\/(.*);/);
    var imageName = random.imageName() + '.' + imageExtention[1];
    var path = __dirname + '/../public/biker/nid/' + imageName;
    image = image.replace(/^(.*)base64,/, "");

    fs.writeFile(path, image, 'base64', function(err) {
      if(err) reject('failed to save image');
      else resolve(imageName);
    });
  });
}

module.exports.deleteImage = function(image) {
  var path = __dirname + '/../public/rider/profile/' + image;
  return new Promise(function (resolve, reject) {
    fs.unlink(path, function(err) {
      if(err) reject('failed to delete image');
      else resolve(image);
    });
  });
}
