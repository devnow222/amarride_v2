module.exports.password = function() {
  var pass = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz#$@0123456789";

  for( var i = 0; i < 6; i++ )
      pass += possible.charAt(Math.floor(Math.random() * possible.length));

  return pass;
}

module.exports.imageName = function() {
  function randomString() {
    return Math.floor((1 + Math.random()) * 0x1000000)
      .toString(16)
      .substring(1);
  }
  return randomString() + randomString() + randomString();
}

module.exports.verificationCode = function() {
  var pass = "";
  var possible = "0123456789";

  for( var i = 0; i < 6; i++ )
      pass += possible.charAt(Math.floor(Math.random() * possible.length));

  return pass;
}
