var gcm     = require('node-gcm')
  , config  = require('../config/config')
  , request = require('request')
  , pushKey = config.env.push_key
  , sender  = new gcm.Sender(config.env.push_key);

var message = new gcm.Message({
  collapseKey: 'AmarRide',
  priority: 'high',
  contentAvailable: true,
  delayWhileIdle: false,
  timeToLive: 1200,
  dryRun: false,
  data: {},
  notification: {
    priority: 'high',
    title: 'AmarRide',
    icon: 'ic_launcher',
    sound: 'default',
    time_to_live: '2419200',
    delay_while_idle: 'false',
    content_available: 'true',
    body: 'You have a notification from AmarRide'
  }
});

var getBody = function(state, messages) {
  var body = '';
  switch (state) {
    case 'Request':
      var from_area = 'here';
      var to_area = '';
      if(messages.from_area) {
        var from_area_array = messages.from_area.split(',');
        if(from_area_array[1])
          from_area = from_area_array[0] + ', ' + from_area_array[1];
        else if(from_area_array[0])
          from_area = from_area_array[0];
      }
      if(messages.to_area) {
        var to_area_array = messages.to_area.split(',');
        if(to_area_array[1])
          to_area = to_area_array[0] + ', ' + to_area_array[1];
        else if(to_area_array[0])
          to_area = to_area_array[0];
      }
      body = 'Trip request!!  From: ' + from_area + ' || To: ' + to_area;
      break;
    case 'Arriving':
      body = 'Your request has been accepted.';
      break;
    case 'Arrived':
      body = 'Your ride has arrived.';
      break;
    case 'Started':
      body = 'Your trip has started.';
      break;
    case 'Ended':
      body = 'Your trip just ended.';
      break;
    case 'Busy':
      body = 'Sorry! the drivers are busy. Please try again later.';
      break;
    case 'Cancelled':
      body = 'Sorry! the trip has been cancelled.';
      break;
    case 'Warning':
      body = 'Warning! You are riding without a passenger.';
      break;
    case 'Update':
      body = 'Your destination has been changed to ' + messages.to_area;
      break;
    case 'Location':
      body = 'Biker location problem...sending old location!';
      break;
    case 'Success':
      body = messages;
      break;
    default:
      body = 'You have a notification from AmarRide';
  }
  return body;
}

module.exports.sendNotification = function(reg_tokens, state, messages, fn) {
  message.addData('message', messages);
  message.addData('state', state);

  var body = getBody(state, messages);
  message.addNotification('body', body);

  switch (state) {
    case 'Arriving':
      message.addNotification('sound', 'busy.mp3');
      break;
    case 'Arrived':
      message.addNotification('sound', 'busy.mp3');
      break;
    default:
      message.addNotification('sound', 'default');
  }

  sender.send(message, { registrationTokens: reg_tokens }, 10,  function (err, response) {
    if(err) {
      // console.log(err);
      fn(err, response);
    }
    else {
      // console.log(response);
      fn(err, response);
    }
  });
}

module.exports.forceNotification = function(reg_tokens, state, messages, fn) {
  var body = getBody(state, messages)
  var options = {
    registration_ids: reg_tokens,
    priority: 'high',
    'data': {
      title: 'AmarRide',
      state: state,
      body: body,
      message: messages
    }
  };

  var request_options = {
    method: 'POST',
    preambleCRLF: true,
    postambleCRLF: true,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'key=' + pushKey
    },
    uri: 'https://fcm.googleapis.com/fcm/send',
    json: options
  };

  request(request_options, function(error, response, resBodyJSON) {
    console.log(request_options);
    if(error) {
      // console.log(error)
      fn(error, resBodyJSON);
    } else {
      // console.log(resBodyJSON);
      fn(error, resBodyJSON);
    }
  });
}

module.exports.bulkNotification = function(reg_tokens, messages) {
  message.addData('message', messages);
  message.addNotification('body', messages);

  sender.send(message, { registrationTokens: reg_tokens }, 10,  function (err, response) {
    console.log(err);
    console.log(response);
  });
}
