var gcm     = require('node-gcm')
  , config  = require('../config/config')
  , sender  = new gcm.Sender(config.env.push_key);

var message = new gcm.Message({
  // collapseKey: 'AmarRide',
  // priority: 'high',
  // contentAvailable: true,
  // delayWhileIdle: false,
  // timeToLive: 2419200,
  // dryRun: false,
  data: {
    content_available: 'true',
  },
  // notification: {
  //   priority: 'high',
  //   title: 'AmarRide',
  //   icon: 'ic_launcher',
  //   sound: 'busy.mp3',
  //   time_to_live: '2419200',
  //   //content_available: '1',
  //   delay_while_idle: 'false',
  //   content_available: 'true',
  //   body: 'You have a notification from AmarRide',
  //   click_action: '',
  //   tag: ''
  // }
});

function updateMessage(body, click_action, tag) {
  message.addNotification('body', body);
  message.addNotification('click_action', click_action);
  message.addNotification('tag', tag);
}

function getBody(state, messages) {
  var body = '';
  switch (state) {
    case 'Request':
      var from_area = 'here';
      var to_area = '';
      if(messages.from_area) {
        var from_area_array = messages.from_area.split(',');
        if(from_area_array[1])
          from_area = from_area_array[0] + ', ' + from_area_array[1];
        else if(from_area_array[0])
          from_area = from_area_array[0];
      }
      if(messages.to_area) {
        var to_area_array = messages.to_area.split(',');
        if(to_area_array[1])
          to_area = to_area_array[0] + ', ' + to_area_array[1];
        else if(to_area_array[0])
          to_area = to_area_array[0];
      }
      body = 'Trip request!!  From: ' + from_area + ' || To: ' + to_area;
      break;
    case 'Arriving':
      body = 'Your request has been accepted.', 'BikerProfileActivity', 'trip';
      break;
    case 'Arrived':
      body = 'Your ride has arrived.', 'BikerProfileActivity', 'trip';
      break;
    case 'Started':
      body = 'Your trip has started.', 'BikerProfileActivity', 'trip';
      break;
    case 'Ended':
      body = 'Your trip just ended.', 'TripSummeryActivity', 'trip';
      break;
    case 'Busy':
      body = 'Sorry! the drivers are busy. Please try again later.', '', 'trip';
      break;
    case 'Cancelled':
      body = 'Sorry! the trip has been cancelled.', '', 'trip';
      break;
    case 'Warning':
      body = 'Warning! You are riding without a passenger.';
      break;
    case 'Update':
      body = messages;
      break;
    case 'Success':
      body = messages;
      break;
    default:
      body = 'You have a notification from AmarRide';
  return body;
  }
}

module.exports.forceNotification = function(reg_tokens, state, messages, fn) {
  var options = {
    registration_ids: [reg_tokens],
    priority: 'high',
    'data': {
      title: 'AmarRide',
      state: state,
      body: getBody(state, messages),
      message: message
    }
  }

  var request_options = {
    method: 'POST',
    preambleCRLF: true,
    postambleCRLF: true,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'key=' + sender
    },
    uri: 'https://fcm.googleapis.com/fcm/send',
    json: options
  }

  request(request_options, function(error, response, resBodyJSON) {
    if(error) fn(error, resBodyJSON);
    else fn(error, resBodyJSON);
  });
}

module.exports.sendNotification = function(reg_tokens, state, messages, fn) {
  message.addData('message', messages);
  message.addData('state', state);

  switch (state) {
    case 'Request':
      var from_area = 'here';
      var to_area = '';
      if(messages.from_area) {
        var from_area_array = messages.from_area.split(',');
        if(from_area_array[1])
          from_area = from_area_array[0] + ', ' + from_area_array[1];
        else if(from_area_array[0])
          from_area = from_area_array[0];
      }
      if(messages.to_area) {
        var to_area_array = messages.to_area.split(',');
        if(to_area_array[1])
          to_area = to_area_array[0] + ', ' + to_area_array[1];
        else if(to_area_array[0])
          to_area = to_area_array[0];
      }
      updateMessage(('Trip request!!  From: ' + from_area + ' || To: ' + to_area), 'RequestActivity', '');
      // message.addNotification('body', 'Trip request!!  From: ' + from_area + ' || To: ' + to_area);
      // message.addNotification('click_action', 'RequestActivity');
      // message.addNotification('tag', 'trip');
      break;
    case 'Arriving':
      updateMessage('Your request has been accepted.', 'BikerProfileActivity', 'trip');
      // message.addNotification('body', 'Your request has been accepted.');
      // message.addNotification('click_action', 'BikerProfileActivity');
      // message.addNotification('tag', 'trip');
      break;
    case 'Arrived':
      updateMessage('Your ride has arrived.', 'BikerProfileActivity', 'trip');
      // message.addNotification('body', 'Your ride has arrived.');
      // message.addNotification('click_action', 'BikerProfileActivity');
      // message.addNotification('tag', 'trip');
      break;
    case 'Started':
      updateMessage('Your trip has started.', 'BikerProfileActivity', 'trip');
      // message.addNotification('body', 'Your trip has started.');
      // message.addNotification('click_action', 'BikerProfileActivity');
      // message.addNotification('tag', 'trip');
      break;
    case 'Ended':
      updateMessage('Your trip just ended.', 'TripSummeryActivity', 'trip');
      // message.addNotification('body', 'Your trip just ended.');
      // message.addNotification('click_action', 'TripSummeryActivity');
      // message.addNotification('tag', 'trip');
      break;
    case 'Busy':
      updateMessage('Sorry! the drivers are busy. Please try again later.', '', 'trip');
      // message.addNotification('body', 'All drivers are busy. Please try again later.');
      // message.addNotification('click_action', 'CancelledActivity');
      // message.addNotification('tag', 'trip');
      break;
    case 'Cancelled':
      updateMessage('Sorry! the trip has been cancelled.', '', 'trip');
      // message.addNotification('body', 'Sorry! the trip has been cancelled.');
      // message.addNotification('click_action', 'CancelledActivity');
      // message.addNotification('tag', 'trip');
      break;

    // case 'Cancelled':
    //   updateMessage('Sorry! the trip has been cancelled.', 'CancelledActivity', 'trip');
    //   // message.addNotification('body', 'Sorry! the trip has been cancelled.');
    //   // message.addNotification('click_action', 'CancelledActivity');
    //   // message.addNotification('tag', 'trip');
    //   break;
    case 'Warning':
      updateMessage('Warning! You are riding without a passenger.', '', '');
      // message.addNotification('body', 'Warning! You are riding without a passenger.');
      // message.addNotification('click_action', '');
      // message.addNotification('tag', '');
      break;
    case 'Update':
      updateMessage(messages, '', '');
      // message.addNotification('body', messages);
      // message.addNotification('click_action', '');
      // message.addNotification('tag', '');
      break;
    case 'Success':
      updateMessage(messages, '', '');
      // message.addNotification('body', messages);
      // message.addNotification('click_action', '');
      // message.addNotification('tag', '');
      break;
    default:
      updateMessage('You have a notification from AmarRide', '', '');
      // message.addNotification('body', 'You have a notification from AmarRide');
      // message.addNotification('click_action', '');
      // message.addNotification('tag', '');
  }

  sender.send(message, { registrationTokens: reg_tokens }, 10,  function (err, response) {
    if(err) fn(err, response);
    else fn(err, response);
  });
}
