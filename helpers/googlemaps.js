var Promise    = require('bluebird')
  , GoogleMaps = require('googlemaps')
  , datetime   = require('./datetime')
  , config     = require('../config/config');

var publicConfig = {
  key: config.env.api_key,
  stagger_time: 1000,
  encode_polylines: false,
  secure: true
};

var publicConfigDistance = {
  key: config.env.distance_key,
  stagger_time: 1000,
  encode_polylines: false,
  secure: true
};

var gmAPI = new GoogleMaps(publicConfig);
var gmAPI_Distance = new GoogleMaps(publicConfigDistance);

module.exports.getDistanceAndTime = function(from_area, to_area){
  var params = {
    origins: from_area,
    destinations: to_area
  };

  return new Promise(function (resolve, reject) {
    gmAPI_Distance.distance(params, function(err, distanceValue) {
      if(err) reject('Error');
      else resolve(distanceValue);
    });
  });
};


module.exports.getImage = function(fn){
  gmAPI.staticMap(params, function(err, binaryImage) {
    // fetch asynchronously the binary image
    fn(binaryImage);
  });
};

module.exports.getImagelink = function(start, end, trip_lat_long){
  var params = {
    size: '500x400',
    maptype: 'roadmap',
    markers: [{
      location: start,
      label   : 'From',
      color   : 'green',
      icon    : 'http://188.166.180.131:4152/static/markers/start_point.png',
      shadow  : true
    }, {
      location: end,
      label   : 'to',
      color   : 'red',
      icon    : 'http://188.166.180.131:4152/static/markers/end_point.png',
      shadow  : true
    }],
    style: [{
      "featureType": "all",
      "stylers": [{
        "saturation": 0
      }, {
        "hue": "#e7ecf0"
      }]
    }, {
      "featureType": "road",
      "stylers": [{
        "saturation": -70
      }]
    }, {
      "featureType": "transit",
      "stylers": [{
        "visibility": "off"
      }]
    }, {
      "featureType": "poi",
      "stylers": [{
        "visibility": "off"
      }]
    }, {
      "featureType": "water",
      "stylers": [{
        "visibility": "simplified"
      }, {
        "saturation": -60
      }]
    }],
    path: [{
      color: 'red',//'0x0000ff',
      weight: '3',
      geodesic: true,
      points: trip_lat_long
    }]
  };
  return gmAPI.staticMap(params);
};
