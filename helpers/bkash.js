var Promise  = require('bluebird')
  , bkash  = require('../private_modules/node-bkash/lib/bkash')
  , config = require('../config/config');

var options = new bkash.Options({
  username: config.env.bkash_user,
  password: config.env.bkash_pass,
  merchantNumber: config.env.bkash_merchant,
  responseType: 'json'
});

var Connector = new bkash.Connector();

module.exports.checkTransaction = function(transactionId, fn) {
  return new Promise(function (resolve, reject) {
    Connector.checkByTransactionId(options, transactionId, function(err, response) {
      var transaction = response.transaction;
      console.log(transaction);
      if(err) reject('failed to connect bkash');
      else if(transaction.trxStatus == 0010 || transaction.trxStatus == 0011) reject('Your transaction is pending. Please try after some time.');
      else if(transaction.trxStatus == 0100) reject('Your transection has been reversed.');
      else if(transaction.trxStatus == 0111) reject('Your transection has been failed. Please sending money again.');
      else if(transaction.trxStatus != 0000) reject('Invalid transaction ID. Please try using a valid transaction ID.');
      else resolve(transaction);
    });
  });
}
