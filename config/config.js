require('dotenv').config();

module.exports.env = {
  base_url             : process.env.BASE_URL,
  user_profile         : process.env.USER_PROFILE,
  biker_profile        : process.env.BIKER_PROFILE,
  secret               : process.env.SECRET,
  api_key              : process.env.API_KEY,
  distance_key         : process.env.DISTANCE_KEY,
  push_key             : process.env.PUSH_KEY,
  mailer_email_receipt : process.env.MAILER_EMAIL_RECEIPT,
  mailer_pass_receipt  : process.env.MAILER_PASS_RECEIPT,
  mailer_email_info    : process.env.MAILER_EMAIL_INFO,
  mailer_pass_info     : process.env.MAILER_PASS_INFO,
  bkash_user           : process.env.BKASH_USER,
  bkash_pass           : process.env.BKASH_PASS,
  bkash_merchant       : process.env.BKASH_MERCHANT,
  sslwireless_user     : process.env.SSLWIRELESS_USER,
  sslwireless_pass     : process.env.SSLWIRELESS_PASS,
  sslwireless_sid      : process.env.SSLWIRELESS_SID,
  deploy_path          : process.env.DEPLOY_PATH,
  deploy_path_test     : process.env.DEPLOY_PATH_TEST,
  restart_id           : process.env.RESTARTID
}
