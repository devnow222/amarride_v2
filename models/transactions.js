var Promise  = require('bluebird')
  , database = require('../helpers/database');


module.exports.add = function(value) {
  return new Promise(function (resolve, reject) {
    database.query('INSERT INTO transactions SET ?', value, function(err, result) {
      if(err) reject('server error');
      else resolve(result);
    });
  });
};

module.exports.findWithTransactionId = function(transactionId) {
  return new Promise(function(resolve, reject) {
    database.query('SELECT * FROM transactions WHERE transaction_id = ? limit 1', transactionId, function(err, transaction) {
      if(err) reject('server error');
      else if(transaction == '') reject('transaction_not_found');
      else resolve(transaction);
    });
  })
};
