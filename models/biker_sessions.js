var Promise  = require('bluebird')
  , database = require('../helpers/database');

  module.exports.add = function(value) {
    return new Promise(function (resolve, reject) {
      database.query('INSERT INTO biker_sessions SET ?', value, function(err, result) {
        if(err) reject('server error');
        else resolve(result);
      });
    });
  };
  
  module.exports.updateDuration = function(update, biker_id) {
	  return new Promise(function (resolve, reject) {
	   var query= database.query('UPDATE biker_sessions SET biker_sessions.lastrequest_duration= ?-biker_sessions.proceed_timestamp WHERE biker_sessions.biker_id in ( SELECT bikers.id from bikers  WHERE bikers.id= ? AND (bikers.current_status=0 OR bikers.current_status=1))  ORDER BY biker_sessions.proceed_timestamp DESC LIMIT 1 ', [update, biker_id], function(err, result) {
	      if(err) reject(err);
	      else resolve(result);
	    });
	  });
	};