var Promise  = require('bluebird')
  , database = require('../helpers/database');

module.exports.add = function(value) {
  return new Promise(function (resolve, reject) {
    database.query('INSERT INTO ratings SET ?', value, function(err, result) {
      if(err) reject('server error');
      else resolve(result);
    });
  });
};

module.exports.findWithBikerId = function(biker_id) {
  return new Promise(function (resolve, reject) {
    database.query('SELECT * FROM ratings WHERE biker_id = ? and who_rated = ?', [biker_id, 'Rider'], function(err, ratings) {
      if(err) reject('server error');
      else resolve(ratings);
    });
  });
};

module.exports.findWithRiderId = function(rider_id) {
  return new Promise(function (resolve, reject) {
    database.query('SELECT * FROM ratings WHERE rider_id = ? and who_rated = ?', [rider_id, 'Biker'], function(err, ratings) {
      if(err) reject('server error');
      else resolve(ratings);
    });
  });
};
