var Promise  = require('bluebird')
  , database = require('../helpers/database');

module.exports.add = function(value) {
  return new Promise(function (resolve, reject) {
    database.query('INSERT INTO deposits SET ?', value, function(err, result) {
      if(err) reject('server error');
      else resolve(result);
    });
  });
};

module.exports.findWithWalletId = function(wallet_id) {
  return new Promise(function(resolve, reject) {
    database.query('SELECT created, amount FROM deposits WHERE wallet_id = ?', wallet_id, function(err, deposit) {
      if(err) reject('server error');
      else resolve(deposit);
    });
  })
};
