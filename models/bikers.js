var Promise  = require('bluebird')
  , database = require('../helpers/database');

  module.exports.add = function(value) {
    return new Promise(function (resolve, reject) {
      database.query('INSERT INTO bikers SET ?', value, function(err, result) {
        if(err) reject('server error');
        else resolve(result);
      });
    });
  };

module.exports.findWithBikerId = function(biker_id) {
  return new Promise(function (resolve, reject) {
    database.query('SELECT * FROM bikers WHERE id = ? limit 1', biker_id, function(err, biker) {
      if(err) reject(err);
      else if(biker == '') reject('biker not found');
      else resolve(biker);
    });
  });
};

module.exports.updateWithBikerId = function(update, biker_id) {
  return new Promise(function (resolve, reject) {
    database.query('UPDATE bikers SET ? WHERE id = ?', [update, biker_id], function(err, result) {
      if(err) reject('server error');
      else resolve(result);
    });
  });
};

module.exports.findWithPhone = function(phone_num) {
  return new Promise(function (resolve, reject) {
    database.query('SELECT * from bikers WHERE phone_num = ?', phone_num, function(err, biker) {
      if(err) reject('server error');
      else if(biker == '') reject('biker not found');
      else resolve(biker);
    });
  });
};

module.exports.updateWithPhone = function(update, phone_num) {
  return new Promise(function (resolve, reject) {
    database.query('UPDATE bikers SET ? WHERE phone_num = ?', [update, phone_num], function(err, result) {
      if(err) reject(err);
      else resolve(result);
    });
  });
};

module.exports.findAllAvailable = function(type) {
  return new Promise(function (resolve, reject) {
    database.query('SELECT * from bikers WHERE type = ? and loggedIn = 1 and current_status = 0', type, function(err, biker) {
      if(err) reject('server error');
      else resolve(biker);
    });
  });
};

module.exports.idleActiveBiker = function(update, biker_ids) {
	  return new Promise(function (resolve, reject) {
		database.query('UPDATE bikers SET ? WHERE id IN (?)', [update, biker_ids ], function(err, result) {
	      if(err) reject('server error');
	      else resolve(result);
	    });
	    });
	};



