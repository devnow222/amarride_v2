var Promise  = require('bluebird')
  , database = require('../helpers/database');

module.exports.add = function(value) {
  return new Promise(function (resolve, reject) {
    database.query("INSERT INTO trips SET ?", value, function(err, result) {
      console.log('err: ', err)
      console.log('result: ', result);
      if(err) reject('server error');
      else resolve(result);
    });
  });
};

module.exports.findWithTripId = function(trip_id) {
  return new Promise(function (resolve, reject) {
    database.query('SELECT * from trips WHERE id = ?', trip_id, function(err, trip) {
      if(err) reject('server error');
      if(trip == '') reject('trip not found');
      else resolve(trip);
    });
  });
};

module.exports.findWithRiderId = function(rider_id) {
  return new Promise(function (resolve, reject) {
    database.query('SELECT * from trips WHERE rider_id = ?', rider_id, function(err, trip) {
      if(err) reject('server error');
      else resolve(trip);
    });
  });
};

module.exports.deleteWithRiderId = function(rider_id) {
  return new Promise(function (resolve, reject) {
    database.query('DELETE FROM trips WHERE rider_id = ? and (trip_status = ? or trip_status = ?)', [rider_id, 'Arriving', 'Arrived'], function (err, result) {
      if(err) reject('server error');
      else resolve(result);
    });
  });
};

module.exports.findWithBikerId = function(biker_id) {
  return new Promise(function (resolve, reject) {
    database.query('SELECT * from trips WHERE biker_id = ? limit 1', biker_id, function(err, trip) {
      if(err) reject('server error');
      if(trip == '') reject('trip not found');
      else resolve(trip);
    });
  });
};

module.exports.updateWithBikerId = function(update, biker_id) {
  return new Promise(function (resolve, reject) {
    database.query('UPDATE trips SET ? WHERE biker_id = ?', [update, biker_id], function(err, result) {
      if(err) reject('server error');
      else resolve(result);
    });
  });
};

module.exports.deleteWithBikerId = function(biker_id) {
  return new Promise(function (resolve, reject) {
    database.query('DELETE FROM trips WHERE biker_id = ?', biker_id, function (err, result) {
      if(err) reject('server error');
      else resolve(result);
    });
  });
};

module.exports.findPreviousWithRiderId = function(rider_id) {
  return new Promise(function (resolve, reject) {
    database.query('SELECT * from trips WHERE rider_id = ?', rider_id, function(err, trip) {
      if(err) reject('server error');
      else if(trip == '') reject('trip_not_found');
      else resolve(trip);
    });
  });
}

module.exports.findPreviousWithBikerId = function(biker_id) {
  return new Promise(function (resolve, reject) {
    database.query('SELECT * from trips WHERE biker_id = ?', biker_id, function(err, trip) {
      if(err) reject('server error');
      else if(trip == '') reject('trip_not_found');
      else resolve(trip);
    });
  });
}

module.exports.findWithTemptripId = function(temptrip_id) {
  return new Promise(function (resolve, reject) {
    database.query('SELECT * from trips WHERE temptrip_id = ? limit 1', temptrip_id, function(err, trip) {
      if(err) reject('server error');
      if(trip == '') reject('trip_not_found');
      else resolve(trip);
    });
  });
}
