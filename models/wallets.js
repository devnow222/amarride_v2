var Promise  = require('bluebird')
  , database = require('../helpers/database');

module.exports.add = function(value) {
  return new Promise(function (resolve, reject) {
    database.query('INSERT INTO wallets SET ?', value, function(err, result) {
      if(err) reject('server error');
      else resolve(result);
    });
  });
};

module.exports.findWithRiderId = function(rider_id) {
  return new Promise(function(resolve, reject) {
    database.query('SELECT * FROM wallets WHERE rider_id = ? limit 1', rider_id, function(err, wallet) {
      if(err) reject('server error');
      else if(wallet == '') reject('wallet not found');
      else resolve(wallet);
    });
  });
};

module.exports.updateWithRiderId = function(update, rider_id) {
  return new Promise(function(resolve, reject) {
    database.query('UPDATE wallets SET ? WHERE rider_id = ?', [update, rider_id], function(err, result) {
      if(err) reject('server error');
      else resolve(result);
    });
  });
};
