var Promise  = require('bluebird')
  , database = require('../helpers/database');

module.exports.findWithPromo = function(promocode) {
  return new Promise(function(resolve, reject) {
    database.query('SELECT * FROM promos WHERE code = ? limit 1', promocode, function(err, promo) {
      if(err) reject('server error');
      else if(promo == '') reject('Invalid promocode');
      else if(promo[0].active != 1) reject('Promocode already used');
      else {
        var currentDate = Date.parse(new Date());
        if(Date.parse(promo[0].end) < currentDate) reject('Promocode outdated');
        else resolve(promo);
      }
    });
  });
};

module.exports.updateWithPromo = function(update, promocode) {
  return new Promise(function(resolve, reject) {
    database.query('UPDATE promos SET ? WHERE code = ?', [update, promocode], function (err, result) {
      if(err) reject('server error');
      else resolve(result);
    });
  });
};
