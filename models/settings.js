var Promise  = require('bluebird')
  , database = require('../helpers/database');

module.exports.findAll = function() {
  return new Promise(function (resolve, reject) {
    database.query('SELECT * from settings limit 1', function(err, settings) {
      if(err) reject(err);
      else if(settings == '') reject('settings not found');
      else resolve(settings);
    });
  });
};
