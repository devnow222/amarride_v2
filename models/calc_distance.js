var Promise  = require('bluebird')
  , database = require('../helpers/database');

module.exports.add = function(value) {
  return new Promise(function (resolve, reject) {
    database.query('INSERT INTO calculate_distance SET ?', value, function(err, result) {
      console.log(err);
      console.log(result);
      if(err) reject('server error');
      else resolve(result);
    });
  });
};
