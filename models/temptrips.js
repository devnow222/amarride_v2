var Promise  = require('bluebird')
  , database = require('../helpers/database');

module.exports.add = function(value) {
  return new Promise(function (resolve, reject) {
    database.query('INSERT INTO temptrips SET ?', value, function(err, result) {
      if(err) reject('server error');
      else resolve(result);
    });
  });
};

module.exports.updateWithRiderId = function(update, rider_id) {
  return new Promise(function (resolve, reject) {
    database.query('UPDATE temptrips SET ? WHERE rider_id = ?', [update, rider_id], function (err, result) {
      if(err) reject('server error');
      else resolve(result);
    });
  });
};

module.exports.deleteWithRiderId = function(rider_id) {
  return new Promise(function (resolve, reject) {
    database.query('DELETE FROM temptrips WHERE rider_id = ?', rider_id, function (err, result) {
      if(err) reject('server error');
      else if(result.affectedRows <= 0) reject('not deleted');
      else resolve(result);
    });
  });
};

module.exports.findWithBikerId = function(biker_id) {
  return new Promise(function (resolve, reject) {
    database.query('SELECT * from temptrips WHERE biker_id = ? limit 1', biker_id, function(err, temptrips) {
      if(err) reject('server error');
      else if(temptrips == '') reject('temptrip error');
      else resolve(temptrips);
    });
  });
};

module.exports.findWithTemptripId = function(temptrip_id) {
  return new Promise(function (resolve, reject) {
    database.query('SELECT * from temptrips WHERE id = ? limit 1', temptrip_id, function(err, temptrips) {
      if(err) reject('server error');
      else if(temptrips == '') reject('temptrip error');
      else resolve(temptrips);
    });
  });
};

module.exports.findWithRiderId = function(rider_id) {
  return new Promise(function (resolve, reject) {
    database.query('SELECT * from temptrips WHERE rider_id = ? limit 1', rider_id, function(err, temptrips) {
      if(err) reject('server error');
      else if(temptrips == '') reject('temptrip error');
      else resolve(temptrips);
    });
  });
};
