var Promise  = require('bluebird')
  , database = require('../helpers/database');

module.exports.findWithBikerId = function(biker_id) {
  return new Promise(function(resolve, reject) {
    database.query('SELECT * FROM rides WHERE biker_id = ? limit 1', biker_id, function(err, ride) {
      if(err) reject('server error');
      else if(ride == '') reject('rides not found');
      else resolve(ride);
    });
  })
};
