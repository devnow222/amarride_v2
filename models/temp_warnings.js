var Promise  = require('bluebird')
  , database = require('../helpers/database');

module.exports.add = function(value) {
  return new Promise(function (resolve, reject) {
    database.query('INSERT INTO temp_warnings SET ?', value, function(err, result) {
      if(err) reject('server error');
      else resolve(result);
    });
  });
};

module.exports.findWithTemptripId = function(temptrip_id) {
  return new Promise(function (resolve, reject) {
    database.query('SELECT * FROM temp_warnings WHERE temptrip_id = ?', temptrip_id, function(err, temp_warnings) {
      if(err) reject('server error');
      else if(temp_warnings == '') reject('no temp_warning found');
      else resolve(temp_warnings);
    });
  });
};

module.exports.updateWithTemptripId = function(update, temptrip_id) {
  return new Promise(function(resolve, reject) {
    database.query('UPDATE temp_warnings SET ? WHERE temptrip_id = ?', [update, temptrip_id], function(err, result) {
      if(err) reject('server error');
      else resolve(result);
    });
  });
};

module.exports.deleteWithTemptripId = function(temptrip_id) {
  return new Promise(function (resolve, reject) {
    database.query('DELETE FROM temp_warnings WHERE temptrip_id = ?', temptrip_id, function (err, result) {
      if(err) reject('server error');
      else resolve(result);
    });
  });
};
