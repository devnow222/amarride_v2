var Promise  = require('bluebird')
  , database = require('../helpers/database');

module.exports.add = function(value) {
  return new Promise(function (resolve, reject) {
    database.query('INSERT INTO riders SET ?', value, function(err, result) {
      if(err) reject('server error');
      else resolve(result);
    });
  });
};

module.exports.findAll = function() {
  return new Promise(function (resolve, reject) {
    database.query('SELECT * from riders', function(err, rider) {
      if(err) reject(err);
      else if(rider == '') reject('rider not found');
      else resolve(rider);
    });
  });
};

module.exports.findWithEmail = function(email) {
  return new Promise(function (resolve, reject) {
    database.query('SELECT * from riders WHERE email = ? limit 1', email, function(err, rider) {
      if(err) reject(err);
      else if(rider == '') reject('rider not found');
      else resolve(rider);
    });
  });
};

module.exports.updateWithEmail = function(update, email) {
  return new Promise(function (resolve, reject) {
    database.query('UPDATE riders SET ? WHERE email = ?', [update, email], function(err, result) {
      if(err) reject(err);
      else resolve(result);
    });
  });
};

module.exports.findWithId = function(id) {
  return new Promise(function (resolve, reject) {
    database.query('SELECT * from riders WHERE id = ? limit 1', id, function(err, rider) {
      if(err) reject(err);
      else if(rider == '') reject('rider not found');
      else resolve(rider);
    });
  });
};

module.exports.updateWithId = function(update, id) {
  return new Promise(function (resolve, reject) {
    database.query('UPDATE riders SET ? WHERE id = ?', [update, id], function(err, result) {
      if(err) reject(err);
      else resolve(result);
    });
  });
};

module.exports.findWithPhone = function(phone_num) {
  return new Promise(function (resolve, reject) {
    database.query('SELECT * from riders WHERE phone_num = ? limit 1', phone_num, function(err, rider) {
      if(err) reject(err);
      else if(rider == '') reject('rider not found');
      else resolve(rider);
    });
  });
};
