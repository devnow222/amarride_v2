var Promise  = require('bluebird')
  , database = require('../helpers/database');


module.exports.add = function(value) {
  return new Promise(function(resolve, reject) {
    database.query('INSERT INTO success_trips SET ?', value, function(err, result) {
      if(err) reject('server error');
      else resolve(result);
    });
  });
};

module.exports.findAll = function(rider_id) {
  return new Promise(function(resolve, reject) {
    database.query('SELECT * FROM success_trips', function(err, successTrips) {
      if(err) reject('server error');
      else resolve(successTrips);
    });
  });
};

module.exports.findWithRiderId = function(rider_id) {
  return new Promise(function(resolve, reject) {
    database.query('SELECT * FROM success_trips WHERE rider_id = ?', rider_id, function(err, successTrips) {
      if(err) reject('server error');
      else resolve(successTrips);
    });
  });
};

module.exports.findWithTripId = function(trip_id) {
  return new Promise(function(resolve, reject) {
    database.query('SELECT * FROM success_trips WHERE trip_id = ? limit 1', trip_id, function(err, successTrips) {
      if(err) reject('successTrips error');
      else if(successTrips == '') reject('history not found');
      else resolve(successTrips);
    });
  });
};

module.exports.updateWithTripId = function(update, trip_id) {
  return new Promise(function(resolve, reject) {
    database.query('UPDATE success_trips SET ? WHERE trip_id = ?', [update, trip_id], function (err, result) {
      if(err) reject('server error');
      else resolve(result);
    });
  });
};

module.exports.findFavWithRiderId = function(rider_id) {
  return new Promise(function(resolve, reject) {
    database.query('SELECT * FROM success_trips WHERE favid = ? and rider_id = ?', ['1', rider_id], function(err, successTrips) {
      if(err) reject('server error');
      else resolve(successTrips);
    });
  });
};

module.exports.deleteFavWithTripId = function(trip_id) {
  return new Promise(function(resolve, reject) {
    database.query('UPDATE success_trips SET favid = 0 WHERE trip_id = ?', trip_id, function (err, result) {
      if(err) reject('server error');
      else resolve(result);
    });
  });
};

module.exports.findPendingWithRiderId = function(rider_id) {
  return new Promise(function(resolve, reject) {
    database.query('SELECT * FROM success_trips WHERE rider_id = ? ORDER BY id DESC limit 1', rider_id, function(err, successTrips) {
      if(err) reject('server error');
      else if(successTrips == '') reject('trip not found');
      else if(successTrips[0].rating_id > -1) reject('no pending rating');
      else resolve(successTrips);
    });
  })
};

module.exports.findPendingWithBikerId = function(biker_id) {
  return new Promise(function(resolve, reject) {
    database.query('SELECT * FROM success_trips WHERE biker_id = ? ORDER BY id DESC limit 1', biker_id, function(err, successTrips) {
      if(err) reject('server error');
      else if(successTrips == '') reject('SuccessTrip not found');
      else if(successTrips[0].total_paid > 0) reject('no pending bills');
      else resolve(successTrips);
    });
  })
};

module.exports.findDateSuccessTrip = function(biker_id,datetime) {

	  return new Promise(function(resolve, reject) {
	   var query= database.query('SELECT COUNT(id) AS today_trips FROM success_trips WHERE DATE(created) = DATE(?) AND biker_id = ?',[datetime,biker_id], function(err, successTrips) {
	      if(err) reject(err);
	      else resolve(successTrips);
	    });
  
	  });
	};


module.exports.findMonthSuccessTrip = function(biker_id,datetime) {

    return new Promise(function(resolve, reject) {
     var query= database.query('SELECT COUNT(id) AS month_trips FROM success_trips WHERE MONTH(created) = MONTH(?) AND YEAR(created)=YEAR(?) AND biker_id = ?',[datetime,datetime,biker_id], function(err, successTrips) {
        if(err) reject(err);
        else resolve(successTrips);
      });
     
    });
  };

