var Promise  = require('bluebird')
  , database = require('../helpers/database');

module.exports.add = function(value) {
  return new Promise(function (resolve, reject) {
    database.query('INSERT INTO request_poles SET ?', value, function(err, result) {
      if(err) reject('server error');
      else resolve(result);
    });
  });
};
