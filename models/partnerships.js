var Promise  = require('bluebird')
  , database = require('../helpers/database');

module.exports.add = function(value) {
  return new Promise(function (resolve, reject) {
    database.query('INSERT INTO partnerships SET ?', value, function(err, result) {
      if(err) reject('server error');
      else resolve(result);
    });
  });
};

module.exports.findMatch = function(phone_num) {
  return new Promise(function (resolve, reject) {
    database.query('SELECT * from partnerships WHERE phone_num = ? limit 1', phone_num, function(err, rider) {
      if(err) reject(err);
      else if(rider == '') reject('rider not found');
      else resolve(rider);
    });
  });
};
