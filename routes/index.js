var express  = require('express')
  , bikers   = require('./bikers/bikers')
  , riders   = require('./riders/riders')
  , webviews = require('./webviews/webviews')
  , locals  = require('./locals/locals')
  , tests    = require('./tests/tests')
  , router   = express.Router();

// local Routes for '/api'
router.use('/biker', bikers);
router.use('/rider', riders);
router.use('/webview', webviews);
router.use('/locals', locals);
router.use('/test', tests);

// local Routes for '/api'
router.get('/', function(req, res){
  res.json({
    greetings: 'Welcome to AmarRide V2'
  });
});

module.exports = router;
