var express       = require('express')
  , md5           = require('md5')
  , dateFormat    = require('dateformat')
  , database      = require('../../helpers/database')
  , datetime      = require('../../helpers/datetime')
  , distance      = require('../../helpers/distance')
  , errorMessage  = require('../../helpers/errormessage')
  , googlemaps    = require('../../helpers/googlemaps')
  , mailer        = require('../../helpers/mailer')
  , push          = require('../../helpers/push')
  , image         = require('../../helpers/image')
  , biker_push    = require('../../helpers/biker_push')
  , riderauth     = require('../../middlewares/riderauth')
  , router        = express.Router();


router.get('/', function(req, res) {
  res.json({
    message: 'testing api'
  });
});


/*--------------------------------- Testing --------------------------------*/
router.get('/getCurrentDate', function(req, res){
  var currentDate = datetime.getDateTime();
  var currentRealDate = new Date();
  var newtime = dateFormat(currentRealDate, "yyyy-mm-dd h:MM:ss");
  res.json({
    currentDate: currentDate,
    currentRealDate: currentRealDate,
    newtime: newtime
  });
});

router.get('/getTime', function(req, res) {

  // var tripStartTime = "2016-07-10 01:10:00";
  // var tripEndTime = "2016-07-10 01:13:15";
  //
  //
  // tripStartTime = dateFormat(tripStartTime, "yyyy-mm-dd h:MM:ss");
  // tripEndTime = dateFormat(tripEndTime, "yyyy-mm-dd h:MM:ss");
  //
  // var totalTripTime = (stringToDate(tripEndTime) - stringToDate(tripStartTime)) / 60000;
  //
  // var subTotalBill = 3.5 * totalTripTime;
  // var totalBill = 35 + subTotalBill;

  var differ = datetime.timeDifference("2016-07-10 01:10:00", "2016-07-10 01:13:15");

  res.json({
    // tripStartTime: tripStartTime,
    // tripEndTime: tripEndTime,
    // totalTripTime: totalTripTime,
    // subTotalBill: subTotalBill,
    // totalBill: totalBill,
    differ: differ
  });
});

router.get('/getImage', function(req, res) {
  googlemaps.getImage(function(image){
    res.json({
      image: image
    })
  });
});

router.get('/getImageUrl', function(req, res) {
  var url = googlemaps.getImagelink();
  res.json({
    url: url
  });
});

router.post('/sendMail', function(req, res) {
  var recipients  = req.body.recipients;
  var url = googlemaps.getImagelink();
  var tripInfo = {
    rider: {
      name: 'Mahaboob'
    },
    trip: {
      trip_start: "2016-08-01 02:01:01",
      from_area: "Banani, Dhaka, Dhaka Division",
      trip_end: "2016-08-01 03:16:55",
      to_area: "Uttara, Dhaka, Dhaka Division",
      cost_per_min: 3.5,
      total_trip_time: "1:15:54",
      surge_factor: 1,
      total_bill: 256,
      screenshot_url: url
    }
  }
  // ["23.7935142, 90.4047918","23.7926711, 90.4042587","23.7916407, 90.4041082","23.791138, 90.4023422","23.7914186, 90.4009133","23.7902558, 90.4003748","23.7890634, 90.4001971","23.7876586, 90.3999738","23.7866875, 90.3998237","23.7856084, 90.3996146","23.7846178, 90.3995504","23.78367, 90.399361","23.7826361, 90.3991942","23.7814406, 90.3989889","23.7804971, 90.3986779","23.7796126, 90.3984916","23.7786719, 90.3984378","23.7773565, 90.3983658","23.7762988, 90.3985837","23.7754078, 90.3988568","23.7745208, 90.3992888","23.7736949, 90.3998628","23.7723535, 90.4008564","23.7713328, 90.4012068","23.7702713, 90.4012113","23.7691027, 90.4010287","23.7680551, 90.4008384","23.7670465, 90.4006722","23.7660945, 90.4004612","23.7649805, 90.4004077","23.7638588, 90.4001752","23.762859, 90.4000469","23.7613173, 90.3997252","23.7596065, 90.3994669","23.7581907, 90.3992653","23.7571145, 90.3991515","23.755684, 90.3996356","23.7539914, 90.4005397","23.7530447, 90.4011288","23.7519025, 90.4017712","23.7508604, 90.4024048","23.7497125, 90.4031116","23.7487204, 90.4036625","23.7477945, 90.4039774","23.7462474, 90.4040649","23.7449234, 90.4044265","23.7439597, 90.4055609","23.7430516, 90.4058367","23.7418575, 90.4058949","23.7403303, 90.4057156","23.7390833, 90.4055149","23.7379636, 90.4054102","23.7377379, 90.4065513","23.7375968, 90.4076512","23.7376607, 90.4087246","23.7374272, 90.4099002","23.7366617, 90.4104702","23.7354862, 90.4104147","23.7342013, 90.4103676","23.7329615, 90.4102474","23.7320202, 90.41025","23.7306429, 90.4102123","23.7296164, 90.4103364","23.7284039, 90.4105504","23.7305758, 90.4094255","23.7290913, 90.4082475","23.7281105, 90.4110034","23.7266123, 90.4107493","23.7255861, 90.4108218","23.7245709, 90.4107325","23.7234477, 90.4105687","23.7224682, 90.4107216","23.7213959, 90.4105362","23.7204925, 90.4101968","23.7195372, 90.4096848","23.7181088, 90.4087715","23.716549, 90.4086507"]

  //'https://maps.googleapis.com/maps/api/staticmap?size=500x400&maptype=roadmap&markers=color%3Agreen%7Clabel%3AFrom%7Cshadow%3Atrue%7C23.794130%2C%2090.402911&markers=color%3Ared%7Clabel%3Ato%7Cshadow%3Atrue%7C23.869384%2C%2090.390122&path=weight%3A5%7Ccolor%3A0x0000ff%7C23.794130%2C%2090.402911%7C23.794326%2C%2090.401065%7C23.802808%2C%2090.402310%7C23.809483%2C%2090.403340%7C23.816236%2C%2090.405443%7C23.817649%2C%2090.413296%7C23.822400%2C%2090.419776%7C23.826286%2C%2090.420420%7C23.837670%2C%2090.418103%7C23.847366%2C%2090.411022%7C23.858317%2C%2090.402052%7C23.861574%2C%2090.400035%7C23.870208%2C%2090.400379%7C23.869423%2C%2090.394027%7C23.869384%2C%2090.390122&style=feature%3Aroad%7Celement%3Aall%7Chue%3A0x00ff00&key=AIzaSyAMEce11xSoomoUCPq14nw9kKrMwzjJxyo'
  mailer.sendMail(recipients, tripInfo, url, function(err, info){
    if(err){
      console.log(err);
      res.json({
        error: 'error'
      });
    } else {
      res.json({
        status: 'success',
        info: info
      });
    }
  });
});

router.get('/getDateOnly', function(req, res) {
  var date = datetime.getDateOnly("08/05/2015 23:41:20");
  res.json({
    date: date
  })
});

router.get('/getModifiedDate', function(req, res) {
  var date = " 00:00:20";
  var date1 = datetime.modifyDate(date);
  res.json({
    date: date1
  })
});

router.post('/logout', function(req, res) {
  var biker_id = req.body.biker_id;
  database.query('UPDATE bikers SET loggedIn = ? WHERE id = ?', [0, biker_id], function(err, result) {
    if(err) {
      errorMessage.sendErrorMessage('server error', res);
    } else {
      res.json({
        result: result
      });
    }
  });
});

router.post('/notifyWithoutTag', function(req, res) {
  var message  = req.body.message
    , state    = req.body.state
    , reg_code = req.body.reg_code;

  var regToken = [reg_code];

  biker_push.sendNotification(regToken, state, message, function(err) {
    if(err){
      console.log("error" + err);
      res.json({ message: 'error'});
    } else {
      res.json({
        state: state,
        message: message,
        reg_code: reg_code
      });
    }
  });
});

router.post('/notifyWithTag', function(req, res) {
  var message  = req.body.message
    , state    = req.body.state
    , reg_code = req.body.reg_code;

  var regToken = [reg_code];

  push.sendNotification(regToken, state, message, function(err) {
    if(err){
      console.log("error" + err);
      res.json({ message: 'error'});
    } else {
      res.json({
        state: state,
        message: message,
        reg_code: reg_code
      });
    }
  });
});

// router.post('/nearestBikers', function(req, res) {
//   var rider_lat   = parseFloat(req.body.lat)
//     , rider_long  = parseFloat(req.body.long);
//   var array = []
//   for(i = 0; i < 10; i++) {
//     // var lat = (Math.random() * 0.01);
//     // var long = (Math.random() * 0.01);
//
//     var lat = (0.09 - (i / 1000)) + (Math.random() * 0.001);
//     var long = (0.01 + (i / 1000)) + (Math.random() * 0.001);
//
//     array.push([(rider_lat + lat), (rider_long + long)]);
//   }
//   res.json({'array': array});
// })
router.post('/saveRiderImage', function(req, res) {
  var base64Image = req.body.image;
  image.saveRiderImage(base64Image)
  .then(function(imageName) {
    res.json({ status: 'success', message: 'localhost:9531/static/rider/profile/' + imageName });
  })
  .catch(function(err) {
    res.json(err);
  });
});

router.post('/saveBikerImage', function(req, res) {
  var base64Image = req.body.image;
  image.saveBikerImage(base64Image)
  .then(function(imageName) {
    res.json({ status: 'success', message: imageName });
  })
  .catch(function(err) {
    res.json(err);
  });
});

router.post('/deleteImage', function(req, res) {
  var imageName = req.body.image;

  image.deleteImage(imageName)
  .then(function(response) {
    res.json(response);
  })
  .catch(function(err) {
    res.json(err);
  });
});

var rider_model = require('../../models/riders');
router.post('/findUser', function(req, res) {
  var phone_num = req.body.phone_num;
  var email = req.body.email;

  rider_model.findWithPhoneAndEmail(phone_num, email)
  .then(function(rider) {
    res.json(rider[0]);
  })
  .catch(function(err) {
    res.json(err);
  });
});

router.post('/sendTestMail', function(req, res) {
  var email = req.body.email;

  mailer.sendTestMail(email, function (err, response) {
    if(err) res.json(err);
    else res.json(response);
  });
});

router.post('/arraytest', function(req, res) {
  var mypath = ["23.8667367, 90.3993017", "23.8656083, 90.3991067" ,"23.8644167, 90.3989467","23.8635233, 90.3987833", "23.862275, 90.3987683", "23.8610467, 90.3986467", "23.860605, 90.39958", "23.860025, 90.4007867","23.8588867, 90.401595", "23.8576083, 90.40265" , "23.8560967, 90.4039033", "23.8551033, 90.4047367", "23.8537283, 90.4058817","23.85274, 90.4068233", "23.8519183, 90.4074633", "23.8511283, 90.408095", "23.8500167, 90.4091167", "23.8487267, 90.41016","23.8476117, 90.4111", "23.8464633, 90.4120333", "23.8450233, 90.413205", "23.844045, 90.4140367", "23.8431867, 90.4147783","23.8417, 90.4160167", "23.8404667, 90.4169233", "23.839275, 90.4176333", "23.8371967, 90.41853", "23.8362383, 90.4187617","23.83466, 90.4190917", "23.8336417, 90.4192733", "23.8327417, 90.4194033", "23.8314633, 90.41963", "23.8302633, 90.4198717","23.8291583, 90.4199567", "23.82778, 90.4201917", "23.8265233, 90.4203467", "23.8246933, 90.4205267", "23.8228783, 90.4199017","23.8219183, 90.4192583", "23.8210233, 90.41826", "23.8203367, 90.4172417","23.8193683, 90.4157717", "23.81867, 90.414625", "23.81764, 90.4131", "23.8167117, 90.4112633", "23.81674, 90.4102217","23.8168633, 90.4084533", "23.816925, 90.40698", "23.8161883, 90.4052767", "23.8154283, 90.4046983", "23.8145733, 90.40439","23.8128583, 90.4041083", "23.8116633, 90.4039067", "23.8107267, 90.4037883","23.8095617, 90.4035533", "23.8082933, 90.403375","23.8072467, 90.403225", "23.8060717, 90.4030417","23.80511, 90.4029117","23.803685, 90.4026767","23.8019383, 90.4023567","23.80098, 90.402175"]

  console.log(mypath.length);
  for (var i = 0; i < mypath.length; i += 2) {
    var removed = mypath.splice(i, 1);
    console.log(removed);
  }
  console.log(mypath.length);
  res.json({arraylength: mypath.length});
});

router.get('/simpleJson', function(req, res) {
  res.json({
    "status": 'success',
    "res_code": '200',
    "data": [{
      "type": "articles",
      "id": "1",
      "attributes": {
        "title": "JSON API paints my bikeshed!"
      },
      "relationships": {
        "author": {
          "links": {
            "self": "http://example.com/articles/1/relationships/author",
            "related": "http://example.com/articles/1/author"
          },
          "data": { "type": "people", "id": "9" }
        },
        "comments": {
          "links": {
            "self": "http://example.com/articles/1/relationships/comments",
            "related": "http://example.com/articles/1/comments"
          },
          "data": [
            { "type": "comments", "id": "5" },
            { "type": "comments", "id": "12" }
          ]
        }
      },
      "links": {
        "self": "http://example.com/articles/1"
      }
    }]
  });
})

//["23.8667367, 90.3993017","23.8656083, 90.3991067","23.8644167, 90.3989467","23.8635233, 90.3987833","23.862275, 90.3987683","23.8610467, 90.3986467","23.860605, 90.39958","23.860025, 90.4007867","23.8588867, 90.401595","23.8576083, 90.40265","23.8560967, 90.4039033","23.8551033, 90.4047367","23.8537283, 90.4058817","23.85274, 90.4068233","23.8519183, 90.4074633","23.8511283, 90.408095","23.8500167, 90.4091167","23.8487267, 90.41016","23.8476117, 90.4111","23.8464633, 90.4120333","23.8450233, 90.413205","23.844045, 90.4140367","23.8431867, 90.4147783","23.8417, 90.4160167","23.8404667, 90.4169233","23.839275, 90.4176333","23.8371967, 90.41853","23.8362383, 90.4187617","23.83466, 90.4190917","23.8336417, 90.4192733","23.8327417, 90.4194033","23.8314633, 90.41963","23.8302633, 90.4198717","23.8291583, 90.4199567","23.82778, 90.4201917","23.8265233, 90.4203467","23.8246933, 90.4205267","23.8228783, 90.4199017","23.8219183, 90.4192583","23.8210233, 90.41826","23.8203367, 90.4172417","23.8193683, 90.4157717","23.81867, 90.414625","23.81764, 90.4131","23.8167117, 90.4112633","23.81674, 90.4102217","23.8168633, 90.4084533","23.816925, 90.40698","23.8161883, 90.4052767","23.8154283, 90.4046983","23.8145733, 90.40439","23.8128583, 90.4041083","23.8116633, 90.4039067","23.8107267, 90.4037883","23.8095617, 90.4035533","23.8082933, 90.403375","23.8072467, 90.403225","23.8060717, 90.4030417","23.80511, 90.4029117","23.803685, 90.4026767","23.8019383, 90.4023567","23.80098, 90.402175","23.7999983, 90.402005" ,"23.7991433, 90.40233","23.7990017, 90.4033817","23.79871, 90.4047767","23.7982583, 90.4060383","23.797135, 90.4072117","23.7961267, 90.4077083","23.7948983, 90.4069583" ,"23.7936833, 90.4060683","23.793525, 90.4050483","23.795293, 90.4047136","23.7936238, 90.4049709"]




module.exports = router;
