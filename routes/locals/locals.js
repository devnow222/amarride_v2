var express     = require('express')
  , bulksms     = require('./bulksms')
  , bulkpush    = require('./bulkpush')
  , partnership = require('./partnership')
  , router      = express.Router();

// local Routes for '/api'
router.use('/bulksms', bulksms);
router.use('/bulkpush', bulkpush);
router.use('/partnership', partnership);

// local Routes for '/api'
router.get('/', function(req, res){
  res.json({
    greetings: 'Welcome to AmarRide V2'
  });
});

module.exports = router;
