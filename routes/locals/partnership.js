var express           = require('express')
  , partnership_model = require('../../models/partnerships')
  , errorMessage      = require('../../helpers/errormessage')
  , router            = express.Router();

// local Routes for '/api'
router.get('/', function(req, res){
  res.json({
    greetings: 'Welcome to AmarRide V2 partnership'
  });
});

router.post('/', function(req, res) {
  if(!req.body.first_name || !req.body.last_name  || !req.body.phone_num || !req.body.type) {
    errorMessage.sendErrorMessage('All fields are not set! Please make sure you are sending all required information.', res);
  } else {
    var value = {
      first_name : req.body.first_name,
      last_name  : req.body.last_name,
      email      : req.body.email ? req.body.email : "",
      type       : req.body.type,
      phone_num  : req.body.phone_num,
      message    : req.body.message ? req.body.message : ""
    };

    partnership_model.findMatch(value.phone_num)
    .then(function(partner) {
      errorMessage.sendErrorMessage('We already have you in our list.', res);
    })
    .catch(function(err) {
      partnership_model.add(value)
      .then(function(riders) {
        res.json({ status: 'success' });
      })
      .catch(function(err) {
        errorMessage.sendErrorMessage(err, res);
      });
    });
  }
});

module.exports = router;
