var express      = require('express')
  , rider_model  = require('../../models/riders')
  , push          = require('../../helpers/push')
  , errorMessage = require('../../helpers/errormessage')
  , router       = express.Router();

// local Routes for '/api'
router.get('/', function(req, res){
  res.json({
    greetings: 'Welcome to AmarRide V2 bulk push'
  });
});

router.post('/', function(req, res) {
  if(!req.body.text_content) {
    errorMessage.sendErrorMessage('All fields are not set! Please make sure you are sending all information.', res);
  } else {
    var text_content = req.body.text_content;
    rider_model.findAll()
    .then(function(riders) {
      var riders_arr = riders;
      var reg_codes = [[]];
      riders_arr.map(function(rider, index) {
        if(rider.reg_code != "" || rider.reg_code != '(null)' || rider.reg_code != "0" || rider.reg_code != 0 || rider.reg_code != null || rider.reg_code != 'null' || rider.reg_code != NULL || rider.reg_code != undefined) {
          var newIndex = Math.floor(index / 900);
          if(reg_codes[newIndex]) {
            reg_codes[newIndex].push(rider.reg_code);
          } else {
            reg_codes.push([]);
            reg_codes[newIndex].push(rider.reg_code);
          }
        }
        // if(rider.id == '180') {
        //   console.log(rider.full_name);
        //   push.bulkNotification([rider.reg_code], text_content);
        // }
      });

      reg_codes.map(function(reg_code) {
        push.bulkNotification(reg_code, text_content);
      });

      res.json({
        status: 'success'
      });
    })
    .catch(function(err) {
      errorMessage.sendErrorMessage(err, res);
    });
  }
});

module.exports = router;
