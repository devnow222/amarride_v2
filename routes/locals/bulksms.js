var express      = require('express')
  , rider_model  = require('../../models/riders')
  , sms          = require('../../helpers/sms')
  , errorMessage = require('../../helpers/errormessage')
  , router       = express.Router();

// local Routes for '/api'
router.get('/', function(req, res){
  res.json({
    greetings: 'Welcome to AmarRide V2 bulk sms'
  });
});

router.post('/', function(req, res) {
  if(!req.body.text_content) {
    errorMessage.sendErrorMessage('All fields are not set! Please make sure you are sending all information.', res);
  } else {
    var text_content = req.body.text_content;
    rider_model.findAll()
    .then(function(riders) {
      var riders_arr = riders;
      riders_arr.map(function(rider) {
        sms.sendBulk(rider.phone_num, text_content);
      })
      res.json({
        status: 'success'
      })
    })
    .catch(function(err) {
      errorMessage.sendErrorMessage(err, res);
    });
  }
});

module.exports = router;
