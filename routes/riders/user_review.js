var express             = require('express')
  , rider_model         = require('../../models/riders')
  , review_model        = require('../../models/reviews')
  , push                = require('../../helpers/push')
  , errorMessage        = require('../../helpers/errormessage')
  , router              = express.Router();


router.post('/', function(req, res){
  if(!req.body.rider_id || !req.body.message) {
    errorMessage.sendErrorMessage('Please provide all information and try again.', res);
  } else {
    var rider_id = req.body.rider_id
      , message  = req.body.message
      , value    = { rider_id: rider_id, message: message };

    review_model.add(value)
    .then(function(result) {
      res.json({ status: 'success', message: 'Feedback successfully sent.' });

      setTimeout(function() {
        rider_model.findWithId(rider_id)
        .then(function(rider) {
          var message = 'Dear ' + rider[0].full_name + ', thank you for your review. Please let us know if you have any other review or suggestion to improve our app.';
          if(rider[0].os_type == 'Android') {
            push.forceNotification([rider[0].reg_code], 'Success', message, function(err)  {
              if(err) console.log("error" + err);
              else console.log("success notification sent");
            });
          } else {
            push.sendNotification([rider[0].reg_code], 'Success', message, function(err)  {
              if(err) console.log("error" + err);
              else console.log("success notification sent");
            });
          }
        });
      }, 60 * 1000);
    })
    .catch(function(err) {
      errorMessage.sendErrorMessage(err, res);
    });
  }
});

module.exports = router;
