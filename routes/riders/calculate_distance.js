var express            = require('express')
  , success_trip_model = require('../../models/success_trips')
  , calc_distance      = require('../../models/calc_distance')
  , errorMessage       = require('../../helpers/errormessage')
  , googlemaps         = require('../../helpers/googlemaps')
  , router             = express.Router();

router.post('/', function(req, res) {
  success_trip_model.findAll()
  .then(function(successTrips) {
    var allTrips = successTrips;
    allTrips.map(function(trip) {
      console.log("Working..." + trip.id);
      googlemaps.getDistanceAndTime(trip.from_area, trip.to_area)
      .then(function(value) {
        var value = {
          biker_id : trip.biker_id,
          from_area : trip.from_area,
          to_area   : trip.to_area,
          distance  : value.rows[0].elements[0].distance.text || 0,
          time : trip.created
        }
        calc_distance.add(value);
      });
    });
    res.json({
      'success' : 'success'
    });
  });
});

module.exports = router;
