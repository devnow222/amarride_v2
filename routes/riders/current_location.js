var express      = require('express')
  , trip_model   = require('../../models/trips')
  , rider_model  = require('../../models/riders')
  , biker_model  = require('../../models/bikers')
  , googlemaps   = require('../../helpers/googlemaps')
  , errorMessage = require('../../helpers/errormessage')
  , router       = express.Router();

/** POST /api/rider/currentLocation - update rider's/user's current location.
 * @property {string} req.body.rider_id - id of rider/user.
 * @property {string} req.body.lat - latitude of current rider/user.
 * @property {string} req.body.long - longitude of current rider/user.
 * @returns (success)=>{status}//(error)=>{status, message}.
 */
router.post('/', function(req, res) {
  if(!req.body.rider_id || !req.body.lat || req.body.lat == 0 || !req.body.long || req.body.long == 0 ) {
    errorMessage.sendErrorMessage('Something went wrong! Please try again.', res);
  } else {
    var rider_id     = req.body.rider_id
      , current_lat  = req.body.lat
      , current_long = req.body.long
      , from_area    = null
      , to_area      = null
      , sendToRider  = {};

    var update  = { current_latitude: current_lat, current_longitude: current_long }
    rider_model.updateWithId(update, rider_id)
    .then(function(result) {
      return trip_model.findWithRiderId(rider_id);
    })
    .then(function(trips) {
      if(trips[0].start_latitude && trips[0].start_longitude)
        to_area = (trips[0].start_latitude).toString() + ',' + (trips[0].start_longitude).toString();
      return biker_model.findWithBikerId(trips[0].biker_id);
    })
    .then(function(bikers) {
      if(bikers)
        from_area = (bikers[0].current_latitude).toString() + ',' + (bikers[0].current_longitude).toString();
      return googlemaps.getDistanceAndTime(from_area, to_area);
    })
    .then(function(value) {
      if(value) {
        if(value.destination_addresses != '' || value.origin_addresses != ''){
          sendToRider = {
            status : 'success',
            total_time : (value.rows[0].elements[0].duration.text).toString(),
            distance : (value.rows[0].elements[0].distance.text).toString()
          }
          res.json(sendToRider);
        } else {
          throw 'Unable to calculate distance';
        }
      } else {
        throw 'Unable to calculate distance';
      }
    })
    .catch(function(err) {
      errorMessage.sendErrorMessage(err, res);
    });
  }
});

module.exports = router;
