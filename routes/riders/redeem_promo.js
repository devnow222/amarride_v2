var express       = require('express')
  , promo_model   = require('../../models/promos')
  , wallet_model  = require('../../models/wallets')
  , deposit_model = require('../../models/deposits')
  , errorMessage  = require('../../helpers/errormessage')
  , router        = express.Router();

/** POST /api/rider/redeemPromo - add a perticular trip to favorite list.
 * @property {string} req.body.trip_id - The trip_id of a trip.
 * @returns (success)=>{status, message}//(error)=>{status, message}.
 */
router.post('/', function(req, res) {
  if(!req.body.promocode || !req.body.rider_id) {
    errorMessage.sendErrorMessage('Something went wrong! Please try again.', res);
  } else {
    var rider_id  = req.body.rider_id
      , promocode = req.body.promocode
      , promo     = {};

    promo_model.findWithPromo(promocode)
    .then(function(promos) {
      promo = promos[0];
      return wallet_model.findWithRiderId(rider_id);
    })
    .then(function(wallet) {
      balance = wallet[0].balance + parseFloat(promo.amount)
      var update  = { balance: balance }
        , value   = {
          wallet_id: wallet[0].id,
          amount: promo.amount,
          source: 'promo',
          code: promo.code
        };
      deposit_model.add(value);
      return wallet_model.updateWithRiderId(update, rider_id);
    })
    .then(function(result) {
      var update = { active: 0 };
      return promo_model.updateWithPromo(update, promocode);
    })
    .then(function(result) {
      var message = promo.amount + ' BDT has been added to your wallet';
      res.json({ status: 'success', message: message });
    })
    .catch(function(err) {
      errorMessage.sendErrorMessage(err, res);
    });
  }
});

module.exports = router;
