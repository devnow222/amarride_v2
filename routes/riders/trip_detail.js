var express            = require('express')
  , biker_model        = require('../../models/bikers')
  , rating_model       = require('../../models/ratings')
  , success_trip_model = require('../../models/success_trips')
  , datetime           = require('../../helpers/datetime')
  , errorMessage       = require('../../helpers/errormessage')
  , router             = express.Router();

// Show history details of a rider
// 100% done - not tested - documented
router.post('/', function(req, res) {
  if(!req.body.trip_id) {
    errorMessage.sendErrorMessage('Something went wrong! Please try again.', res);
  } else {
    var trip_id     = req.body.trip_id
      , sendToRider = {};

    success_trip_model.findWithTripId(trip_id)
    .then(function(successTrips) {
      sendToRider.history = {
        trip_id: successTrips[0].trip_id.toString(),
        from_area: successTrips[0].from_area.toString(),
        to_area: successTrips[0].to_area.toString(),
        cost_per_min: successTrips[0].cost_per_min.toString(),
        waiting_charge_per_min: successTrips[0].waiting_charge_per_min.toString(),
        duration: datetime.getMinutesOnly(successTrips[0].total_trip_time).toString(),
        waitingDuration: datetime.getMinutesOnly(successTrips[0].total_trip_time).toString(),
        total_bill: successTrips[0].total_bill.toString(),
        total_waiting_bill: successTrips[0].total_waiting_bill.toString(),
        favid: successTrips[0].favid.toString(),
        screenshot_url: successTrips[0].trip_screenshot.toString()
      };
      return biker_model.findWithBikerId(successTrips[0].biker_id);
    })
    .then(function(biker) {
      sendToRider.biker = { full_name: biker[0].full_name, rating: (biker[0].rating / 10).toString() };
      sendToRider.status = 'success';
      res.json(sendToRider);
    })
    .catch(function(err) {
      errorMessage.sendErrorMessage(err, res);
    });
  }
});

module.exports = router;
