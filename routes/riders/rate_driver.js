var express            = require('express')
  , biker_model        = require('../../models/bikers')
  , rating_model       = require('../../models/ratings')
  , success_trip_model = require('../../models/success_trips')
  , errorMessage       = require('../../helpers/errormessage')
  , router             = express.Router();

// Rider rates the driver after a trip
// 90% done - not tested - documented
router.post('/', function(req, res) {
  if(!req.body.biker_id || !req.body.rider_id || !req.body.rating_value || !req.body.trip_id) {
    errorMessage.sendErrorMessage('Something went wrong! Please try again.', res);
  } else {
    var biker_id      = req.body.biker_id
      , rider_id      = req.body.rider_id
      , rating_value  = req.body.rating_value
      , trip_id       = req.body.trip_id;

    var value = {
      rider_id    : rider_id,
      biker_id    : biker_id,
      who_rated   : 'Rider',
      rating_value: rating_value
    };

    rating_model.add(value)
    .then(function(result) {
      var update = { rating_id: result.insertId };
      return success_trip_model.updateWithTripId(update, trip_id);
    })
    .then(function(result) {
      return rating_model.findWithBikerId(biker_id);
    })
    .then(function(ratings) {
      var ratingValue = 0;
      if(ratings.length > 0) {
        ratings.map(function(rating){
          ratingValue += rating.rating_value
        });
        ratingValue = parseInt((ratingValue / ratings.length) * 10);
      }
      var update = { rating: ratingValue };
      return biker_model.updateWithBikerId(update, biker_id);
    })
    .then(function(result) {
      if(result) {
        res.json({  status: 'success' });
      }
    })
    .catch(function(err) {
      errorMessage.sendErrorMessage(err, res);
    });
  }
});

module.exports = router;
