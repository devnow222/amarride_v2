var express      = require('express')
  , rider_model  = require('../../models/riders')
  , errorMessage = require('../../helpers/errormessage')
  , router       = express.Router();

// Biker cancel trip with reason
// 100% done - not tested - documented
router.post('/', function(req, res){
  if(!req.body.rider_id || !req.body.reg_code || !req.body.device_id) {
    errorMessage.sendErrorMessage('Something went wrong! Please try again.', res);
  } else {
    var rider_id  = req.body.rider_id
      , reg_code  = req.body.reg_code
      , device_id = req.body.device_id;

    rider_model.findWithId(rider_id)
    .then(function(rider) {
      if(rider && rider[0].device_id == device_id) {
        var update   = { reg_code: reg_code }
        rider_model.updateWithId(update, rider_id)
        .then(function(result) {
          res.json({ status: 'success' });
        });
      } else {
        res.json({ status: 'logout' });
      }
    })
    .catch(function(err) {
      errorMessage.sendErrorMessage(err, res);
    });
  }
});

module.exports = router;
