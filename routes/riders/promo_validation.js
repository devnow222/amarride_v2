var express      = require('express')
  , promo_model  = require('../../models/promos')
  , errorMessage = require('../../helpers/errormessage')
  , router       = express.Router();

// Promocode validation
// 90% done - not tested - not documented
router.post('/', function(req, res) {
  if(!req.body.promocode) {
    errorMessage.sendErrorMessage('Something went wrong! Please try again.', res);
  } else {
    var promocode = req.body.promocode;

    promo_model.findWithPromo(promocode)
    .then(function(promo) {
      if(promo) {
        res.json({
          status: 'success',
          message: 'valid promocode'
        });
      }
    })
    .catch(function(err) {
      errorMessage.sendErrorMessage(err, res);
    });
  }
});

module.exports = router;
