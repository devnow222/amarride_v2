var express      = require('express')
  , biker_model  = require('../../models/bikers')
  , setting_model= require('../../models/settings')
  , distance     = require('../../helpers/distance')
  , errorMessage = require('../../helpers/errormessage')
  , router       = express.Router();

// Rider search for nearest available biker
// 100% done - tested - documented
router.post('/', function(req, res){
  if(req.body.lat == '' || req.body.lat == 0 || req.body.long == '' || req.body.long == 0 || req.body.vehicle_type == '') {
    errorMessage.sendErrorMessage('Something went wrong! Please try again.', res);
  } else {
    var rider_lat   = parseFloat(req.body.lat)
      , rider_long  = parseFloat(req.body.long)
      , biker_type  = req.body.vehicle_type == 3 ? 0 : req.body.vehicle_type
      , requestArea = 5;

    setting_model.findAll()
    .then(function(settings) {
      switch(biker_type) {
        case '0':
          requestArea = settings[0].requestArea_bike;
          break;
        case '1':
          requestArea = settings[0].requestArea_cng;
          break;
        case '2':
          requestArea = settings[0].requestArea_car;
          break;
        default:
          requestArea = settings[0].requestArea_bike;
      }
      return biker_model.findAllAvailable(biker_type);
    })
    .then(function(bikers) {
      var nearestBikers = [];
      if(bikers != '') {
        for(var i = 0; i < bikers.length; i++) {
          var mDistance = distance.getDistanceFromLatLonInKm(rider_lat, rider_long, bikers[i].current_latitude, bikers[i].current_longitude);
          // console.log("============================================================================");
          // console.log(mDistance);
          // console.log("============================================================================");
          if(mDistance <= requestArea){
            // console.log(mDistance);
            // console.log("============================================================================");
            nearestBikers.push({
              //biker_id  : bikers[i].id.toString(),
              biker_lat : bikers[i].current_latitude,
              biker_long: bikers[i].current_longitude
              //distance  : mDistance.toString()
            });
          }
        }
      }
      res.json({
        status: 'success',
        bikers: nearestBikers
      });
    })
    .catch(function(err) {
      errorMessage.sendErrorMessage(err, res);
    });
  }
});

module.exports = router;
