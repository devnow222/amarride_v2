var express        = require('express')
  , rider_model    = require('../../models/riders')
  , trip_model     = require('../../models/trips')
  , temptrip_model = require('../../models/temptrips')
  , errorMessage   = require('../../helpers/errormessage')
  , router         = express.Router();

// Rider logout
// 100% done - not tested - documented
router.post('/', function(req, res) {
  if(!req.body.rider_id || !req.body.device_id) {
    errorMessage.sendErrorMessage('Something went wrong! Please try again.', res);
  } else {
    var rider_id  = req.body.rider_id
      , device_id = req.body.device_id
      , update    = { device_id: '', reg_code: '0' };

    rider_model.findWithId(rider_id)
    .then(function(rider) {
      if(rider && rider[0].device_id == device_id) {
        trip_model.findWithRiderId(rider_id)
        .then(function(trip) {
          if(trip[0].trip_status != 'Started') {
            trip_model.deleteWithRiderId(rider_id);
            rider_model.updateWithId(update, rider_id)
            res.json({ status: 'success' });
          } else {
            errorMessage.sendErrorMessage('You can\'t logout while being in a trip.', res);
          }
        })
        .catch(function(err) {
          temptrip_model.deleteWithRiderId(rider_id)
          rider_model.updateWithId(update, rider_id)
          res.json({ status: 'success' });
        });
      } else {
        res.json({ status: 'success' });
      }
    })
    .catch(function(err) {
      errorMessage.sendErrorMessage(err, res);
    })
  }
});

module.exports = router;
