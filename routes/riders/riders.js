var express             = require('express')
  , login               = require('./login')
  , logout              = require('./logout')
  , history             = require('./history')
  , user_review         = require('./user_review')
  , cancel_trip         = require('./cancel_trip')
  , rate_driver         = require('./rate_driver')
  , wallet_info         = require('./wallet_info')
  , trip_detail         = require('./trip_detail')
  , add_fav_trip        = require('./add_fav_trip')
  , get_fav_trip        = require('./get_fav_trip')
  , trip_request        = require('./trip_request')
  , verify_rider        = require('./verify_rider')
  , check_pricing       = require('./check_pricing')
  , nearest_biker       = require('./nearest_biker')
  , pending_rating      = require('./pending_rating')
  , update_regcode      = require('./update_regcode')
  , delete_fav_trip     = require('./delete_fav_trip')
  , change_password     = require('./change_password')
  , fare_estimation     = require('./fare_estimation')
  , current_location    = require('./current_location')
  , promo_validation    = require('./promo_validation')
  , get_biker_info      = require('./get_biker_info')
  , deposit_balance     = require('./deposit_balance')
  , redeem_promo        = require('./redeem_promo')
  , forget_password     = require('./forget_password')
  , resend_verification = require('./resend_verification')
  , signup              = require('./signup')
  , edit_profile        = require('./edit_profile')
  , calculate_distance  = require('./calculate_distance')
  , router              = express.Router();


router.use('/login', login);
router.use('/logout', logout);
router.use('/history', history);
router.use('/userReview', user_review);
router.use('/cancelTrip', cancel_trip);
router.use('/rateDriver', rate_driver);
router.use('/walletInfo', wallet_info);
router.use('/tripDetail', trip_detail);
router.use('/addFavTrip', add_fav_trip);
router.use('/getFavTrip', get_fav_trip);
router.use('/tripRequest', trip_request);
router.use('/verifyRider', verify_rider);
router.use('/checkPricing', check_pricing);
router.use('/nearestBiker', nearest_biker);
router.use('/getBikerInfo', get_biker_info);
router.use('/pendingRating', pending_rating);
router.use('/updateRegcode', update_regcode);
router.use('/deleteFavTrip', delete_fav_trip);
router.use('/changePassword', change_password);
router.use('/fareEstimation', fare_estimation);
router.use('/currentLocation', current_location);
router.use('/promoValidation', promo_validation);
router.use('/depositBalance', deposit_balance);
router.use('/redeemPromo', redeem_promo);
router.use('/forgetPassword', forget_password);
router.use('/resendVerification', resend_verification);
router.use('/signup', signup);
router.use('/editProfile', edit_profile);
router.use('/calculateDistance', calculate_distance);


// local Routes for '/api/rider'
router.get('/', function(req, res, next){
  res.json({
    greetings: 'Welcome to AmarRide V2 Rider Part'
  });
});

module.exports = router;
