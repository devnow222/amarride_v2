var express            = require('express')
  , biker_profile      = require('../../config/config').env.biker_profile
  , trip_model         = require('../../models/trips')
  , biker_model        = require('../../models/bikers')
  , success_trip_model = require('../../models/success_trips')
  , errorMessage       = require('../../helpers/errormessage')
  , router             = express.Router();

/** POST /api/rider/deleteFavTrip - delete a perticular trip from favorite list.
 * @property {string} req.body.rider_id - id of a rider/user.
 * @returns {status, biker, biker_rating, ride, total_time, distance}<=(success)
 * @returns {status, trip_status, total_bill, total_trip_time, cost_per_min, base_price, screenshot_url}<=(trip_not_found)
 * @returns {status, message}<=(error).
 */
router.post('/', function(req, res) {
  var rider_id = req.body.rider_id
    , biker_id = null
    , sendToRider = {};

  trip_model.findPreviousWithRiderId(rider_id)
  .then(function(trip) {
    sendToRider.trip_id = trip[0].id.toString();
    sendToRider.trip_status = trip[0].trip_status;
    sendToRider.from_area = trip[0].from_area;
    sendToRider.to_area = trip[0].to_area;
    biker_id = trip[0].biker_id;
    return biker_model.findWithBikerId(biker_id)
  })
  .then(function(biker) {
    sendToRider.biker = {
      biker_id: biker[0].id.toString(),
      biker_lat: biker[0].current_latitude,
      biker_long: biker[0].current_longitude,
      full_name: biker[0].full_name,
      phone_num: '+' + biker[0].phone_num,
      biker_photo: biker_profile + biker[0].user_photo,
      biker_email: biker[0].email
    }
    sendToRider.ride = {
      ride_model: biker[0].ride_model,
      ride_sl_num: biker[0].ride_serial
    };
    sendToRider.biker_rating = (biker[0].rating / 10).toString();
    sendToRider.total_time = '0';
    sendToRider.distance   = '0';
    sendToRider.status     = 'success';
    res.json(sendToRider);
  })
  .catch(function(err) {
    if(err == 'trip_not_found') {
      success_trip_model.findPendingWithRiderId(rider_id)
      .then(function(success_trip) {
        sendToRider = {
          status: 'success',
          trip_status: 'Ended',
          total_bill: success_trip[0].total_bill.toString(),
          total_trip_time: success_trip[0].total_trip_time.toString(),
          cost_per_min: success_trip[0].cost_per_min.toString(),
          waiting_charge_per_min: success_trip[0].waiting_charge_per_min.toString(),
          total_wating_time: success_trip[0].total_wating_time.toString(),
          total_waiting_bill: success_trip[0].total_waiting_bill.toString(),
          base_price: success_trip[0].base_price.toString(),
          screenshot_url: success_trip[0].trip_screenshot,
          trip_end: {
            total_bill: success_trip[0].total_bill.toString(),
            total_trip_time: success_trip[0].total_trip_time.toString(),
            cost_per_min: success_trip[0].cost_per_min.toString(),
            waiting_charge_per_min: success_trip[0].waiting_charge_per_min.toString(),
            total_wating_time: success_trip[0].total_wating_time.toString(),
            total_waiting_bill: success_trip[0].total_waiting_bill.toString(),
            base_price : success_trip[0].base_price.toString(),
            screenshot_url: success_trip[0].trip_screenshot
          }
        };
        res.json(sendToRider);
      })
      .catch(function(err) {
        errorMessage.sendErrorMessage(err, res);
      });
    } else {
      errorMessage.sendErrorMessage(err, res);
    }
  });
});

module.exports = router;
