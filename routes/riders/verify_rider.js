var express      = require('express')
  , rider_model  = require('../../models/riders')
  , errorMessage = require('../../helpers/errormessage')
  , router       = express.Router();

router.post('/', function(req, res){
  if(!req.body.phone_num || !req.body.code) {
    errorMessage.sendErrorMessage('Something went wrong! Please try again.', res);
  } else {
    var phone_num = req.body.phone_num
      , code = req.body.code;

    rider_model.findWithPhone(phone_num)
    .then(function(rider) {
      if(rider[0].verification_code == code) {
        var update = { verified: 1 };
        rider_model.updateWithId(update, rider[0].id)
        .then(function(result) {
          res.json({ status: 'success' });
        })
        .catch(function(err) {
          errorMessage.sendErrorMessage(err, res);
        });
      } else {
        errorMessage.sendErrorMessage('Invalid verification code.', res);
      }
    })
    .catch(function(err) {
      errorMessage.sendErrorMessage(err, res);
    });
  }
});

module.exports = router;
