var express      = require('express')
  , user_profile = require('../../config/config').env.user_profile
  , rider_model  = require('../../models/riders')
  , errorMessage = require('../../helpers/errormessage')
  , riderauth    = require('../../middlewares/riderauth')
  , router       = express.Router();

// Rider Login - rider will request using email and password and will get back
// a secret to use for further requests to api's created with node
// 100% done - tested - documented
router.post('/', riderauth.simpleauth, function(req, res){
  if(!req.body.reg_code || req.body.reg_code == 0 || !req.body.device_id || !req.body.os_type) {
    errorMessage.sendErrorMessage('Something went wrong! Please try again.', res);
  } else {
    var email       = (req.body.email.replace(/ /g,'')).toLowerCase()
      , password    = req.body.password
      , reg_code    = req.body.reg_code
      , device_id   = req.body.device_id
      , os_type     = req.body.os_type
      , sendToRider = {};

    var update = { reg_code: reg_code, device_id: device_id, os_type: os_type };

    rider_model.findWithEmail(email)
    .then(function(rider) {
      if(rider[0].verified == 1) {
        var token = riderauth.gettoken(email, password);
        var name_array = rider[0].full_name.split(' ');
        sendToRider = {
          status: 'success',
          rider_id: rider[0].id.toString(),
          full_name: rider[0].full_name,
          first_name: name_array[0],
          phone_num: rider[0].phone_num,
          email: rider[0].email,
          user_photo: user_profile + rider[0].user_photo,
          token: token
        }
        return rider_model.updateWithEmail(update, email);
      } else {
        sendToRider = {
          status: 'notVerified',
          phone_num: rider[0].phone_num
        }
        res.json(sendToRider);
      }
    })
    .then(function(result) {
      if(result) {
        res.json(sendToRider);
      }
    })
    .catch(function(err) {
      errorMessage.sendErrorMessage(err, res);
    });
  }
});

module.exports = router;
