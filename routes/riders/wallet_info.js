var express            = require('express')
  , wallet_model       = require('../../models/wallets')
  , deposit_model      = require('../../models/deposits')
  , success_trip_model = require('../../models/success_trips')
  , datetime           = require('../../helpers/datetime')
  , errorMessage       = require('../../helpers/errormessage')
  , riderauth          = require('../../middlewares/riderauth')
  , router             = express.Router();

// Wallet info
// 100% done - not tested - documented
//, riderauth.tokenauth
router.post('/', riderauth.tokenauth, function(req, res) {
  if(!req.body.rider_id) {
    errorMessage.sendErrorMessage('Something went wrong! Please try again.', res);
  } else {
    var rider_id    = req.body.rider_id
      , sendToRider = {};

    wallet_model.findWithRiderId(rider_id)
    .then(function(wallet) {
      sendToRider.balance = wallet[0].balance.toString();
      return deposit_model.findWithWalletId(wallet[0].id);
    })
    .then(function(deposits) {
      var deposits_array = [];
      if(deposits != ''){
        deposits.sort(function(a, b){return a.id-b.id});
        deposits.forEach(function(entry){
          var deposit = {
            date: datetime.getDateOnly(entry.created).toString(),
            amount: entry.amount.toString()
          };
          deposits_array.push(deposit);
        });
      }
      sendToRider.deposits = deposits_array;
      return success_trip_model.findWithRiderId(rider_id);
    })
    .then(function(successTrips) {
      var payments = [];
      if(successTrips != '') {
        successTrips.sort(function(a, b){return b.id-a.id});
        successTrips.forEach(function(entry){
          var successTrip = {
            date: datetime.getDateOnly(entry.trip_start).toString(),
            amount: entry.total_bill.toString()
          };
          payments.push(successTrip);
        });
      }
      sendToRider.payments = payments;
      sendToRider.status = 'success';
      res.json(sendToRider);
    })
    .catch(function(err) {
      errorMessage.sendErrorMessage(err, res);
    });
  }
});

module.exports = router;
