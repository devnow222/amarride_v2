var express      = require('express')
  , rider_model  = require('../../models/riders')
  , sms          = require('../../helpers/sms')
  , random       = require('../../helpers/random')
  , errorMessage = require('../../helpers/errormessage')
  , router       = express.Router();

router.post('/', function(req, res) {
  if(!req.body.phone_num) {
    errorMessage.sendErrorMessage('Something went wrong! Please try again.', res);
  } else {
    var phone_num   = req.body.phone_num
      , sendMessage = '';

    rider_model.findWithPhone(phone_num)
    .then(function(rider) {
      var verificationCode = random.verificationCode();
      var phone = rider[0].phone_num.substr(rider[0].phone_num.length - 2);
      sendMessage = 'A text message with your verification code has been sent to: ***** ****' + phone;
      sms.sendVerification(rider[0].phone_num, verificationCode);
      var update = { verification_code: verificationCode};
      return rider_model.updateWithId(update, rider[0].id);
    })
    .then(function(result) {
      res.json({ status: 'success', message: sendMessage })
    })
    .catch(function(err) {
      errorMessage.sendErrorMessage(err, res);
    })
  }
});

module.exports = router;
