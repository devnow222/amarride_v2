var express      = require('express')
  , setting_model= require('../../models/settings')
  , errorMessage = require('../../helpers/errormessage')
  , router       = express.Router();

/** POST /api/rider/checkPricing - check pricing of a perticular vehicle.
 * @property {string} req.body.vehicle_type - type of vehicle.
 * @returns (success)=>{status, base_price, cost_per_min, capacity}//(error)=>{status, message}.
 */
router.post('/', function(req, res) {
  if(!req.body.vehicle_type) {
    errorMessage.sendErrorMessage('Something went wrong! Please try again.', res);
  } else {
    var type = req.body.vehicle_type;

    setting_model.findAll()
    .then(function(settings) {
      var sendValue = {};

      switch (type) {
        case '0':
          sendValue.base_price = (settings[0].minFare_bike).toString() + ' Tk';
          sendValue.cost_per_min = (settings[0].costPerMin_bike).toString() + ' Tk';
          sendValue.waiting_charge_per_min = (settings[0].waitingCharge_bike).toString() + ' Tk';
          sendValue.capacity = (settings[0].capacity_bike).toString() + ' Person';
          break;
        case '1':
          sendValue.base_price = (settings[0].minFare_cng).toString() + ' Tk';
          sendValue.cost_per_min = (settings[0].costPerMin_cng).toString() + ' Tk';
          sendValue.waiting_charge_per_min = (settings[0].waitingCharge_cng).toString() + ' Tk';
          sendValue.capacity = (settings[0].capacity_cng).toString() + ' Persons';
          break;
        case '2':
          sendValue.base_price = (settings[0].minFare_car).toString() + ' Tk';
          sendValue.cost_per_min = (settings[0].costPerMin_car).toString() + ' Tk';
          sendValue.waiting_charge_per_min = (settings[0].	waitingCharge_car).toString() + ' Tk';
          sendValue.capacity = (settings[0].capacity_car).toString() + ' Persons';
          break;
        case '3':
          sendValue.weight_limit1 = '1-' + (settings[0].capacity_courier_1).toString() + ' kg';
          sendValue.weight_limit2 = (settings[0].capacity_courier_1 + 1).toString() + '-' + (settings[0].capacity_courier_2).toString() + ' kg';
          sendValue.weight_limit3 = (settings[0].capacity_courier_2 + 1).toString() + '-' + (settings[0].capacity_courier_3).toString() + ' kg';
          sendValue.weight_limit4 = (settings[0].capacity_courier_3 + 1).toString() + '-' + (settings[0].capacity_courier_4).toString() + ' kg';
          sendValue.base_price1 = (settings[0].minFare_courier_1).toString() + ' Tk';
          sendValue.base_price2 = (settings[0].minFare_courier_2).toString() + ' Tk';
          sendValue.base_price3 = (settings[0].minFare_courier_3).toString() + ' Tk';
          sendValue.base_price4 = (settings[0].minFare_courier_4).toString() + ' Tk';
          break;
        default:
          sendValue.base_price = (settings[0].minFare_bike).toString() + ' Tk';
          sendValue.cost_per_min = (settings[0].costPerMin_bike).toString() + ' Tk';
          sendValue.waiting_charge_per_min = (settings[0].waitingCharge_bike).toString() + ' Tk';
          sendValue.capacity = (settings[0].capacity_bike).toString() + ' Person';
      }
      sendValue.status = 'success';
      res.json(sendValue);
    })
    .catch(function(err) {
      errorMessage.sendErrorMessage(err, res);
    });
  }
});

module.exports = router;
