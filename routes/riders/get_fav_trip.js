var express            = require('express')
  , success_trip_model = require('../../models/success_trips')
  , datetime           = require('../../helpers/datetime')
  , errorMessage       = require('../../helpers/errormessage')
  , router             = express.Router();

/** POST /api/rider/deleteFavTrip - delete a perticular trip from favorite list.
 * @property {string} req.body.rider_id - id of a rider/user.
 * @returns {status, biker, biker_rating, ride, total_time, distance}<=(success)
 * @returns {status, trip_status, total_bill, total_trip_time, cost_per_min, base_price, screenshot_url}<=(trip_not_found)
 * @returns {status, message}<=(error).
 */
router.post('/', function(req, res) {
  if(!req.body.rider_id) {
    errorMessage.sendErrorMessage('Something went wrong! Please try again.', res);
  } else {
    var rider_id = req.body.rider_id;

    success_trip_model.findFavWithRiderId(rider_id)
    .then(function(successTrips) {
      var favorites = [];
      if(successTrips != ''){
        successTrips.sort(function(a, b){return b.id-a.id});
        favorites = successTrips.map(function(entry){
          return {
            trip_id: entry.trip_id.toString(),
            trip_start: datetime.getDateOnly(entry.trip_start).toString(),
            trip_end: datetime.getDateOnly(entry.trip_end).toString(),
            from_area: entry.from_area.toString(),
            to_area: entry.to_area.toString()
          }
        });
      }
      res.json({
        status: 'success',
        favorites: favorites
      });
    })
    .catch(function(err) {
      errorMessage.sendErrorMessage(err, res);
    });
  }
});

module.exports = router;
