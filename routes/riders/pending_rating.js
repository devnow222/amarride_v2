var express            = require('express')
  , biker_profile      = require('../../config/config').env.biker_profile
  , biker_model        = require('../../models/bikers')
  , rider_model        = require('../../models/riders')
  , rating_model       = require('../../models/ratings')
  , setting_model      = require('../../models/settings')
  , success_trip_model = require('../../models/success_trips')
  , datetime           = require('../../helpers/datetime')
  , errorMessage       = require('../../helpers/errormessage')
  , router             = express.Router();

// Rider checks if there is any pending rating
// 90% done - not tested - not documented
router.post('/', function(req, res) {
  if(!req.body.rider_id || !req.body.device_id) {
    errorMessage.sendErrorMessage('Something went wrong! Please try again.', res);
  } else {
    var rider_id    = req.body.rider_id
      , device_id   = req.body.device_id
      , sendToRider = {};

    rider_model.findWithId(rider_id)
    .then(function(rider) {
      if(rider && rider[0].device_id == device_id) {
        success_trip_model.findPendingWithRiderId(rider_id)
        .then(function(successTrips) {
          var biker_id = successTrips[0].biker_id;
          sendToRider.trip_id = successTrips[0].trip_id;
          return biker_model.findWithBikerId(biker_id);
        })
        .then(function(bikers) {
          sendToRider.biker = {
            biker_id     : bikers[0].id.toString(),
            full_name    : bikers[0].full_name.toString(),
            phone_num    : '+' + bikers[0].phone_num.toString(),
            biker_photo  : biker_profile + bikers[0].user_photo,
            biker_email  : bikers[0].email.toString()
          };
          sendToRider.ride = {
            ride_model: bikers[0].ride_model,
            ride_sl_num: bikers[0].ride_serial
          };
          sendToRider.biker_rating = (bikers[0].rating / 10).toString();
          return setting_model.findAll();
        })
        .then( function(settings) {
          sendToRider.service_time = "AmarRide service is available\n" + datetime.getHourMinuteOnly(settings[0].start_time) + " - " + datetime.getHourMinuteOnly(settings[0].end_time) + ".";
          var curTime = new Date();
          var curMilisecond = curTime.getTime();
          if(curMilisecond >= 1484832972666 && curMilisecond <= 1484927999000) {
            sendToRider.service_time = "AmarRide is currently undergoing maintenance. We will be available on Saturday 8:00 AM.";
          }
          // 1484841599000

          // 1484832972666

          // 1484927999000
          sendToRider.blocked_services = JSON.parse(settings[0].blocked_services);
          sendToRider.status = 'success';
          console.log(sendToRider);
          res.json(sendToRider);
        })
        .catch(function(err) {
          //errorMessage.sendErrorMessage(err, res);
          setting_model.findAll()
          .then(function(settings) {
            var service_time = "AmarRide service is available\n" + datetime.getHourMinuteOnly(settings[0].start_time) + " - " + datetime.getHourMinuteOnly(settings[0].end_time) + ".";
            var curTime = new Date();
            var curMilisecond = curTime.getTime();
            if(curMilisecond >= 1484832731899 && curMilisecond <= 1484927999000) {
              service_time = "AmarRide is currently undergoing maintenance. We will be available on Saturday 8:00 AM.";
            }
            res.json({
              status: 'error',
              service_time: service_time,
              blocked_services: JSON.parse(settings[0].blocked_services),
              message: err
            })
          })
          .catch(function(err) {
            throw err;
          })
        });
      } else {
        res.json({ status: 'logout' });
      }
    })
    .catch(function(err) {
      errorMessage.sendErrorMessage(err, res);
    });
  }
});

module.exports = router;
