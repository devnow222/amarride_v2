var express      = require('express')
  , md5          = require('md5')
  , rider_model  = require('../../models/riders')
  , errorMessage = require('../../helpers/errormessage')
  , router       = express.Router();

/** POST /api/rider/changePassword - change rider's/ user's current password.
 * @property {string} req.body.email - email address of a rider/user.
 * @property {string} req.body.prev_pass - old password of a rider/user.
 * @property {string} req.body.new_pass - new password of a rider/user.
 * @returns (success)=>{status, message}//(error)=>{status, message}.
 */
router.post('/', function(req, res){
  if(!req.body.email || !req.body.prev_pass || !req.body.new_pass) {
    errorMessage.sendErrorMessage('Please provide all info.', res);
  } else {
    var email     = req.body.email
      , prev_pass = md5(req.body.prev_pass)
      , new_pass  = md5(req.body.new_pass);

    rider_model.findWithEmail(email)
    .then(function(riders) {
      if(riders[0].reg_code == 0){
        errorMessage.sendErrorMessage('not loggedIn', res);
      } else if(riders[0].password != prev_pass) {
        errorMessage.sendErrorMessage('password didn\'t match', res);
      } else {
        var update = { password: new_pass };
        rider_model.updateWithEmail(update, email)
        .then(function(result) {
          if(result) {
            res.json({
              status: 'success',
              message: 'Please login with your new password.'
            });
          }
        })
        .catch(function(err) {
          errorMessage.sendErrorMessage(err, res);
        });
      }
    })
    .catch(function(err) {
      errorMessage.sendErrorMessage(err, res);
    });
  }
});

module.exports = router;
