var express           = require('express')
  , wallet_model      = require('../../models/wallets')
  , deposit_model     = require('../../models/deposits')
  , transaction_model = require('../../models/transactions')
  , bkash             = require('../../helpers/bkash')
  , errorMessage      = require('../../helpers/errormessage')
  , router            = express.Router();

/** POST /api/rider/addFavTrip - add a perticular trip to favorite list.
 * @property {string} req.body.trip_id - The trip_id of a trip.
 * @returns (success)=>{status, message}//(error)=>{status, message}.
 */
router.post('/', function(req, res) {
  if(!req.body.transaction_id || !req.body.rider_id) {
    errorMessage.sendErrorMessage('Something went wrong! Please try again.', res);
  } else {
    var rider_id       = req.body.rider_id
      , transaction_id = req.body.transaction_id
      , transaction    = {};

    transaction_model.findWithTransactionId(transaction_id)
    .then(function(result) {
      errorMessage.sendErrorMessage('This transaction ID has already been used. Please try using a new one.', res);
    })
    .catch(function(err) {
      if(err == 'transaction_not_found') {
        bkash.checkTransaction(transaction_id)
        .then(function(transactions) {
          transaction = transactions;
          return wallet_model.findWithRiderId(rider_id);
        })
        .then(function(wallet) {
          var balance = wallet[0].balance + parseFloat(transaction.amount)
            , update  = { balance: balance }
            , value   = {
              wallet_id: wallet[0].id,
              amount: transaction.amount,
              source: 'bkash',
              code: transaction.trxId
            };
          deposit_model.add(value);
          return wallet_model.updateWithRiderId(update, rider_id);
        })
        .then(function(result) {
          var value = {
            amount : transaction.amount,
            currency: transaction.currency,
            transaction_id: transaction.trxId,
            reference: transaction.reference,
            sender: transaction.sender,
            receiver: transaction.receiver,
            service: transaction.service,
            transaction_time: transaction.trxTimestamp,
            rider_id: rider_id
          }
          return transaction_model.add(value);
        })
        .then(function(result) {
          var message = transaction.amount + ' ' + transaction.currency + ' has been added to your wallet';
          res.json({ status: 'success', message: message });
        })
        .catch(function(err) {
          errorMessage.sendErrorMessage(err, res);
        })
      } else {
        errorMessage.sendErrorMessage('Something went wrong! Please try again.', res);
      }
    });
  }
});

module.exports = router;
