var express            = require('express')
  , trip_model         = require('../../models/trips')
  , rider_model        = require('../../models/riders')
  , biker_model        = require('../../models/bikers')
  , setting_model      = require('../../models/settings')
  , warning_model      = require('../../models/warnings')
  , temptrip_model     = require('../../models/temptrips')
  , request_pole_model = require('../../models/request_poles')
  , temp_warning_model = require('../../models/temp_warnings')
  , push               = require('../../helpers/push')
  , distance           = require('../../helpers/distance')
  , datetime           = require('../../helpers/datetime')
  , errorMessage       = require('../../helpers/errormessage')
  , riderauth          = require('../../middlewares/riderauth')
  , sms                = require('../../helpers/sms')
  , router             = express.Router();

// Rider search for nearest available biker
// 100% done - tested - documented
// router.use(function(req, res, next) {
//   console.log(' ');
//   console.log(' ');
//   console.log(' ');
//   console.log('=====================================================================================');
//   console.log(req.url, req.method, new Date());
//   console.log(req.body);
//   console.log('_____________________________________________________________________________________');
//   next();
// });
router.post('/', riderauth.tokenauth, function(req, res){
  if(!req.body.current_lat || !req.body.current_long || !req.body.end_lat || !req.body.end_long || !req.body.to_area || !req.body.rider_id || !req.body.biker_type) {
    errorMessage.sendErrorMessage('Something went wrong! Please try again.', res);
  } else {
    trip_model.findPreviousWithRiderId(req.body.rider_id)
    .then(function(trip) {
      errorMessage.sendErrorMessage('You are already in a trip', res);
    })
    .catch(function(err) {
      temptrip_model.findWithRiderId(req.body.rider_id)
      .then(function(temptrip) {
        errorMessage.sendErrorMessage('Your previous request yet to be processed. Please try after sometime.', res);
      })
      .catch(function(err) {
        var curTime = new Date();
        var curMilisecond = curTime.getTime();
        if (curMilisecond >= 1484832972666 && curMilisecond <= 1484927999000) {
          errorMessage.sendErrorMessage('AmarRide is currently undergoing maintenance. We will be available on Saturday 8:00 AM.', res);
        } else {
          var rider_current_lat   = req.body.current_lat
            , rider_current_long  = req.body.current_long
            , rider_start_lat     = req.body.start_lat
            , rider_start_long    = req.body.start_long
            , rider_end_lat       = req.body.end_lat
            , rider_end_long      = req.body.end_long
            , from_area           = req.body.from_area
            , to_area             = req.body.to_area
            , rider_id            = req.body.rider_id
            , is_courier          = req.body.biker_type == 3 ? 1 : 0
            , biker_type          = req.body.biker_type == 3 ? 0 : req.body.biker_type
            , promocode           = req.body.promocode || 0
            , courier_type        = req.body.courier_type || ''
            , courier_weight      = Math.ceil(req.body.weight) || 0
            , note_to_driver      = req.body.note_to_driver || ''
            , receiver_name       = req.body.receiver_name || ''
            , receiver_phone      = req.body.receiver_phone || ''
            , nearestBikers       = []
            , requestArea         = 5
            , block_count         = 3
            , rider               = {}
            , sendToBiker         = {}
            , sendToRider         = {};

          if(rider_start_lat == null || rider_start_lat == undefined || rider_start_lat == ''|| rider_start_long == null || rider_start_long == undefined || rider_start_long == ''){
            rider_start_lat = rider_current_lat;
            rider_start_long = rider_current_long;
          }

          setting_model.findAll()
          .then(function(settings) {
            var today = datetime.getTime();
            if (today >= settings[0].start_time && today <= settings[0].end_time) {
              switch(biker_type) {
                case '0':
                  requestArea = settings[0].requestArea_bike;
                  break;
                case '1':
                  requestArea = settings[0].requestArea_cng;
                  break;
                case '2':
                  requestArea = settings[0].requestArea_car;
                  break;
                default:
                  requestArea = settings[0].requestArea_bike;
              }
              //requestArea = settings[0].bikerRequestArea;
              block_count = settings[0].block_count;
              return rider_model.findWithId(rider_id);
            } else {
                throw 'Service unavailable! Please try to request from ' + datetime.getHourMinuteOnly(settings[0].start_time) + " to " + datetime.getHourMinuteOnly(settings[0].end_time) + ".";
            }
          })
          .then(function(rider) {
            if(rider[0].cancel_count >= block_count) {
              errorMessage.sendErrorMessage('You have been blocked from AmarRide for cancelling too many trips. Please call 01955445555 for detail.', res);
            } else if(biker_type == '1') {
              errorMessage.sendErrorMessage('"Amar CNG" is not available.', res);
            } else {
              sendToBiker.rider = {
                rider_id: rider_id,
                full_name: rider[0].full_name,
                ratingValue: (rider[0].rating / 10).toString()
              };

              biker_model.findAllAvailable(biker_type)
              .then(function(bikers) {
            	  var idle_bikers=[];
                var idle_bikers_phones=[];
                for(var i = 0; i < bikers.length; i++) {
                  var mDistance = distance.getDistanceFromLatLonInKm(rider_start_lat, rider_start_long, bikers[i].current_latitude, bikers[i].current_longitude);
                  var currentTimeInMillisecond = new Date().getTime();
                  var timeDifference = currentTimeInMillisecond - bikers[i].timestamp;
                  console.log("condition");
                  
                  if (timeDifference > 600000 )
                  	{ 
                	  idle_bikers.push(bikers[i].id);
                    if(bikers[i].membership_type==0)
                    {
                       idle_bikers_phones.push(bikers[i].phone_num);

                    }
                   
                	 }
                  if(mDistance <= requestArea && timeDifference < 60000 && currentTimeInMillisecond <= bikers[i].expirydate) {
                	 
                    nearestBikers.push({
                      biker_id: bikers[i].id,
                      reg_code: bikers[i].reg_code,
                      distance: mDistance.toString()
                    });
                  }
                }
                if(idle_bikers.length)
                {
               var  message="You have been idol from the system for network connectivity or gps problem. Please kill and restart your app.";

                	biker_model.idleActiveBiker({current_status: 2 },idle_bikers);

                 idle_bikers_phones.forEach(function(value, index, array) {
                      sms.sendBulk(value,message);
                    });
                	
                }
                nearestBikers.sort(function(a, b){return a.distance-b.distance});
                console.log(nearestBikers);

                sendToBiker.to_area    = to_area;
                sendToBiker.from_area  = from_area;
                sendToBiker.is_courier = is_courier;

                var value = {
                  rider_id: rider_id,
                  trip_type: req.body.biker_type,
                  start_longitude: rider_start_long,
                  start_latitude: rider_start_lat,
                  from_area: from_area,
                  current_longitude: rider_current_long,
                  current_latitude: rider_current_lat,
                  end_longitude: rider_end_long,
                  end_latitude: rider_end_lat,
                  to_area: to_area,
                  is_courier: is_courier,
                  promocode: promocode,
                  courier_type: courier_type,
                  courier_weight: courier_weight,
                  note_to_driver: note_to_driver,
                  receiver_name: receiver_name,
                  receiver_phone: receiver_phone
                };
                var pole_value = {
                  rider_id: rider_id,
                  trip_type: req.body.biker_type,
                  from_area: from_area,
                  to_area: to_area,
                }
                var update = {
                  current_latitude: rider_current_lat,
                  current_longitude: rider_current_long
                }
                request_pole_model.add(pole_value);
                rider_model.updateWithId(update, rider_id);
                return temptrip_model.add(value);
              })
              .then(function(result) {
                sendToRider.temptrip_id = result.insertId.toString();
                sendToBiker.temptrip_id = result.insertId.toString();

                if(nearestBikers.length > 0) {
                  var reg_code = [];
                  var biker_warning = [];
                  for(var i = 0; i < nearestBikers.length; i++) {
                    reg_code.push(nearestBikers[i].reg_code);
                    biker_warning.push(nearestBikers[i].biker_id);
                  }
                  var value = {
                    temptrip_id: sendToRider.temptrip_id,
                    biker_id: JSON.stringify(biker_warning)
                  }
                  temp_warning_model.add(value);
                  push.forceNotification(reg_code, 'Request', sendToBiker, function(err, data) {
                    if(err) console.log("notification error" + err);
                    else console.log("request notification sent");
                  });
                }
                sendToRider.status = 'success';
                res.json(sendToRider);
                setTimeout(function() {
                  temptrip_model.findWithTemptripId(sendToRider.temptrip_id)
                  .then(function(temptrip) {
                    if(temptrip) {
                      temptrip_model.deleteWithRiderId(rider_id);
                      
                      if(rider[0].os_type == 'Android') {
                        push.forceNotification([rider[0].reg_code], 'Busy', 'Sorry! the drivers are busy. Please try again later.', function(err) {
                          if(err) console.log("error : " + err);
                          else console.log("busy notification sent");
                        });
                      } else {
                        push.sendNotification([rider[0].reg_code], 'Busy', 'Sorry! the drivers are busy. Please try again later.', function(err) {
                          if(err) console.log("error : " + err);
                          else console.log("busy notification sent");
                        });
                      }
                     
                    }
                  });

                  temp_warning_model.findWithTemptripId(sendToRider.temptrip_id)
                  .then(function(temp_warning) {
                    var biker_warning = JSON.parse(temp_warning[0].biker_id);
                    biker_warning.forEach(function(biker_warning_id) {
                      var value = {
                        type: 'Request not accepted',
                        biker_id: biker_warning_id
                      }
                      warning_model.add(value);
                    });
                    temp_warning_model.deleteWithTemptripId(sendToRider.temptrip_id);
                  });
                }, 45 * 1000);
              })
              .catch(function(err) {
                errorMessage.sendErrorMessage(err, res);
              });
            }
          })
          .catch(function(err) {
            errorMessage.sendErrorMessage(err, res);
          });
        }
      });
    });
  }
});

module.exports = router;
