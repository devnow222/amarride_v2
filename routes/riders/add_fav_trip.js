var express            = require('express')
  , success_trip_model = require('../../models/success_trips')
  , errorMessage       = require('../../helpers/errormessage')
  , router             = express.Router();

/** POST /api/rider/addFavTrip - add a perticular trip to favorite list.
 * @property {string} req.body.trip_id - The trip_id of a trip.
 * @returns (success)=>{status, message}//(error)=>{status, message}.
 */
router.post('/', function(req, res) {
  if(!req.body.trip_id) {
    errorMessage.sendErrorMessage('Something went wrong! Please try again.', res);
  } else {
    var trip_id = req.body.trip_id
      , update  = { favid: 1 };

    success_trip_model.updateWithTripId(update, trip_id)
    .then(function(result) {
      if(result.changedRows > 0){
        res.json({
          status: 'success',
          message: 'added to favorite'
        });
      } else {
        errorMessage.sendErrorMessage('already favorite', res);
      }
    })
    .catch(function(err) {
      errorMessage.sendErrorMessage(err, res);
    });
  }
});

module.exports = router;
