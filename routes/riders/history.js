var express            = require('express')
  , success_trip_model = require('../../models/success_trips')
  , datetime           = require('../../helpers/datetime')
  , errorMessage       = require('../../helpers/errormessage')
  , router             = express.Router();

// Show history of a rider
// 100% done - not tested - documented
router.post('/', function(req, res) {
  if(!req.body.rider_id) {
    errorMessage.sendErrorMessage('Something went wrong! Please try again.', res);
  } else {
    var rider_id = req.body.rider_id;

    success_trip_model.findWithRiderId(rider_id)
    .then(function(successTrips) {
      var history = [];
      if(successTrips != ''){
        successTrips.sort(function(a, b){return b.id-a.id});
        history = successTrips.map(function(entry){
          return {
            trip_id: entry.trip_id.toString(),
            trip_start: datetime.getDateOnly(entry.trip_start).toString(),
            trip_end: datetime.getDateOnly(entry.trip_end).toString(),
            from_area: entry.from_area.toString(),
            to_area: entry.to_area.toString()
          }
        });
      }
      res.json({
        status: 'success',
        history: history
      });
    })
    .catch(function(err) {
      errorMessage.sendErrorMessage(err, res);
    });
  }
});

module.exports = router;
