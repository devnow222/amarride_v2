var express       = require('express')
  , md5           = require('md5')
  , rider_model   = require('../../models/riders')
  , wallet_model  = require('../../models/wallets')
  , deposit_model = require('../../models/deposits')
  , setting_model = require('../../models/settings')
  , sms           = require('../../helpers/sms')
  , image         = require('../../helpers/image')
  , random        = require('../../helpers/random')
  , errorMessage  = require('../../helpers/errormessage')
  , router        = express.Router();

router.post('/', function(req, res) {
  if(!req.body.phone_num || !req.body.password || !req.body.full_name || !req.body.email || !req.body.image ) {
    errorMessage.sendErrorMessage('All fields are not set! Please make sure you are sending all information.', res);
  } else {
    var email          = (req.body.email.replace(/ /g,'')).toLowerCase()
      , phone_num      = req.body.phone_num
      , password       = md5(req.body.password)
      , full_name      = req.body.full_name
      , age            = req.body.age ? req.body.age : '0'
      , gender         = req.body.gender ? req.body.gender : '3'
      , base64         = req.body.image
      , initialBalance = 0;

    rider_model.findWithEmail(email)
    .then(function(rider) {
      errorMessage.sendErrorMessage('Email address is already in use. Please try using a new one.', res);
    })
    .catch(function(err) {
      rider_model.findWithPhone(phone_num)
      .then(function(rider) {
        errorMessage.sendErrorMessage('Phone number is already in use. Please try using different one.', res);
      })
      .catch(function(err) {
        var verificationCode = random.verificationCode();
        setting_model.findAll()
        .then(function(settings) {
          initialBalance = settings[0].signUpBonus;
          return image.saveRiderImage(base64);
        })
        .then(function(imageName) {
          var value = {
            full_name: full_name,
            age: age,
            gender: gender,
            user_photo: imageName,
            email: email,
            password: password,
            current_latitude: '23.7936765',
            current_longitude: '90.4049176',
            phone_num: phone_num,
            verification_code: verificationCode
          }
          sms.sendVerification(phone_num, verificationCode);
          return rider_model.add(value)
        })
        .then(function(result) {
          var value = {
            rider_id: result.insertId,
            balance: initialBalance,
            spent: 0
          }
          return wallet_model.add(value);
        })
        .then(function(result) {
          if(initialBalance > 0) {
            var value   = {
              wallet_id: result.insertId,
              amount: initialBalance,
              source: 'signup_gift',
              code: 'na'
            };
            deposit_model.add(value);
          }
          var phone = phone_num.substr(phone_num.length - 2);
          var message = 'Signup successful. A verification code has been sent to: ***** ****' + phone;
          res.json({ status: 'success', message: message });
        })
        .catch(function(err) {
          errorMessage.sendErrorMessage(err, res);
        })
      })
    });
  }
});

module.exports = router;
