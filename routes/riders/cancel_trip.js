var express             = require('express')
  , trip_model          = require('../../models/trips')
  , biker_model         = require('../../models/bikers')
  , rider_model         = require('../../models/riders')
  , setting_model       = require('../../models/settings')
  , temptrip_model      = require('../../models/temptrips')
  , temp_warning_model  = require('../../models/temp_warnings')
  , canceled_trip_model = require('../../models/canceled_trips')
  , push                = require('../../helpers/push')
  , errorMessage        = require('../../helpers/errormessage')
  , riderauth           = require('../../middlewares/riderauth')
  , router              = express.Router();

/** POST /api/rider/cancelTrip - cancel the current trip.
 * @property {string} req.body.rider_id - The id of a rider/user.
 * @returns (success)=>{status}//(error)=>{status, message}.
 * @notification {Cancelled} - sends biker a cancel notification.
 */
router.post('/', riderauth.tokenauth, function(req, res){
  if(!req.body.rider_id) {
    errorMessage.sendErrorMessage('Something went wrong! Please try again.', res);
  } else {
    var rider_id    = req.body.rider_id
      , temptrip_id = req.body.temptrip_id
      , force       = req.body.force ? req.body.force : 0
      , biker_id    = null
      , message     = 'Successfully cancelled.'
      , setting     = {};

    // if(temptrip_id)
    //   temp_warning_model.deleteWithTemptripId(temptrip_id);
    //

    temptrip_model.deleteWithRiderId(rider_id)
    .then(function (result) {
      if(result) res.json({ status: 'success' });
    })
    .catch(function (err) {
      setting_model.findAll()
      .then(function(settings) {
        setting = settings[0];
        return rider_model.findWithId(rider_id);
      })
      .then(function(riders) {
        if(force != 1 && riders[0].cancel_count == setting.block_count - 1) {
          message = 'You already cancelled ' + (riders[0].cancel_count == 1 ? ' once' : riders[0].cancel_count + ' times') + '. You will be bocked for ever if you cancel now. Do you want to preceed?'
          res.json({ status: 'force_cancel', message: message });
        } else {
          if(force != 1 && riders[0].cancel_count == setting.block_count - 2)
            message = 'You already cancelled ' + (riders[0].cancel_count + 1 == 1 ? ' once' : (riders[0].cancel_count + 1) + ' times') + '. You will be bocked for ever if you cancel next time.'
          var update = {
            cancel_count: riders[0].cancel_count + 1
          }
          rider_model.updateWithId(update, rider_id);
          return trip_model.findWithRiderId(rider_id)
        }
      })
      .then(function (trip) {
        if(trip) {
          if(trip[0].trip_status != 'Started') {
            biker_id = trip[0].biker_id;
            var value = {
              biker_id: trip[0].biker_id,
              rider_id: trip[0].rider_id,
              trip_type: trip[0].trip_type,
              cancelled_by: 1,
              rider_latitude: trip[0].start_latitude,
              rider_longitude: trip[0].start_longitude,
              from_area: trip[0].from_area,
              to_area: trip[0].to_area,
              trip_status: trip[0].trip_status,
              cancel_reason: ''
            };
            canceled_trip_model.add(value);
            return trip_model.deleteWithRiderId(rider_id);
          } else {
            errorMessage.sendErrorMessage('You can\'t cancel now. Biker has already started the trip.', res);
          }
        }
      })
      .then(function (result) {
        if(result){
          return biker_model.findWithBikerId(biker_id);
        }
      })
      .then(function (biker) {
        if(biker) {
          biker_model.updateWithBikerId({ current_status: 2 }, biker_id);
          var reg_code = [biker[0].reg_code];
          push.forceNotification(reg_code, 'Cancelled', '', function (err) {
            if (err) console.log("error" + err);
            else console.log("cancel notification sent to biker");
          });
          res.json({ status: 'success', message: message });
        }
      })
      .catch(function (err) {
        errorMessage.sendErrorMessage(err, res);
      });
    });
  }
});

module.exports = router;
