var express      = require('express')
  , setting_model= require('../../models/settings')
  , errorMessage = require('../../helpers/errormessage')
  , googlemaps   = require('../../helpers/googlemaps')
  , router       = express.Router();

/** POST /api/rider/deleteFavTrip - delete a perticular trip from favorite list.
 * @property {string} req.body.from_area - area from where rider/user wants a trip.
 * @property {string} req.body.to_area - where rider/user wants to go.
 * @property {string} req.body.vehicle_type - type of veficle rider/user needs.
 * @returns (success)=>{status, minDuration, maxDuration, minCost, maxCost}//(error)=>{status, message}.
 */
router.post('/', function(req, res) {
  if(!req.body.from_area || !req.body.to_area || !req.body.vehicle_type) {
    errorMessage.sendErrorMessage('Something went wrong! Please try again.', res);
  } else {
    var from_area   = req.body.from_area
      , to_area     = req.body.to_area
      , biker_type  = req.body.vehicle_type
      , duration    = 0;

    googlemaps.getDistanceAndTime(from_area, to_area)
    .then(function(value) {
      console.log("==========================================================================");
      console.log(value);
      console.log("==========================================================================");
      if(value.destination_addresses == '' || value.origin_addresses == ''){
        res.json({
          status: 'success',
          minDuration: '0 Min',
          maxDuration: '0 Min',
          minCost: '0 Tk',
          maxCost: '0 Tk',
          baseFare: '0 Tk',
          costPerMin: '0 Tk'
        });
      } else {
        duration = Math.ceil(value.rows[0].elements[0].duration.value / 60);
        return setting_model.findAll();
      }
    })
    .then(function(settings) {
      if(settings) {
        var minDuration = 0
          , maxDuration = 0
          , minCost     = 0
          , maxCost     = 0
          , baseFare    = 0
          , costPerMin  = 0;

        switch(biker_type) {
          case '0':
            minDuration = duration;
            maxDuration = (minDuration / 100) * 140;
            minCost = (parseFloat(minDuration) * settings[0].costPerMin_bike) + settings[0].minFare_bike;
            maxCost = (parseFloat(maxDuration) * settings[0].costPerMin_bike) + settings[0].minFare_bike;
            baseFare = settings[0].minFare_bike;
            costPerMin = settings[0].costPerMin_bike;
            break;
          case '1':
            minDuration = (duration / 100) * 110;
            maxDuration = (parseFloat(minDuration) / 100) * 140;
            minCost = (parseFloat(minDuration) * settings[0].costPerMin_cng) + settings[0].minFare_cng;
            maxCost = (parseFloat(maxDuration) * settings[0].costPerMin_cng) + settings[0].minFare_cng;
            baseFare = settings[0].minFare_cng;
            costPerMin = settings[0].costPerMin_cng;
            break;
          case '2':
            minDuration = (duration / 100) * 120;
            maxDuration = (parseFloat(minDuration) / 100) * 140;
            minCost = (parseFloat(minDuration) * settings[0].costPerMin_car) + settings[0].minFare_car;
            maxCost = (parseFloat(maxDuration) * settings[0].costPerMin_car) + settings[0].minFare_car;
            baseFare = settings[0].minFare_car;
            costPerMin = settings[0].costPerMin_car;
            break;
          case '3':
            minDuration = duration;
            maxDuration = (duration / 100) * 140;
            costPerMin = 0;
            console.log(req.body.weight);

            if (req.body.weight <= settings[0].capacity_courier_1) {
              baseFare = (settings[0].minFare_courier_1).toString();
            } else if (req.body.weight <= settings[0].capacity_courier_2) {
              baseFare = (settings[0].minFare_courier_2).toString();
            } else if (req.body.weight <= settings[0].capacity_courier_3) {
              baseFare = (settings[0].minFare_courier_3).toString();
            } else {
              baseFare = (settings[0].minFare_courier_4).toString();
            }
            minCost = baseFare;
            maxCost = baseFare;
            break;
        }

        res.json({
          status: 'success',
          minDuration: parseInt(minDuration).toString() + ' Min',
          maxDuration: parseInt(maxDuration).toString() + ' Min',
          minCost: parseInt(minCost).toString() + ' Tk',
          maxCost: parseInt(maxCost).toString() + ' Tk',
          baseFare: baseFare.toString() + ' Tk',
          costPerMin: costPerMin.toString() + ' Tk'
        });
      }
    })
    .catch(function(err) {
      errorMessage.sendErrorMessage(err, res);
    });
  }
});

module.exports = router;
