var express            = require('express')
  , success_trip_model = require('../../models/success_trips')
  , errorMessage       = require('../../helpers/errormessage')
  , router             = express.Router();

/** POST /api/rider/deleteFavTrip - delete a perticular trip from favorite list.
 * @property {string} req.body.trip_id - id of a trip.
 * @returns {status, message}<=(success)//{status, message}<=(error).
 */
router.post('/', function(req, res) {
  if(!req.body.trip_id) {
    errorMessage.sendErrorMessage('Something went wrong! Please try again.', res);
  } else {
    var trip_id = req.body.trip_id;

    success_trip_model.deleteFavWithTripId(trip_id)
    .then(function(result) {
      if(result.changedRows > 0){
        res.json({
          status: 'success',
          message: 'deleted from favorite'
        });
      } else {
        errorMessage.sendErrorMessage('not yet favorite', res);
      }
    })
    .catch(function(err) {
      errorMessage.sendErrorMessage(err, res);
    });
  }
});

module.exports = router;
