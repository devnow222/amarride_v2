var express      = require('express')
  , md5          = require('md5')
  , rider_model  = require('../../models/riders')
  , sms          = require('../../helpers/sms')
  , mailer       = require('../../helpers/mailer')
  , random       = require('../../helpers/random')
  , errorMessage = require('../../helpers/errormessage')
  , router       = express.Router();

/** POST /api/rider/redeemPromo - add a perticular trip to favorite list.
 * @property {string} req.body.trip_id - The trip_id of a trip.
 * @returns (success)=>{status, message}//(error)=>{status, message}.
 */
router.post('/', function(req, res) {
  if(!req.body.email || !req.body.send_options) {
    errorMessage.sendErrorMessage('Something went wrong! Please try again.', res);
  } else {
    var email        = req.body.email
      , send_options = req.body.send_options
      , rider        = {}
      , randomPass   = null
      , sendMessage  = '';

    rider_model.findWithEmail(email)
    .then(function(riders) {
      rider = riders[0];
      randomPass = random.password();
      switch(send_options){
        case '0':
          sendMessage = 'An email with your new password has been sent to: ' + email;
          mailer.sendPassword(email, riders[0].full_name, randomPass, function(err){
            if(err) console.log(err);
            else console.log('password email success');
          });
          break;
        case '1':
          var phone = riders[0].phone_num.substr(riders[0].phone_num.length - 2);
          sendMessage = 'A text message with your new password has been sent to: ***** ****' + phone;
          sms.sendPassword(riders[0].phone_num, randomPass);
          break;
        default:

      }
      var password = md5(randomPass);
      var update = { password: password };
      return rider_model.updateWithEmail(update, email);
    })
    .then(function(result) {
      res.json({ status: 'success', message: sendMessage });
    })
    .catch(function(err) {
      errorMessage.sendErrorMessage(err, res);
    })
  }
});

module.exports = router;
