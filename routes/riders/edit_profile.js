var express       = require('express')
  , jwt           = require('jwt-simple')
  , secret        = require('../../config/config').env.secret
  , rider_model   = require('../../models/riders')
  , image         = require('../../helpers/image')
  , random        = require('../../helpers/random')
  , errorMessage  = require('../../helpers/errormessage')
  , router        = express.Router();

/** POST /api/rider/addFavTrip - add a perticular trip to favorite list.
 * @property {string} req.body.trip_id - The trip_id of a trip.
 * @returns (success)=>{status, message}//(error)=>{status, message}.
 */
router.post('/', function(req, res) {
  if(!req.body.rider_id) {
    errorMessage.sendErrorMessage('Something went wrong! Please try again.', res);
  } else {
    var rider_id       = req.body.rider_id
      , email          = (req.body.email.replace(/ /g,'')).toLowerCase()
      , phone_num      = req.body.phone_num
      , full_name      = req.body.full_name
      , base64Image    = req.body.image
      , rider          = {}
      , token          = null;

    rider_model.findWithId(rider_id)
    .then(function(riders) {
      rider = riders[0];
      if(base64Image != '')
        return image.saveRiderImage(base64Image);
      else
        return '';
    })
    .then(function(imageName) {
      var update = {};

      if(full_name != '')
        update.full_name = full_name;
      if(imageName != '')
        update.user_photo = imageName;
      if(email != '')
        update.email = email;
      if(phone_num != '')
        update.phone_num = phone_num;

      var makeToken = {
        email: email != '' ? email : rider.email,
        password: rider.password
      }
      token = jwt.encode(makeToken, secret);

      console.log(update);

      if(imageName != '') {
        var userPrevPhotos = rider.user_prev_photo;
        var allPhotos = [];
        if(userPrevPhotos != '')
          allPhotos = JSON.parse(userPrevPhotos);
        allPhotos.push(rider.user_photo);
        // image.deleteImage(rider.user_photo);
        update.user_prev_photo = JSON.stringify(allPhotos);
      }

      if(update.full_name || update.user_photo || update.email || update.phone_num)
        return rider_model.updateWithId(update, rider_id);
    })
    .then(function(result) {
      res.json({ status: 'success', token: token });
    })
    .catch(function(err) {
      errorMessage.sendErrorMessage(err, res);
    });
  }
});



module.exports = router;
