var express             = require('express')
  , push                = require('../../helpers/push')
  , mailer              = require('../../helpers/mailer')
  , database            = require('../../helpers/database')
  , datetime            = require('../../helpers/datetime')
  , distance            = require('../../helpers/distance')
  , googlemaps          = require('../../helpers/googlemaps')
  , errorMessage        = require('../../helpers/errormessage')
  , riderauth           = require('../../middlewares/riderauth')
  , router              = express.Router();

// Rider checks if trip is canceled
// 100% done - not tested - not documented
router.post('/', function(req, res){
  var rider_id = req.body.rider_id;

  database.query('SELECT * from trips WHERE rider_id = ? limit 1', rider_id, function(err, trips, fields) {
    if(err) {
      errorMessage.sendErrorMessage('server error', res);
    } else if(trips == '') {
      res.json({
        status: 'success',
        cancel_status: 'true'
      });
    } else {
      res.json({
        status: 'success',
        cancel_status: 'false'
      });
    }
  });
});

module.exports = router;
