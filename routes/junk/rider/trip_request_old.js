var express             = require('express')
  , push                = require('../../helpers/push')
  , mailer              = require('../../helpers/mailer')
  , database            = require('../../helpers/database')
  , datetime            = require('../../helpers/datetime')
  , distance            = require('../../helpers/distance')
  , googlemaps          = require('../../helpers/googlemaps')
  , errorMessage        = require('../../helpers/errormessage')
  , riderauth           = require('../../middlewares/riderauth')
  , router              = express.Router();

// Rider search for nearest available biker
// 100% done - tested - documented
// router.post('/tripRequest', riderauth.tokenauth, function(req, res)
router.post('/', riderauth.tokenauth, function(req, res){
  var rider_start_lat     = req.body.start_lat
    , rider_start_long    = req.body.start_long
    , rider_current_lat   = req.body.current_lat
    , rider_current_long  = req.body.current_long
    , rider_end_lat       = req.body.end_lat
    , rider_end_long      = req.body.end_long
    , from_area           = req.body.from_area
    , to_area             = req.body.to_area
    , rider_id            = req.body.rider_id
    , is_courier          = req.body.biker_type == 3 ? 1 : 0
    , biker_type          = req.body.biker_type == 3 ? 0 : req.body.biker_type
    , payment_type        = req.body.payment_type || 0
    , promocode           = req.body.promocode || 0
    , note_to_driver      = req.body.note_to_driver || ''
    , receiver_name       = req.body.receiver_name || ''
    , receiver_phone      = req.body.receiver_phone || '';

    console.log("rider_start_lat - " + rider_start_lat);
    console.log("rider_start_long - " + rider_start_long);
    console.log("rider_current_lat - " + rider_current_lat);
    console.log("rider_current_long - " + rider_current_long);
    console.log("rider_end_lat - " + req.body.end_lat);
    console.log("rider_end_long " + rider_end_long );
    console.log("from_area - " + from_area);
    console.log("to_area - " + to_area);
    console.log("rider_id - " + rider_id);
    console.log("is_courier - " + is_courier);
    console.log("biker_type - " + biker_type);
    console.log("payment_type - " + payment_type);
    console.log("promocode - " + promocode);
    console.log("note_to_driver - " + note_to_driver );
    console.log("receiver_name - " + receiver_name);
    console.log("receiver_phone - " + receiver_phone);

  if(rider_start_lat == null || rider_start_lat == undefined || rider_start_lat == ''|| rider_start_long == null || rider_start_long == undefined || rider_start_long == ''){
    rider_start_lat = rider_current_lat;
    rider_start_long = rider_current_long;
  }

  console.log('Request by : ' + rider_id );
  if(is_courier == 1)
    console.log('Request to courier');

  database.query('SELECT * from settings limit 1', function(err, settings, fields) {
    if(err) {
      errorMessage.sendErrorMessage('server error', res);
    } else if(settings == ''){
      errorMessage.sendErrorMessage('settings error', res);
    } else {
      var requestArea = settings[0].bikerRequestArea;

      database.query('SELECT * from bikers WHERE loggedIn = ? and current_status = ? and type = ?', [ 1, 0, biker_type], function(err, bikers, fields) {
        if(err) {
          errorMessage.sendErrorMessage('server error', res);
        } else if(bikers == ''){
          errorMessage.sendErrorMessage('biker not available 1', res);
        } else {
          var nearestBikers = [];

          for(var i = 0; i < bikers.length; i++) {
            var mDistance = distance.getDistanceFromLatLonInKm(rider_start_lat, rider_start_long, bikers[i].current_latitude, bikers[i].current_longitude);
            console.log("mDistance : " + mDistance);
            if(mDistance <= requestArea){
              nearestBikers.push({
                biker_id: bikers[i].id,
                distance: mDistance
              });
            }
          }
          nearestBikers.sort(function(a, b){return a.distance-b.distance});
          console.log(nearestBikers);

          var bikerCount = 0;
          var timeCount = 0;

          (function myQuery() {
            if(bikerCount >= nearestBikers.length || bikerCount > 2) {
              errorMessage.sendErrorMessage('biker not available', res);
            } else {
              database.query('UPDATE bikers SET current_status = ? WHERE id = ? and loggedIn = ? and current_status = ?', [1, nearestBikers[bikerCount].biker_id, 1, 0], function(err, result) {
                if(err || result.changedRows == 0){
                  bikerCount++;
                  myQuery();
                } else {
                  var currentDateTime = datetime.getDateTime();
                  console.log("requesting to : " + nearestBikers[bikerCount].biker_id + " at " + currentDateTime);

                  timeCount = 0;
                  var updateValue = {
                    trip_start: '2016-01-01',
                    trip_end: '2016-01-01',
                    rating_id: 0,
                    biker_id: nearestBikers[bikerCount].biker_id,
                    rider_id: rider_id,
                    total_bill: 0,
                    distance: 0,
                    total_paid: 0,
                    total_due: 0,
                    payment_method_id: 0,
                    cost_per_min: 0,
                    start_longitude: rider_start_long,
                    start_latitude: rider_start_lat,
                    from_area: from_area,
                    current_longitude: rider_current_long,
                    current_latitude: rider_current_lat,
                    end_longitude: rider_end_long,
                    end_latitude: rider_end_lat,
                    to_area: to_area,
                    heading: '',
                    status: 'a',
                    is_courier: is_courier,
                    biker_status: 'Active',
                    rider_status: '',
                    deleted: 0,
                    promocode: promocode,
                    note_to_driver: note_to_driver,
                    receiver_name: receiver_name,
                    receiver_phone: receiver_phone
                  }

                  database.query('INSERT INTO temptrips SET ?', updateValue, function(err, result) {
                    if(!err) {
                      var myInterval = setInterval(function() {
                        database.query('SELECT * from temptrips WHERE biker_id = ? limit 1', nearestBikers[bikerCount].biker_id, function(err, temptrip, fields) {
                          if(err) {
                            errorMessage.sendErrorMessage('server error', res);
                          } else if(temptrip == ''){
                            clearInterval(myInterval);
                            res.json({
                              status: 'success'
                            });
                          } else if(temptrip[0].rider_status == 'Decline') {
                            clearInterval(myInterval);
                            setTimeout(function(){
                              console.log("Rider Cancel Trip");
                            }, 10000);
                            database.query('DELETE FROM temptrips WHERE biker_id = ?', nearestBikers[bikerCount].biker_id, function (err, result) {
                              database.query('UPDATE bikers SET current_status = ? WHERE id = ?', [0, nearestBikers[bikerCount].biker_id], function(err, result) {
                                bikerCount = 10;
                                myQuery();
                              });
                            });
                          } else if(temptrip[0].biker_status == 'Decline') {
                            clearInterval(myInterval);
                            database.query('DELETE FROM temptrips WHERE biker_id = ?', nearestBikers[bikerCount].biker_id, function (err, result) {
                              // clearInterval(myInterval);
                              bikerCount++;
                              myQuery();
                            });
                          } else if(timeCount >= 10){
                            database.query('DELETE FROM temptrips WHERE biker_id = ?', nearestBikers[bikerCount].biker_id, function (err, result) {
                              var updateValue = {
                                type: 'Cancel Trip',
                                biker_id: nearestBikers[bikerCount].biker_id
                              }
                              database.query('INSERT INTO warnings SET ?', updateValue, function(err, result) {
                                database.query('UPDATE bikers SET current_status = ? WHERE id = ?', [0, nearestBikers[bikerCount].biker_id], function(err, result) {
                                  clearInterval(myInterval);
                                  bikerCount++;
                                  myQuery();
                                });
                              });
                            });
                          } else {
                            timeCount++;
                          }
                        });
                      }, 2000 );
                    }
                  });
                }
              });
            }
          })();
        }
      });
    }
  });
});

module.exports = router;
