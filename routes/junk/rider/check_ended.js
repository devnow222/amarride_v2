var express             = require('express')
  , push                = require('../../helpers/push')
  , mailer              = require('../../helpers/mailer')
  , database            = require('../../helpers/database')
  , datetime            = require('../../helpers/datetime')
  , distance            = require('../../helpers/distance')
  , googlemaps          = require('../../helpers/googlemaps')
  , errorMessage        = require('../../helpers/errormessage')
  , riderauth           = require('../../middlewares/riderauth')
  , router              = express.Router();

// Bill info of a trip
// 100% done - not tested - not documented
router.post('/', function(req, res){
  var trip_id = req.body.trip_id;

  database.query('SELECT * from success_trips WHERE trip_id = ? limit 1', trip_id, function(err, successTrip, fields) {
    if(err) {
      errorMessage.sendErrorMessage('server error', res);
    } else if(successTrip == '') {
      errorMessage.sendErrorMessage('trip not found', res);
    } else {
      var sendValue = {
        total_bill: successTrip[0].total_bill.toString(),
        distance: successTrip[0].distance.toString(),
        total_trip_time: (datetime.modifyDate(successTrip[0].total_trip_time)).toString(),
        cost_per_min: successTrip[0].cost_per_min.toString(),
        base_price : successTrip[0].base_price.toString(),
        screenshot_url: successTrip[0].trip_screenshot
      };

      res.json({
        status: 'success',
        trip_end: sendValue
      });
    }
  });
});

module.exports = router;
