var express             = require('express')
  , push                = require('../../helpers/push')
  , mailer              = require('../../helpers/mailer')
  , database            = require('../../helpers/database')
  , datetime            = require('../../helpers/datetime')
  , distance            = require('../../helpers/distance')
  , googlemaps          = require('../../helpers/googlemaps')
  , errorMessage        = require('../../helpers/errormessage')
  , riderauth           = require('../../middlewares/riderauth')
  , router              = express.Router();

// Rider checks if the biker is arriving - Mithun // push done
// 100% done - tested - documented
router.post('/', function(req, res) {
  var rider_id = req.body.rider_id;

  database.query('SELECT * FROM trips WHERE rider_id = ?', rider_id,function(err, trips, fields) {
    if(err){
      errorMessage.sendErrorMessage('server error', res);
    } else if(trips == ''){
      errorMessage.sendErrorMessage('trip not found', res);
    } else if(trips[0].trip_status == 'Arriving' || trips[0].trip_status == 'Arrived' || trips[0].trip_status == 'Started'){
      var trip_id = trips[0].id;
      var biker_id = trips[0].biker_id;

      database.query('SELECT * FROM bikers WHERE id = ?', biker_id,function(err, bikers, fields) {
        if(err){
          errorMessage.sendErrorMessage('server error', res);
        } else if(bikers == ''){
          errorMessage.sendErrorMessage('biker not found', res);
        } else {
          var biker = {
            biker_id    : biker_id.toString(),
            biker_lat   : bikers[0].current_latitude,
            biker_long  : bikers[0].current_longitude,
            full_name   : bikers[0].full_name.toString(),
            phone_num   : bikers[0].phone_num.toString(),
            biker_photo : bikers[0].user_photo,
            biker_email : bikers[0].email
          };
          database.query('SELECT * FROM ratings WHERE biker_id = ? and who_rated = ?', [biker_id, 'Rider'],function(err, ratings, fields) {
            if(err){
              errorMessage.sendErrorMessage('server error', res);
            } else {
              var ratingValue = 5;
              if(ratings.length > 0) {
                ratings.map(function(rating){
                  ratingValue += rating.rating_value
                });
                ratingValue = (ratingValue / ratings.length).toFixed(1);
              }
              database.query('SELECT * FROM rides WHERE biker_id = ?', biker_id,function(err, ride, fields) {
                if(err){
                  errorMessage.sendErrorMessage('server error', res);
                } else {
                  var ride = {
                    id          : ride[0].id.toString(),
                    ride_sl_num : ride[0].ride_sl_num,
                    ride_model  : ride[0].ride_model
                  }

                  var from_area = (bikers[0].current_latitude).toString() + ',' + (bikers[0].current_longitude).toString();
                  var to_area = (trips[0].start_latitude).toString() + ',' + (trips[0].start_longitude).toString();

                  console.log("from_area : " + from_area + "    to_area : " + to_area);

                  googlemaps.getDistanceAndTime(from_area, to_area, function(value) {
                    console.log(value);
                    if(value.destination_addresses == '' || value.origin_addresses == ''){
                      var sendValue = {
                        status       : "success",
                        biker        : biker,
                        ride         : ride,
                        biker_rating : ratingValue.toString(),
                        total_time   : '10',
                        distance     : '0.5',
                        trip_id      : trip_id.toString()
                      };
                    } else {
                      var distance = parseInt(value.rows[0].elements[0].distance.text);
                      var duration = Math.ceil(value.rows[0].elements[0].duration.value / 60);

                      var sendValue = {
                        status       : "success",
                        biker        : biker,
                        ride         : ride,
                        biker_rating : ratingValue.toString(),
                        total_time   : duration.toString(),
                        distance     : distance.toString(),
                        trip_id      : trip_id.toString()
                      };
                    }
                    console.log(sendValue);
                    res.json(sendValue);
                  });
                }
              });
            }
          });
        }
      });
    } else {
      res.json({
        success : 'not arriving'
      });
    }
  });
});

module.exports = router;
