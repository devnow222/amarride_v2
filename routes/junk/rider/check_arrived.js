var express             = require('express')
  , push                = require('../../helpers/push')
  , mailer              = require('../../helpers/mailer')
  , database            = require('../../helpers/database')
  , datetime            = require('../../helpers/datetime')
  , distance            = require('../../helpers/distance')
  , googlemaps          = require('../../helpers/googlemaps')
  , errorMessage        = require('../../helpers/errormessage')
  , riderauth           = require('../../middlewares/riderauth')
  , router              = express.Router();

// When a biker accepts a request rider checks if the biker has arrived - Mithun
// 100 % done - not tested - documented
router.post('/', function(req, res) {
  var rider_id = req.body.rider_id;

  database.query('SELECT * FROM trips WHERE rider_id = ?', rider_id, function(err, trips, fields) {
    if(err){
      errorMessage.sendErrorMessage('server error', res);
    } else if(trips == ''){
      errorMessage.sendErrorMessage('trip error', res);
    } else if(trips[0].trip_status == 'Arrived' || trips[0].trip_status == 'Started'){
      res.json({
        status: 'success'
      });
    } else {
      errorMessage.sendErrorMessage('biker not arrived', res);
    }
  });
});

module.exports = router;
