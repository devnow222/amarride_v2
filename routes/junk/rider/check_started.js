var express             = require('express')
  , push                = require('../../helpers/push')
  , mailer              = require('../../helpers/mailer')
  , database            = require('../../helpers/database')
  , datetime            = require('../../helpers/datetime')
  , distance            = require('../../helpers/distance')
  , googlemaps          = require('../../helpers/googlemaps')
  , errorMessage        = require('../../helpers/errormessage')
  , riderauth           = require('../../middlewares/riderauth')
  , router              = express.Router();

// Rider checks if the trip has started
// 100% done - not tested - documented
router.post('/', function(req, res){
  var rider_id = req.body.rider_id;

  database.query('SELECT * from trips WHERE rider_id = ? limit 1', rider_id, function(err, trips, fields) {
    if(err) {
      errorMessage.sendErrorMessage('server error', res);
    } else if(trips == '') {
      errorMessage.sendErrorMessage('trip error', res);
    } else if(trips[0].trip_status == 'Started') {
      res.json({
        status: 'success'
      });
    } else {
      errorMessage.sendErrorMessage('not started', res);
    }
  });
});

module.exports = router;
