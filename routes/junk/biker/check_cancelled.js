var express      = require('express')
  , dateFormat   = require('dateformat')
  , database     = require('../../helpers/database')
  , datetime     = require('../../helpers/datetime')
  , distance     = require('../../helpers/distance')
  , googlemaps   = require('../../helpers/googlemaps')
  , errorMessage = require('../../helpers/errormessage')
  , bikerauth    = require('../../middlewares/bikerauth')
  , router       = express.Router();

// Biker checks once in every 2 second if rider canceled the trip
// 100% done - not tested - documented
router.post('/', function(req, res){
  var biker_id = req.body.biker_id;

  database.query('SELECT * from trips WHERE biker_id = ? limit 1', biker_id, function(err, trips, fields) {
    if(err) {
      errorMessage.sendErrorMessage('server error', res);
    } else if(trips == ''){
      database.query('UPDATE bikers SET current_status = ? WHERE id = ?', [0, biker_id], function(err, result) {
        if(err) {
          errorMessage.sendErrorMessage('server error', res);
        } else {
          console.log("Cancel is true for biker");
          res.json({
            status: 'success',
            cancel_status: 'true'
          });
        }
      });
    } else {
      res.json({
        status: 'success',
        cancel_status: 'false'
      });
    }
  });
});

module.exports = router;
