var express        = require('express')
  , ride_model     = require('../../models/rides')
  , trip_model     = require('../../models/trips')
  , biker_model    = require('../../models/bikers')
  , rider_model    = require('../../models/riders')
  , temptrip_model = require('../../models/temptrips')
  , push           = require('../../helpers/push')
  , googlemaps     = require('../../helpers/googlemaps')
  , errorMessage   = require('../../helpers/errormessage')
  , router       = express.Router();

// Biker accept the trip
// 90% done - not tested - documented
router.post('/', function(req, res){
  var biker_id    = req.body.biker_id
    , rider_id    = null
    , reg_code    = null
    , from_area   = null
    , to_area     = null
    , sendToBiker = {}
    , sendToRider = {};

  temptrip_model.findWithBikerId(biker_id)
  .then(function(temptrips) {
    var currentDateTime = new Date();
    console.log("Request accepted by : " + biker_id + " at - " + currentDateTime);

    var value = {
      trip_start : '2016-01-01 12:00:00',
      trip_end : '2016-01-01 12:00:00',
      rating_id : temptrips[0].rating_id,
      biker_id : temptrips[0].biker_id,
      rider_id : temptrips[0].rider_id,
      wallet_id : 0,
      total_bill : temptrips[0].total_bill,
      distance : temptrips[0].distance,
      total_paid : temptrips[0].total_paid,
      total_due : temptrips[0].total_due,
      payment_method_id : temptrips[0].payment_method_id,
      cost_per_min : temptrips[0].cost_per_min,
      start_longitude : temptrips[0].start_longitude,
      start_latitude : temptrips[0].start_latitude,
      from_area : temptrips[0].from_area,
      current_longitude : temptrips[0].current_longitude,
      current_latitude : temptrips[0].current_latitude,
      end_longitude : temptrips[0].end_longitude,
      end_latitude : temptrips[0].end_latitude,
      to_area : temptrips[0].to_area,
      heading : temptrips[0].heading,
      status : temptrips[0].status,
      is_courier : temptrips[0].is_courier,
      favid : 0,
      trip_status : 'Arriving',
      cancel_status : 0,
      trip_screenshot : '',
      callcenterConfirmation : '',
      callcenterDestinationChange : '',
      reviewStatus : '',
      promocode: temptrips[0].promocode,
      note_to_driver: temptrips[0].note_to_driver,
      receiver_name : temptrips[0].receiver_name,
      receiver_phone : temptrips[0].receiver_phone
    };
    rider_id =  temptrips[0].rider_id;
    sendToBiker.temptrip = {
      id: temptrips[0].id,
      start_long: temptrips[0].start_longitude,
      start_lat: temptrips[0].start_latitude,
      from_area: temptrips[0].from_area,
      current_long: temptrips[0].current_longitude,
      current_lat: temptrips[0].current_latitude,
      end_long: temptrips[0].end_longitude,
      end_lat: temptrips[0].end_latitude,
      to_area: temptrips[0].to_area,
      is_courier: temptrips[0].is_courier,
      promocode: temptrips[0].promocode,
      note_to_driver: temptrips[0].note_to_driver,
      receiver_name: temptrips[0].receiver_name,
      receiver_phone: temptrips[0].receiver_phone
    };
    to_area = (temptrips[0].start_latitude).toString() + ',' + (temptrips[0].start_longitude).toString();
    return trip_model.add(value);
  })
  .then(function(result) {
    sendToRider.trip_id = result.insertId;
    var update = { current_status: 1 };
    temptrip_model.deleteWithRiderId(rider_id);
    return biker_model.updateWithBikerId(update, biker_id);
  })
  .then(function(result){
    if(result) return rider_model.findWithId(rider_id);
  })
  .then(function(riders) {
    reg_code = [riders[0].reg_code];
    sendToBiker.rider = {
      id: riders[0].id,
      full_name: riders[0].full_name,
      age: riders[0].age,
      sex: riders[0].sex,
      user_photo:riders[0].user_photo,
      current_long: riders[0].current_longitude,
      current_lat: riders[0].current_latitude,
      phone_num: riders[0].phone_num,
      userRating: (riders[0].rating / 10).toString()
    };
    return biker_model.findWithBikerId(biker_id);
  })
  .then(function(bikers) {
    sendToRider.biker = {
      biker_id    : biker_id.toString(),
      biker_lat   : bikers[0].current_latitude,
      biker_long  : bikers[0].current_longitude,
      full_name   : bikers[0].full_name.toString(),
      phone_num   : bikers[0].phone_num.toString(),
      biker_photo : bikers[0].user_photo,
      biker_email : bikers[0].email
    };
    sendToRider.biker_rating = (bikers[0].rating / 10).toString();

    from_area = (bikers[0].current_latitude).toString() + ',' + (bikers[0].current_longitude).toString();
    return ride_model.findWithBikerId(biker_id);
  })
  .then(function(ride) {
    sendToRider.ride = {
      id          : ride[0].id.toString(),
      ride_sl_num : ride[0].ride_sl_num,
      ride_model  : ride[0].ride_model
    };
    return googlemaps.getDistanceAndTime(from_area, to_area);
  })
  .then(function(value) {

    if(value.destination_addresses == '' || value.origin_addresses == ''){
      sendToRider.total_time = '10';
      sendToRider.distance   = '0.5';
    } else {
      sendToRider.total_time = (value.rows[0].elements[0].duration.text).toString();
      sendToRider.distance   = (value.rows[0].elements[0].distance.text).toString();
    }
    push.sendNotification(reg_code, 'Arriving', sendToRider, function(err, response) {
      if(err) console.log("error" + err);
      else console.log("arriving notification sent");
    });
    sendToBiker.status = 'success';
    res.json(sendToBiker);
  })
  .catch(function(err){
    errorMessage.sendErrorMessage(err, res);
  });
});

module.exports = router;
