var express      = require('express')
  , dateFormat   = require('dateformat')
  , database     = require('../../helpers/database')
  , datetime     = require('../../helpers/datetime')
  , distance     = require('../../helpers/distance')
  , googlemaps   = require('../../helpers/googlemaps')
  , errorMessage = require('../../helpers/errormessage')
  , bikerauth    = require('../../middlewares/bikerauth')
  , router       = express.Router();

// Biker checks for warning
// 100% done - tested - documented
router.post('/', function(req, res) {
  var biker_id = req.body.biker_id;

  database.query('SELECT * from bikers WHERE id = ? limit 1', biker_id, function(err, rows, fields) {
    if(err) {
      errorMessage.sendErrorMessage('server error', res);
    } else if(rows == ''){
      errorMessage.sendErrorMessage('biker not found', res);
    } else {
      if(rows[0].warning == 0){
        res.json({
          status: 'success',
          warning: 'false'
        })
      } else {
        var updateVlaue = {
          warning: 0,
          end_lat: rows[0].current_latitude,
          end_long: rows[0].current_longitude
        }
        database.query('UPDATE bikers SET ? WHERE id = ?', [updateVlaue, biker_id], function(err, result) {
          if(err) {
            errorMessage.sendErrorMessage('server error', res);
          } else {
            res.json({
              status: 'success',
              warning: 'true'
            });
          }
        });
      }
    }
  });
});

module.exports = router;
