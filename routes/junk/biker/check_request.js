var express      = require('express')
  , dateFormat   = require('dateformat')
  , database     = require('../../helpers/database')
  , datetime     = require('../../helpers/datetime')
  , distance     = require('../../helpers/distance')
  , googlemaps   = require('../../helpers/googlemaps')
  , errorMessage = require('../../helpers/errormessage')
  , bikerauth    = require('../../middlewares/bikerauth')
  , router       = express.Router();

// Biker checks for request once in every 5 second
// 100% done - tested - documented
router.post('/', function(req, res) {
  var biker_id = req.body.biker_id;

  database.query('SELECT * from temptrips WHERE biker_id = ? limit 1', biker_id, function(err, temptrip, fields) {
    if(err) {
      errorMessage.sendErrorMessage('server error', res);
    } else if(temptrip == ''){
      errorMessage.sendErrorMessage('no request 1', res);
    } else if(temptrip[0].biker_status == 'Active'){
      var rider_id = temptrip[0].rider_id;

      database.query('SELECT * from riders WHERE id = ? limit 1', rider_id, function(err, rider, fields) {
        if(err) {
          errorMessage.sendErrorMessage('server error', res);
        } else if(rider == ''){
          errorMessage.sendErrorMessage('no request 2', res);
        } else {
          database.query('SELECT * FROM ratings WHERE rider_id = ? and who_rated = ?', [rider_id, 'Biker'], function(err, ratings, fields) {
            if(err){
              errorMessage.sendErrorMessage('server error', res);
            } else {
              var ratingValue = 5;
              if(ratings.length > 0) {
                ratings.map(function(rating){
                  ratingValue += rating.rating_value
                });
                ratingValue = (ratingValue / ratings.length).toFixed(1);
              }
              var sendValue = {
                status: 'success',
                rider: {
                  id: rider[0].id,
                  full_name: rider[0].full_name,
                  to_area: temptrip[0].to_area,
                  from_area: temptrip[0].from_area,
                  is_courier: temptrip[0].is_courier,
                  userRating: ratingValue
                }
              }
              database.query('UPDATE temptrips SET biker_status = ? WHERE biker_id = ?', ['Request', biker_id], function(err, result) {
                if(err) {
                  errorMessage.sendErrorMessage('server error', res);
                } else {
                  res.json(sendValue);
                }
              });
            }
          });
        }
      });
    } else {
      errorMessage.sendErrorMessage('no request 3', res);
    }
  });
});

module.exports = router;
