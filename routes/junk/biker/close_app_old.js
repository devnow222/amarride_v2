var express       = require('express')
  , trip_model    = require('../../models/trips')
  , biker_model   = require('../../models/bikers')
  , rider_model   = require('../../models/riders')
  , warning_model = require('../../models/warnings')
  , push          = require('../../helpers/push')
  , errorMessage  = require('../../helpers/errormessage')
  , router        = express.Router();

/**
 * @api {post} /api/biker/closeApp - Confirmation when app is closed.
 * @apiName Close App
 * @apiGroup Biker
 *
 * @apiBodyParam {Number} biker_id Biker unique ID.
 * @apiBodyParam {Number} token Unique token for biker.
 *
 * @apiSuccess {JSON Object}
 *   @jsonBody {String} status Status of request.
 *
 * @apiError {JSON Object} lastname  Lastname of the User.
 *   @jsonBody {String} status Status of request.
 *   @jsonBody {String} message Error information.
 */
router.post('/', function(req, res) {
  var biker_id = req.body.biker_id
    , rider_id = null;

  var value = {
    type: 'Close App',
    biker_id: biker_id
  };

  warning_model.add(value)
  .then(function (result) {
    trip_model.findWithBikerId(biker_id)
    .then(function (trip) {
      if(trip) {
        rider_id = trip[0].rider_id;
        trip_model.deleteWithBikerId(biker_id);
        return rider_model.findWithId(rider_id);
      }
    })
    .then(function (rider) {
      var reg_code = [rider[0].reg_code];
      if(rider[0].os_type == 'Android') {
        push.forceNotification(reg_code, 'Cancelled', '', function (err) {
          if (err) console.log("error" + err);
          else console.log("notification sent");
        });
      } else {
        push.sendNotification(reg_code, 'Cancelled', '', function (err) {
          if (err) console.log("error" + err);
          else console.log("notification sent");
        });
      }
    })

    var update = { current_status: 2 };
    return biker_model.updateWithBikerId(update, biker_id);
  })
  .then(function (result) {
    if(result) {
      res.json({ status: 'success' });
    } else {
      errorMessage.sendErrorMessage('Something went wrong. Please try again.', res);
    }
  })
  .catch(function (err) {
    errorMessage.sendErrorMessage(err, res);
  });
});

module.exports = router;
