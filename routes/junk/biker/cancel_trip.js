var express        = require('express')
  , trip_model     = require('../../models/trips')
  , biker_model    = require('../../models/bikers')
  , warning_model  = require('../../models/warnings')
  , errorMessage   = require('../../helpers/errormessage')
  , router         = express.Router();

// This api gets hit if the biker doesn't accept the request in less then 15 second
// 90% done - not tested - documented
router.post('/', function(req, res){
  var biker_id = req.body.biker_id;
  // 
  // trip_model.findWithBikerId(biker_id)
  // .then(function(temptrips) {
  //
  // })

  database.query('SELECT * from temptrips WHERE biker_id = ? limit 1', biker_id, function(err, rows, fields) {
    if(err) {
      errorMessage.sendErrorMessage('server error', res);
    } else if(rows == ''){
      errorMessage.sendErrorMessage('trip error', res);
    } else if(rows[0].biker_status == 'Request'){
      database.query('UPDATE temptrips SET biker_status = ? WHERE id = ?', ['Decline', biker_id], function(err, result) {
        if(err) {
          errorMessage.sendErrorMessage('server error', res);
        } else {
          var updateVaue = {
            type: 'Cancel Trip',
            biker_id: biker_id
          }

          database.query('INSERT INTO warnings SET ?', updateVaue, function(err, result) {
            if(err) {
              errorMessage.sendErrorMessage('server error', res);
            } else {
              database.query('UPDATE bikers SET current_status = ? WHERE id = ?', [0, biker_id], function(err, result) {
                if(err) {
                  errorMessage.sendErrorMessage('server error', res);
                } else {
                  res.json({
                    status: 'success'
                  });
                }
              });
            }
          });
        }
      });
    } else {
      errorMessage.sendErrorMessage('not authorized', res);
    }
  });
});

module.exports = router;
