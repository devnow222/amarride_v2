var express      = require('express')
  , biker_model  = require('../../models/bikers')
  , errorMessage = require('../../helpers/errormessage')
  , bikerauth    = require('../../middlewares/bikerauth')
  , router       = express.Router();

// This api gets hit once the biker closes his app
// 100% done - tested - documented
router.post('/', function(req, res) {
  if(!req.body.biker_id) {
    errorMessage.sendErrorMessage('Please provide all info.', res);
  } else {
    var biker_id = req.body.biker_id;

    update = { current_status: 2, loggedIn: 0 };

    biker_model.updateWithBikerId(update, biker_id)
    .then(function(result) {
      res.json({ status: 'success' });
    })
    .catch(function(err) {
      errorMessage.sendErrorMessage('server error', res);
    });
  }
});

module.exports = router;
