var express            = require('express')
  , user_profile       = require('../../config/config').env.user_profile
  , trip_model         = require('../../models/trips')
  , rider_model        = require('../../models/riders')
  , biker_model        = require('../../models/bikers')
  , success_trip_model = require('../../models/success_trips')
  , errorMessage       = require('../../helpers/errormessage')
  , router             = express.Router();

router.post('/', function(req, res){
  if(!req.body.biker_id) {
    errorMessage.sendErrorMessage('Please provide all info.', res);
  } else {
    var biker_id    = req.body.biker_id
    var sendToBiker = {};

    trip_model.findPreviousWithBikerId(biker_id)
    .then(function(trips) {
      sendToBiker.status = 'success';
      sendToBiker.trip_status = trips[0].trip_status
      sendToBiker.temptrip = {
        id: trips[0].id,
        start_lat: trips[0].start_latitude,
        start_long: trips[0].start_longitude,
        end_lat: trips[0].end_latitude,
        end_long: trips[0].end_longitude,
        from_area: trips[0].from_area,
        to_area: trips[0].to_area,
        is_courier: trips[0].is_courier,
        courier_type: trips[0].courier_type,
        note_to_driver: trips[0].note_to_driver,
        receiver_name: trips[0].receiver_name,
        receiver_phone: trips[0].receiver_phone
      };
      return rider_model.findWithId(trips[0].rider_id);
    })
    .then(function(riders) {
      sendToBiker.rider = {
        id: riders[0].id,
        full_name: riders[0].full_name,
        age: riders[0].age,
        sex: riders[0].gender,
        current_lat: riders[0].current_latitude,
        current_long: riders[0].current_longitude,
        phone_num: riders[0].phone_num,
        user_photo: user_profile + riders[0].user_photo,
        userRating: (riders[0].rating / 10)
      };
      res.send(sendToBiker);
    })
    .catch(function(err) {
      success_trip_model.findPendingWithBikerId(biker_id)
      .then(function(success_trip) {
        sendToBiker = {
          status: 'success',
          trip_status: 'Ended',
          trip_id: success_trip[0].trip_id,
          rider_id: success_trip[0].rider_id,
          total_bill: success_trip[0].total_bill,
          total_trip_time: success_trip[0].total_trip_time,
          cost_per_min: success_trip[0].cost_per_min,
          waiting_charge_per_min: success_trip[0].waiting_charge_per_min,
          total_wating_time: success_trip[0].total_wating_time,
          total_waiting_bill: success_trip[0].total_waiting_bill,
          base_price : success_trip[0].base_price,
          distance: 0,
          cost_per_km: 0
        };
        res.send(sendToBiker);
      })
      .catch(function(err) {
        biker_model.findWithBikerId(biker_id)
        .then(function(bikers) {
          var currentTimeInMillisecond = new Date().getTime();
          if(currentTimeInMillisecond > bikers[0].expirydate) {
            var message = 'Your subscription has expired! Please call 01955445555 for more info.';
            res.send({
              status: 'success',
              trip_status: 'Expired',
              message: message
            });
          } else {
            errorMessage.sendErrorMessage(err, res);
          }
        })
        .catch(function(err) {
          errorMessage.sendErrorMessage(err, res);
        });
      });
    });
  }
});

module.exports = router;
