var express      = require('express')
  , biker_model  = require('../../models/bikers')
  , errorMessage = require('../../helpers/errormessage')
  , router       = express.Router()
  , biker_session_model  = require('../../models/biker_sessions');

// This api gets hit once the biker opens his app
// 100% done - tested - documented
router.post('/', function(req, res) {
  if(!req.body.biker_id) {
    errorMessage.sendErrorMessage('Please provide all info.', res);
  } else {
    var biker_id = req.body.biker_id
      , update   = { current_status: 0 };

    biker_model.findWithBikerId(biker_id)
    .then(function(biker) {
      if(biker[0].loggedIn == 0){
        errorMessage.sendErrorMessage('not loggedin', res);
      } else {
        return biker_model.updateWithBikerId(update, biker_id);
      }
    }).then(function (result) {
    	
    	var value = {
    			biker_id : req.body.biker_id
    		, 	proceed_timestamp: new Date().getTime()
    	};
    	return biker_session_model.add(value);
    }).then(function(result) {
      if(result) {
        res.json({
          status: 'success'
        });
      }
    })
    .catch(function(err) {
      errorMessage.sendErrorMessage('server error', res);
    });
  }
});

module.exports = router;
