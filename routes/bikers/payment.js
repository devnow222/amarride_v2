var express            = require('express')
  , biker_model        = require('../../models/bikers')
  , rider_model        = require('../../models/riders')
  , wallet_model       = require('../../models/wallets')
  , payment_model      = require('../../models/payments')
  , success_trip_model = require('../../models/success_trips')
  , push               = require('../../helpers/push')
  , mailer             = require('../../helpers/mailer')
  , datetime           = require('../../helpers/datetime')
  , bikerauth          = require('../../middlewares/bikerauth')
  , errorMessage       = require('../../helpers/errormessage')
  , router             = express.Router();

// Biker payment api - Riajul
// 90% done - not tested - documented
// bikerauth.tokenauth,
router.post('/', function(req, res) {
  if(!req.body.biker_id || !req.body.trip_id) {
    errorMessage.sendErrorMessage('Please provide all info.', res);
  } else {
    var biker_id      = req.body.biker_id
      , cash_amount   = parseInt(req.body.cash_amount) || 0
      , wallet_amount = parseInt(req.body.wallet_amount) || 0
      , trip_id       = req.body.trip_id
      , callcenter    = req.body.callcenter ? 1 : 0
      , rider_id      = null
      , wallet_id     = null
      , total_bill    = null
      , ccConf        = []
      , insertValue   = {}
      , tripInfo      = {};

    success_trip_model.findWithTripId(trip_id)
    .then(function(successTrip) {
      if(successTrip[0].total_paid > 0) {
        res.json({
          status: 'already_paid',
          message: 'Payment already received successfully.'
        });
      } else {
        rider_id = successTrip[0].rider_id;
        total_bill = successTrip[0].total_bill;
        ccConf = successTrip[0].callcenterConfirmation == '' ? [] : JSON.parse(successTrip[0].callcenterConfirmation);
        insertValue = {
          trip_id: successTrip[0].trip_id,
          trip_type: successTrip[0].trip_type,
          amount: cash_amount + wallet_amount,
          cash_amount: cash_amount,
          wallet_amount: wallet_amount
        };
        tripInfo.trip = {
          trip_start: successTrip[0].trip_start,
          from_area: successTrip[0].from_area,
          trip_end: successTrip[0].trip_end,
          to_area: successTrip[0].to_area,
          cost_per_min: successTrip[0].cost_per_min,
          total_trip_time: datetime.modifyDate(successTrip[0].total_trip_time),
          total_wating_time: datetime.modifyDate(successTrip[0].total_wating_time),
          surge_factor: 1,
          total_bill: successTrip[0].total_bill,
          total_waiting_bill: successTrip[0].total_waiting_bill,
          base_price: successTrip[0].base_price,
          screenshot_url: successTrip[0].trip_screenshot
        };
        wallet_model.findWithRiderId(rider_id)
        .then(function(wallet) {
          wallet_id = wallet[0].id;
          var walletBalance = parseInt(wallet[0].balance);
          if(wallet_amount > 0) {
            if(walletBalance >= wallet_amount) {
              var update = {
                balance: walletBalance - wallet_amount,
                spent: parseInt(wallet[0].spent) + wallet_amount
              };
              return wallet_model.updateWithRiderId(update, rider_id);
            } else {
              throw 'Not enough balance in wallet. Ask user to recharge his/her wallet.';
            }
          }
        })
        .then(function() {
          if(total_bill != (cash_amount + wallet_amount)) {
            throw 'Bill amount is not correct. Make sure you are taking correct amount from user.';
          } else {
            var update = {};
            if(cash_amount > 0 && wallet_amount > 0) {
              update = {
                total_paid : cash_amount + wallet_amount,
                payment_method_id : 2,
                wallet_id: wallet_id
              }
            } else if(wallet_amount > 0) {
              update = {
                total_paid : wallet_amount,
                payment_method_id : 1,
                wallet_id: wallet_id
              }
            } else {
              update = {
                total_paid : cash_amount,
                payment_method_id : 0
              }
            }
            if(callcenter == 1) {
              ccConf.push('Payment');
              update.callcenterConfirmation = JSON.stringify(ccConf);
            }
            insertValue.payment_method_id = update.payment_method_id;
            return success_trip_model.updateWithTripId(update, trip_id)
          }
        })
        .then(function(result) {
          return biker_model.findWithBikerId(biker_id);
        })
        .then(function(biker) {
          var update = { total_cash: biker[0].total_cash + cash_amount, total_wallet: biker[0].total_wallet + wallet_amount };
          biker_model.updateWithBikerId(update, biker_id);
        })
        .then(function(result) {
          return payment_model.add(insertValue);
        })
        .then(function(result) {
          if(result) {
            res.json({ status: 'success' });
            return rider_model.findWithId(rider_id)
          }
        })
        .then(function(rider) {
          if(rider) {
            var reg_code = [rider[0].reg_code]
              , recipients  = rider[0].email;
            tripInfo.rider = {
              name: rider[0].full_name
            };
            mailer.sendMail(recipients, tripInfo, function(err){
              if(err) console.log(err);
              else console.log('email success');
            });
            var message = 'Payment received. Thanks for riding with us.';
            if(rider[0].os_type == 'Android') {
              push.forceNotification(reg_code, 'Success', message, function(err) {
                if(err) console.log("error : " + err);
                else console.log("success notification sent");
              });
            } else {
              push.sendNotification(reg_code, 'Success', message, function(err) {
                if(err) console.log("error : " + err);
                else console.log("success notification sent");
              });
            }
          }
        })
        .catch(function(err) {
          errorMessage.sendErrorMessage(err, res);
        });
      }
    })
    .catch(function(err) {
      errorMessage.sendErrorMessage(err, res);
    });
  }
});

module.exports = router;
