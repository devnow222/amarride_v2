var express      = require('express')
  , wallet_model = require('../../models/wallets')
  , errorMessage = require('../../helpers/errormessage')
  , router       = express.Router();

/**
 * @api {post} /api/biker/checkWallet - Check balance on user wallet.
 * @apiName Check Wallet
 * @apiGroup Biker
 *
 * @apiBodyParam {Number} token Unique token for biker.
 * @apiBodyParam {Number} rider_id Rider unique ID.
 *
 * @apiSuccess {JSON Object}
 *   @jsonBody {String} status Status of request.
 *   @jsonBody {Number} balance Wallet balance of user.
 *
 * @apiError {JSON Object} lastname  Lastname of the User.
 *   @jsonBody {String} status Status of request.
 *   @jsonBody {String} message Error information.
 */
router.post('/', function(req, res) {
  if(!req.body.rider_id) {
    errorMessage.sendErrorMessage('Please provide all info.', res);
  } else {
    var rider_id = req.body.rider_id;

    wallet_model.findWithRiderId(rider_id)
    .then(function(wallet) {
      var wallet_balance = wallet[0].balance.toString();
      res.json({
        status: 'success',
        balance: wallet_balance
      });
    })
    .catch(function(err) {
      errorMessage.sendErrorMessage(err, res);
    });
  }
});

module.exports = router;
