var express       = require('express')
  , biker_profile = require('../../config/config').env.biker_profile
  , trip_model    = require('../../models/trips')
  , rider_model   = require('../../models/riders')
  , biker_model   = require('../../models/bikers')
  , push          = require('../../helpers/push')
  , datetime      = require('../../helpers/datetime')
  , errorMessage  = require('../../helpers/errormessage')
  , router        = express.Router();

// Biker set trip started
// 100% done - not tested - documented
router.post('/', function(req, res){
  if(!req.body.biker_id) {
    errorMessage.sendErrorMessage('Please provide all info.', res);
  } else {
    var biker_id      = req.body.biker_id
      , callcenter    = req.body.callcenter ? 1 : 0
      , reg_code      = null
      , os_type       = null
      , trip_lat_long = null
      , ccConf        = []
      , sendToRider   = {};

    trip_model.findWithBikerId(biker_id)
    .then(function(trip) {
      sendToRider.trip_id = (trip[0].id).toString();
      sendToRider.from_area = (trip[0].from_area).toString();
      sendToRider.to_area = (trip[0].to_area).toString();
      trip_lat_long = trip[0].trip_lat_long;
      ccConf = trip[0].callcenterConfirmation == '' ? [] : JSON.parse(trip[0].callcenterConfirmation);
      switch (trip[0].trip_status) {
        case 'Arriving':
          throw 'Make sure you arrived at correct location and then press Arrived. Call 16393 for more information.';
        case 'Started':
          throw 'Your trip has already Started. Call 16393 for more information.';
        default:
          return rider_model.findWithId(trip[0].rider_id);
      }
    })
    .then(function(rider) {
      reg_code = [rider[0].reg_code];
      os_type = rider[0].os_type;
      return biker_model.findWithBikerId(biker_id);
    })
    .then(function (biker) {
      sendToRider.biker = {
        biker_id: biker[0].id.toString(),
        biker_lat: biker[0].current_latitude,
        biker_long: biker[0].current_longitude,
        full_name: biker[0].full_name,
        phone_num: biker[0].phone_num,
        biker_photo: biker_profile + biker[0].user_photo,
        biker_email: biker[0].email
      }
      sendToRider.ride = {
        ride_model: biker[0].ride_model,
        ride_sl_num: biker[0].ride_serial
      };
      sendToRider.biker_rating = (biker[0].rating / 10).toString();

      var currentDateTime = datetime.getDateTime();

      var trip_lat_long_array = JSON.parse(trip_lat_long);
      var current_lat_long = biker[0].current_latitude + ', ' + biker[0].current_longitude;
      trip_lat_long_array.push(current_lat_long);

      var update = {
        trip_start : currentDateTime,
        ///changes here
        start_latitude : biker[0].current_latitude,
        start_longitude : biker[0].current_longitude,
        trip_lat_long: JSON.stringify(trip_lat_long_array),
        trip_status : 'Started'
      };
      if(callcenter == 1) {
        ccConf.push('Started');
        update.callcenterConfirmation = JSON.stringify(ccConf);
      }
      return trip_model.updateWithBikerId(update, biker_id);
    })
    .then(function(result) {
      if(result.changedRows > 0){
        sendToRider.total_time = '0';
        sendToRider.distance   = '0';

        if(os_type == 'Android') {
          push.forceNotification(reg_code, 'Started', sendToRider, function(err) {
            if(err) console.log("error : " + err);
            else console.log("started notification sent");
          });
        } else {
          push.sendNotification(reg_code, 'Started', sendToRider, function(err) {
            if(err) console.log("error : " + err);
            else console.log("started notification sent");
          });
        }
        res.json({ status : 'success' });
      } else {
        errorMessage.sendErrorMessage('status not updated', res);
      }
    })
    .catch(function(err) {
      errorMessage.sendErrorMessage(err, res);
    });
  }
});

module.exports = router;
