var express      = require('express')
  , biker_model  = require('../../models/bikers')
  , errorMessage = require('../../helpers/errormessage')
  , router       = express.Router();

// Biker cancel trip with reason
// 100% done - not tested - documented
router.post('/', function(req, res){
  if(!req.body.biker_id || !req.body.reg_code) {
    errorMessage.sendErrorMessage('Please provide all info.', res);
  } else {
    var biker_id = req.body.biker_id
      , reg_code = req.body.reg_code
      , update   = { reg_code: reg_code };

    biker_model.updateWithBikerId(update, biker_id)
    .then(function(result) {
      res.json({ status: 'success' });
    })
    .catch(function(err) {
      errorMessage.sendErrorMessage(err, res);
    });
  }
});

module.exports = router;
