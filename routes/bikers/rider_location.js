var express      = require('express')
  , trip_model   = require('../../models/trips')
  , rider_model  = require('../../models/riders')
  , errorMessage = require('../../helpers/errormessage')
  , router       = express.Router();

// Biker checks for rider's location
// 90% done - not tested - documented
router.post('/', function(req, res){
  if(!req.body.biker_id) {
    errorMessage.sendErrorMessage('Please provide all info.', res);
  } else {
    var biker_id = req.body.biker_id;

    trip_model.findWithBikerId(biker_id)
    .then(function(trip) {
      return rider_model.findWithId(trip[0].rider_id);
    })
    .then(function(rider) {
      res.json({
        status: 'success',
        rider_lat: rider[0].current_latitude,
        rider_long: rider[0].current_longitude
      });
    })
    .catch(function(err) {
      errorMessage.sendErrorMessage(err, res);
    });
  }
});

module.exports = router;
