var express       = require('express')
  , biker_model   = require('../../models/bikers')
  , errorMessage  = require('../../helpers/errormessage')
  , bikerauth     = require('../../middlewares/bikerauth')
  , router        = express.Router();

// 100% done - tested - documented
router.post('/', bikerauth.tokenauth, function(req, res){
  if(!req.body.biker_id || !req.body.ride) {
    errorMessage.sendErrorMessage('Please provide all info.', res);
  } else {
    var biker_id = req.body.biker_id
      , ride     = req.body.ride.split(',');

    biker_model.findWithBikerId(biker_id)
    .then(function(bikers) {
      if(ride.length == 2) {
        ride[0] = ride[0].trim();
        ride[1] = ride[1].trim();
        ride[2] = ride[2].trim();
      }

      if(bikers[0].type != ride[0]) {
        throw 'You are not authorized to ride this vehicle';
      }

      if(bikers[0].biker_model != ride[1] || bikers[0].biker_serial != ride[2]) {
        var update = { ride_model: ride[1], ride_serial: ride[2] };
        return biker_model.updateWithBikerId(update, biker_id);
      }
    })
    .then(function(result) {
      res.json({
        status: 'success',
        ride_model: ride[1],
        ride_sl_num: ride[2]
      });
    })
    .catch(function(err) {
      errorMessage.sendErrorMessage(err, res);
    });
  }
});

module.exports = router;
