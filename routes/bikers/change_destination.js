var express      = require('express')
  , trip_model   = require('../../models/trips')
  , rider_model  = require('../../models/riders')
  , push         = require('../../helpers/push')
  , errorMessage = require('../../helpers/errormessage')
  , router       = express.Router();

/**
 * @api {post} /api/biker/changeDestination - Destiantion change on user demand.
 * @apiName Change Destiantion
 * @apiGroup Biker
 *
 * @apiBodyParam {Number} biker_id Biker unique ID.
 * @apiBodyParam {Number} destination New destination of current trip.
 *
 * @apiSuccess {JSON Object}
 *   @jsonBody {String} status Status of request.
 *
 * @apiError {JSON Object} lastname  Lastname of the User.
 *   @jsonBody {String} status Status of request.
 *   @jsonBody {String} message Error information.
 */
router.post('/', function(req, res) {
  if(!req.body.biker_id || !req.body.destination) {
    errorMessage.sendErrorMessage('Please provide all info.', res);
  } else {
    var biker_id    = req.body.biker_id
      , destination = req.body.destination
      , callcenter    = req.body.callcenter ? 1 : 0
      , trip_status = null
      , from_area   = null
      , rider_id    = null;

    trip_model.findWithBikerId(biker_id)
    .then(function(trip) {
      rider_id = trip[0].rider_id;
      trip_status = trip[0].trip_status;
      from_area = trip[0].from_area;

      var update = { to_area: destination };
      if(callcenter == 1) {
        var ccConf = trip[0].callcenterConfirmation == '' ? [] : JSON.parse(trip[0].callcenterConfirmation);
        if(ccConf.indexOf("DestiantionChange") < 0)
          ccConf.push('DestiantionChange');
        update.callcenterConfirmation = JSON.stringify(ccConf);
      }
      return trip_model.updateWithBikerId(update, biker_id);
    })
    .then(function(result) {
      return rider_model.findWithId(rider_id);
    })
    .then(function(rider) {
      var sendToRider = {
        trip_status: trip_status.toString(),
        from_area: from_area.toString(),
        to_area: destination.toString()
      }
      if(rider[0].os_type == 'Android') {
        push.forceNotification([rider[0].reg_code], 'Update', sendToRider, function(err, response) {
          if(err) console.log("error : " + err);
          else console.log("update notification sent");
        });
      } else {
        push.sendNotification([rider[0].reg_code], 'Update', sendToRider, function(err, response) {
          if(err) console.log("error : " + err);
          else console.log("update notification sent");
        });
      }
      res.json({
        status: 'success',
        message: 'destination changed'
      });
    })
    .catch(function(err) {
      errorMessage.sendErrorMessage(err, res);
    });
  }
});

module.exports = router;
