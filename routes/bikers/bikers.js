var express            = require('express')
  , signup             = require('./signup')
  , login              = require('./login')
  , logout             = require('./logout')
  , open_app           = require('./open_app')
  , rate_rider         = require('./rate_rider')
  , start_trip         = require('./start_trip')
  , accept_trip        = require('./accept_trip')
  , set_arrived        = require('./set_arrived')
  , check_wallet       = require('./check_wallet')
  , rider_location     = require('./rider_location')
  , current_location   = require('./current_location')
  , cancel_with_reason = require('./cancel_with_reason')
  , change_destination = require('./change_destination')
  , end_trip           = require('./end_trip')
  , payment            = require('./payment')
  , update_regcode     = require('./update_regcode')
  , app_crash          = require('./app_crash')
  , close_app          = require('./close_app')
  , get_state          = require('./get_state')
  , update_bikeinfo    = require('./update_bikeinfo')
  , router             = express.Router();

router.use('/signup', signup);
router.use('/login', login);
router.use('/logout', logout);
router.use('/openApp', open_app);
router.use('/rateRider', rate_rider);
router.use('/startTrip', start_trip);
router.use('/acceptTrip', accept_trip);
router.use('/setArrived', set_arrived);
router.use('/checkWallet', check_wallet);
router.use('/currentLocation', current_location);
router.use('/checkRiderLocation', rider_location);
router.use('/cancelWithReason', cancel_with_reason);
router.use('/changeDestination', change_destination);
router.use('/endTrip', end_trip);
router.use('/payment', payment);
router.use('/updateRegcode', update_regcode);
router.use('/appCrash', app_crash);
router.use('/closeApp', close_app);
router.use('/getState', get_state);
router.use('/updateBikeinfo', update_bikeinfo);

//router.use('/checkRequiest', check_request);
//router.use('/checkWarning', check_warning);
//router.use('/checkCancelled', check_cancelled);

// local Routes for '/api/biker'
router.get('/', function(req, res, next){
  res.json({
    greetings: 'Welcome to AmarRide V2 Biker Part'
  });
});

module.exports = router;
