var express            = require('express')
  , user_profile       = require('../../config/config').env.user_profile
  , biker_profile      = require('../../config/config').env.biker_profile
  , trip_model         = require('../../models/trips')
  , biker_model        = require('../../models/bikers')
  , rider_model        = require('../../models/riders')
  , setting_model      = require('../../models/settings')
  , temptrip_model     = require('../../models/temptrips')
  , temp_warning_model = require('../../models/temp_warnings')
  , push               = require('../../helpers/push')
  , googlemaps         = require('../../helpers/googlemaps')
  , errorMessage       = require('../../helpers/errormessage')
  , router             = express.Router();

/**
 * @api {post} /api/biker/acceptTrip - Accept trip request.
 * @apiName Accept Trip
 * @apiGroup Biker
 *
 * @apiBodyParam {Number} biker_id Biker unique ID.
 * @apiBodyParam {Number} temptrip_id Trip unique ID.
 *
 * @apiSuccess {JSON Object}
 *   @jsonBody {String} status Status of request.
 *   @jsonBody {Object} temptrip Temptrip information.
 *     @temptripBody {Number} id Temptrip unique ID.
 *     @temptripBody {String} start_lat Start latitude of trip.
 *     @temptripBody {String} start_long Start longitude of trip.
 *     @temptripBody {String} current_lat Current latitude of user.
 *     @temptripBody {String} current_long Current longitude of user.
 *     @temptripBody {String} from_area Start area name of trip.
 *     @temptripBody {String} to_area End area name of trip.
 *     @temptripBody {String} end_lat End latitude of trip.
 *     @temptripBody {String} end_long End longitude of trip.
 *     @temptripBody {Number} is_courier If its' a courier request.
 *     @temptripBody {Number} promocode Unique promotional code.
 *     @temptripBody {String} courier_type If it's a courier then what it is.
 *     @temptripBody {String} note_to_driver A note by user to biker.
 *     @temptripBody {String} receiver_name If it's a courier then who is the receiver.
 *     @temptripBody {String} receiver_phone What is the phone number of the receiver.
 *   @jsonBody {Object} rider Rider information.
 *     @riderBody {Number} rider_id Rider unique ID.
 *     @riderBody {String} full_name Rider full name.
 *     @riderBody {Number} age Age of rider.
 *     @riderBody {Number} sex Gender of rider.
 *     @riderBody {String} user_photo Url of user photo.
 *     @riderBody {String} current_lat Current latitude of user.
 *     @riderBody {String} current_long Current longitude of user.
 *     @riderBody {String} phone_num Rider phone number.
 *     @riderBody {String} userRating Rider rating.
 *
 * @apiError {JSON Object} lastname  Lastname of the User.
 *   @jsonBody {String} status Status of request.
 *   @jsonBody {String} message Error information.
 */
router.post('/', function(req, res){
  if(!req.body.biker_id || !req.body.temptrip_id) {
    errorMessage.sendErrorMessage('Please provide all info.', res);
  } else {
    var biker_id    = req.body.biker_id
      , temptrip_id = req.body.temptrip_id
      , rider_id    = null
      , reg_code    = null
      , os_type     = null
      , from_area   = null
      , to_area     = null
      , settings    = {}
      , sendToBiker = {}
      , sendToRider = {};

    temp_warning_model.findWithTemptripId(temptrip_id)
    .then(function(temp_warning) {
      var biker_warning_id = JSON.parse(temp_warning[0].biker_id);
      var i = biker_warning_id.indexOf(parseInt(biker_id));
      if(i != -1) biker_warning_id.splice(i, 1);
      var update = { biker_id: JSON.stringify(biker_warning_id) }
      temp_warning_model.updateWithTemptripId(update, temptrip_id);
    })

    temptrip_model.findWithTemptripId(temptrip_id)
    .then(function(temptrips) {
      var currentDateTime = new Date();
      var value = {
        id: new Date().getTime().toString(),
        temptrip_id: temptrip_id,
        trip_arrived : '2016-01-01 12:00:00',
        trip_start : '2016-01-01 12:00:00',
        trip_end : '2016-01-01 12:00:00',
        biker_id : biker_id,
        rider_id : temptrips[0].rider_id,
        trip_type : temptrips[0].trip_type,
        start_latitude : temptrips[0].start_latitude,
        start_longitude : temptrips[0].start_longitude,
        end_latitude : temptrips[0].end_latitude,
        end_longitude : temptrips[0].end_longitude,
        from_area : temptrips[0].from_area,
        to_area : temptrips[0].to_area,
        trip_lat_long: JSON.stringify([]),
        is_courier : temptrips[0].is_courier,
        trip_status : 'Arriving',
        callcenterConfirmation : '',
        callcenterDestinationChange : '',
        reviewStatus : '',
        promocode: temptrips[0].promocode,
        courier_type: temptrips[0].courier_type,
        courier_weight: temptrips[0].courier_weight,
        note_to_driver: temptrips[0].note_to_driver,
        receiver_name : temptrips[0].receiver_name,
        receiver_phone : temptrips[0].receiver_phone
      };
      rider_id = temptrips[0].rider_id;
      sendToBiker.temptrip = {
        id: temptrips[0].id,
        start_long: temptrips[0].start_longitude,
        start_lat: temptrips[0].start_latitude,
        from_area: temptrips[0].from_area,
        current_long: temptrips[0].current_longitude,
        current_lat: temptrips[0].current_latitude,
        end_long: temptrips[0].end_longitude,
        end_lat: temptrips[0].end_latitude,
        to_area: temptrips[0].to_area,
        is_courier: temptrips[0].is_courier,
        note_to_driver: temptrips[0].note_to_driver,
        courier_type: temptrips[0].courier_type,
        courier_weight: temptrips[0].courier_weight,
        receiver_name: temptrips[0].receiver_name,
        receiver_phone: temptrips[0].receiver_phone
      };
      to_area = (temptrips[0].start_latitude).toString() + ',' + (temptrips[0].start_longitude).toString();
      return trip_model.add(value);
    })
    .then(function(result) {
      sendToRider.trip_id = result.insertId;
      temptrip_model.deleteWithRiderId(rider_id);
      return setting_model.findAll();
    })
    .then(function(settings) {
      setting = settings[0];
      return rider_model.findWithId(rider_id);
    })
    .then(function(riders) {
      if(riders[0].success_count < setting.user_new)
        sendToBiker.rider_status = 0;
      else if(riders[0].success_count < setting.user_silver)
        sendToBiker.rider_status = 1;
      else if(riders[0].success_count < setting.user_gold)
        sendToBiker.rider_status = 2;
      else
        sendToBiker.rider_status = 3;

      reg_code = [riders[0].reg_code];
      os_type = riders[0].os_type;
      sendToBiker.rider = {
        id: riders[0].id,
        full_name: riders[0].full_name,
        age: riders[0].age,
        sex: riders[0].gender,
        user_photo: user_profile + riders[0].user_photo,
        current_long: riders[0].current_longitude,
        current_lat: riders[0].current_latitude,
        phone_num: riders[0].phone_num,
        userRating: (riders[0].rating / 10).toString()
      };
      return biker_model.findWithBikerId(biker_id);
    })
    .then(function(bikers) {
      if(bikers[0].current_status == 0) {
        sendToRider.biker = {
          biker_id    : biker_id.toString(),
          biker_lat   : bikers[0].current_latitude,
          biker_long  : bikers[0].current_longitude,
          full_name   : bikers[0].full_name.toString(),
          phone_num   : '+' + bikers[0].phone_num.toString(),
          biker_photo : biker_profile + bikers[0].user_photo,
          biker_email : bikers[0].email
        };
        sendToRider.ride = {
          ride_model: bikers[0].ride_model,
          ride_sl_num: bikers[0].ride_serial
        };
        sendToRider.biker_rating = (bikers[0].rating / 10).toString();
        from_area = (bikers[0].current_latitude).toString() + ',' + (bikers[0].current_longitude).toString();
        var update = { current_status: 1 };
        return biker_model.updateWithBikerId(update, biker_id);
      } else {
        errorMessage.sendErrorMessage('You are already in a trip.', res);
      }
    })
    .then(function(result) {
      return googlemaps.getDistanceAndTime(from_area, to_area);
    })
    .then(function(value) {
      if(value) {
        if(value.destination_addresses == '' || value.origin_addresses == ''){
          sendToRider.total_time = '10';
          sendToRider.distance   = '0.5';
        } else {
          sendToRider.total_time = (value.rows[0].elements[0].duration.text).toString();
          sendToRider.distance   = (value.rows[0].elements[0].distance.text).toString();
        }
        if(os_type == 'Android') {
          push.forceNotification(reg_code, 'Arriving', sendToRider, function(err) {
            if(err) console.log("error" + err);
            else console.log("arriving notification sent");
          });
        } else {
          push.sendNotification(reg_code, 'Arriving', sendToRider, function(err) {
            if(err) console.log("error" + err);
            else console.log("arriving notification sent");
          });
        }
        sendToBiker.status = "success";
        res.json(sendToBiker);
      }
    })
    .catch(function(err){
      trip_model.findWithTemptripId(temptrip_id)
      .then(function(trip) {
        errorMessage.sendErrorMessage('already_accepted', res);
      })
      .catch(function(err) {
        errorMessage.sendErrorMessage('cancelled', res);
      })
    });
  }
});

module.exports = router;
