var express      = require('express')
  , biker_model  = require('../../models/bikers')
  , image        = require('../../helpers/image')
  , errorMessage = require('../../helpers/errormessage')
  , router       = express.Router();

// Biker Login - biker will request using phone number and password and will get back
// a secret to use for further requests to api's created with node
// 100% done - tested - documented
router.post('/', function(req, res) {
  if(!req.body.vehicle_type || !req.body.phone_num || !req.body.full_name || !req.body.age || !req.body.address || !req.body.user_photo || !req.body.national_id_num || !req.body.national_id_photo || !req.body.password || !req.body.membership_type) {
    errorMessage.sendErrorMessage('Please provide all info.', res);
  } else {
    biker_model.findWithPhone(req.body.phone_num)
    .then(function(result) {
      errorMessage.sendErrorMessage('Phone number is already in use. Please try using different one.', res);
    })
    .catch(function(err) {
      var profileBase64 = req.body.user_photo
        , nidBase64 = req.body.national_id_photo
        , user_photo = '';

      image.saveBikerImage(profileBase64)
      .then(function(userImageName){
        user_photo = userImageName;
        return image.saveNidImage(nidBase64);
      })
      .then(function(nidImageName) {
        var value = {
          type : req.body.vehicle_type,
          loggedIn  : 0,
          active  : 2,
          current_latitude : '23.7936388',
          current_longitude : '90.4049243',
          phone_num : req.body.phone_num,
          full_name : req.body.full_name,
          age : req.body.age,
          gender : req.body.gender ? req.body.gender : 1,
          address : req.body.address,
          occupation : req.body.occupation ? req.body.occupation : 'Driver',
          bloodgroup : req.body.bloodgroup ? req.body.bloodgroup : '',
          user_photo : user_photo,
          national_id_num : req.body.national_id_num,
          national_id_photo : nidImageName,
          email : req.body.email ? req.body.email : '',
          password : req.body.password,
          rating : 50,
          current_status : 2,
          membership_type : req.body.membership_type,
          ride_model: 'Bajaj CT-100',
          ride_serial: 'DHA GHA 12-4258',
          end_lat : '23.7936388',
          end_long : '90.4049243',
          reg_code : 0
        }
        return biker_model.add(value);
      })
      .then(function(result) {
        if(result) {
          res.json({ status: 'success', biker_id: result.insertId });
        }
      })
      .catch(function(err) {
        errorMessage.sendErrorMessage(err, res);
      });
    });
  }
});

module.exports = router;
