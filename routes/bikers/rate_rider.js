var express      = require('express')
  , rider_model  = require('../../models/riders')
  , biker_model  = require('../../models/bikers')
  , rating_model = require('../../models/ratings')
  , errorMessage = require('../../helpers/errormessage')
  , router       = express.Router();

// Biker rate the rider
// 100% done - not tested - documented
router.post('/', function(req, res) {
  if(!req.body.biker_id || !req.body.rider_id || !req.body.rating_value) {
    errorMessage.sendErrorMessage('Please provide all info.', res);
  } else {
    var biker_id = req.body.biker_id
      , rider_id = req.body.rider_id
      , rating_value = req.body.rating_value;

    var value = {
      rider_id: rider_id,
      biker_id: biker_id,
      who_rated: 'Biker',
      rating_value: rating_value
    };

    rating_model.add(value)
    .then(function(result) {
      if(result) return rating_model.findWithRiderId(rider_id);
    })
    .then(function(ratings) {
      var ratingValue = 0;
      if(ratings.length > 0) {
        ratings.map(function(rating){
          ratingValue += rating.rating_value
        });
        ratingValue = parseInt((ratingValue / ratings.length) * 10);
      }
      var update = { rating: ratingValue };
      return rider_model.updateWithId(update, rider_id);
    })
    .then(function(result) {
      if(result) {
        res.json({ status: 'success' });
      }
    })
    .catch(function(err) {
      errorMessage.sendErrorMessage(err, res);
    });
  }
});

module.exports = router;
