var express            = require('express')
  , trip_model         = require('../../models/trips')
  , biker_model        = require('../../models/bikers')
  , crash_report_model = require('../../models/crash_report')
  , errorMessage       = require('../../helpers/errormessage')
  , router             = express.Router();

/**
 * @api {post} /api/biker/appCrash - Saves biker app crash report.
 * @apiName App Crash
 * @apiGroup Biker
 *
 * @apiBodyParam {Number} biker_id Biker unique ID.
 * @apiBodyParam {Number} error_report Cause of error/app crash.
 *
 * @apiSuccess {JSON Object}
 *   @jsonBody {String} status Status of request.
 *
 * @apiError {JSON Object} lastname  Lastname of the User.
 *   @jsonBody {String} status Status of request.
 *   @jsonBody {String} message Error information.
 */
router.post('/', function(req, res){
  var biker_id      = req.body.biker_id
    , error_report  = req.body.error_report
    , value         = { biker_id: biker_id, error_report: error_report };

  crash_report_model.add(value);
  trip_model.findWithBikerId(biker_id)
  .then(function(trips) {
    errorMessage.sendErrorMessage('App crashed while biker was in a trip', res);
  })
  .catch(function (err) {
    var update = { current_status: 2 };
    biker_model.updateWithBikerId(update, biker_id)
    .then(function(result) {
      res.json({ status: 'success' });
    })
    .catch(function(err) {
      errorMessage.sendErrorMessage('Something went wrong. Coundn\'t update biker current_status', res);
    });
  });
});

module.exports = router;
