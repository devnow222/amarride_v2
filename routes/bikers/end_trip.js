var express            = require('express')
  //, dateFormat         = require('dateformat')
  , trip_model         = require('../../models/trips')
  , rider_model        = require('../../models/riders')
  , biker_model        = require('../../models/bikers')
  , setting_model      = require('../../models/settings')
  , success_trip_model = require('../../models/success_trips')
  , sms                = require('../../helpers/sms')
  , push               = require('../../helpers/push')
  , datetime           = require('../../helpers/datetime')
  , googlemaps         = require('../../helpers/googlemaps')
  , errorMessage       = require('../../helpers/errormessage')
  , bikerauth          = require('../../middlewares/bikerauth')
  , router             = express.Router();

// router.use(function(req, res, next) {
//   console.log(' ');
//   console.log(' ');
//   console.log('=====================================================================================');
//   console.log(req.url, req.method, new Date());
//   console.log(req.body);
//   console.log('_____________________________________________________________________________________');
//   next();
// });

/**
 * @api {post} /api/biker/currentLocation - Biker current location.
 * @apiName Current Location
 * @apiGroup Biker
 *
 * @apiBodyParam {Number} biker_id Biker unique ID.
 * @apiBodyParam {Number} token Unique token for biker.
 *
 * @apiSuccess {JSON Object}
 *   @jsonBody {String} status Status of request.
 *   @jsonBody {Number} trip_id Unique trip id.
 *   @jsonBody {Object} trip_end Unique trip id.
 *
 * @apiError {JSON Object} lastname  Lastname of the User.
 *   @jsonBody {String} status Status of request.
 *   @jsonBody {String} message Error information.
 */
 //bikerauth.tokenauth,
router.post('/', function(req, res){
  if(!req.body.biker_id) {
    errorMessage.sendErrorMessage('Please provide all info.', res);
  } else {
    var biker_id            = req.body.biker_id
      , callcenter          = req.body.callcenter ? 1 : 0
      , end_time_cc         = req.body.end_time_cc
      , biker               = {}
      , trip                = {}
      , sendToRider         = {}
      , sendToBiker         = {}
      , discount_percentage = 0
      , discount_message    = 'Discount';

    trip_model.findWithBikerId(biker_id)
    .then(function(trips) {
      trip = trips[0];
      switch (trip.trip_status) {
        case 'Arriving':
          throw 'Make sure you arrived at correct location and then press Arrived. Call 16393 for more information.';
        case 'Arrived':
          throw 'You didn\'t start the trip yet. Call 16393 for more information.';
        default:
          return biker_model.findWithBikerId(biker_id);
      }
    })
    .then(function(bikers) {
      biker = bikers[0];
      return setting_model.findAll()
    })
    .then(function(settings) {
      var costPerMin = 0;
      var minFare = 0;
      var waitingCharge = 0;
      discount_percentage = settings[0].discount_percentage;
      discount_message = settings[0].discount_message;

      switch (biker.type) {
        case 0:
          if(trip.is_courier == 1) {
            costPerMin = 0;
            if (trip.courier_weight < settings[0].capacity_courier_1)
              minFare = settings[0].minFare_courier_1;
            else if(trip.courier_weight < settings[0].capacity_courier_2)
              minFare = settings[0].minFare_courier_2;
            else if(trip.courier_weight < settings[0].capacity_courier_3)
              minFare = settings[0].minFare_courier_3;
            else
              minFare = settings[0].minFare_courier_4;
            waitingCharge = 0;
          } else {
            costPerMin = settings[0].costPerMin_bike;
            minFare = settings[0].minFare_bike;
            waitingCharge = settings[0].waitingCharge_bike;
          }
          break;
        case 1:
          costPerMin = settings[0].costPerMin_cng;
          minFare = settings[0].minFare_cng;
          waitingCharge = settings[0].waitingCharge_cng;
          break;
        case 2:
          costPerMin = settings[0].costPerMin_car;
          minFare = settings[0].minFare_car;
          waitingCharge = settings[0].waitingCharge_car;
          break;
        default:
          costPerMin = settings[0].costPerMin_bike;
          minFare = settings[0].minFare_bike;
          waitingCharge = settings[0].waitingCharge_bike;
      }

      var tripArrivedTime = datetime.dateFormat(trip.trip_arrived);
      var tripStartTime = datetime.dateFormat(trip.trip_start);
      var tripEndTime = datetime.getDateTime();

      if(callcenter == 1) {
        tripEndTime = datetime.getDateTime(datetime.stringToDate(tripEndTime) - (end_time_cc * 60000));
      }

      var totalTripTime = (datetime.stringToDate(tripEndTime) - datetime.stringToDate(tripStartTime)) / 60000;
      if(totalTripTime < 0)
        throw 'Mia fizlami koro.....? faul...'
      var totalWaitingTime = (datetime.stringToDate(tripStartTime) - datetime.stringToDate(tripArrivedTime)) / 60000;
      var tripTimeDifference = datetime.timeDifference(tripStartTime, tripEndTime);
      var waitingTimeDifference = datetime.timeDifference(tripArrivedTime, tripStartTime);

      var subTotalBill = parseFloat(costPerMin) * totalTripTime;
      var waitingBill = Math.ceil(parseFloat(waitingCharge) * totalWaitingTime);
      var totalBill = Math.ceil(parseFloat(minFare) + subTotalBill + waitingBill);
      totalBill = parseInt((totalBill / 100) * (100 - discount_percentage));

      var start = (trip.start_latitude).toString() + ', ' + (trip.start_longitude).toString();
      var end = (biker.current_latitude).toString() + ', ' + (biker.current_longitude).toString();
      var trip_lat_long = JSON.parse(trip.trip_lat_long);

      // if(trip_lat_long.length > 52) {
      //   var toBeDeleted = trip_lat_long.length - 52;
      //   var intervalCount = trip_lat_long.length / toBeDeleted;
      //   for (var i = intervalCount; i < trip_lat_long.length; i += intervalCount) {
      //     var removed = trip_lat_long.splice(Math.round(i), 1);
      //     i--;
      //   }
      // }

      if(trip_lat_long.length > 50) {
        var toBeDeleted = trip_lat_long.length - 50;
        var newIndex = Math.round(trip_lat_long.length / toBeDeleted);
        trip_lat_long = trip_lat_long.filter(function(arr, index) {
          if(index === newIndex) {
            toBeDeleted -= 1;
            newIndex += Math.round((trip_lat_long.length - newIndex) / toBeDeleted);
            return false;
          } else {
            return true;
          }
        });
      }
      trip_lat_long.push(end);
      var screenshot_url = googlemaps.getImagelink(start, end, trip_lat_long);

      var value = {
        trip_id: trip.id,
        trip_arrived: trip.trip_arrived,
        trip_start: trip.trip_start,
        trip_end: tripEndTime,
        rating_id: -1,
        biker_id: trip.biker_id,
        rider_id: trip.rider_id,
        trip_type: trip.trip_type,
        wallet_id: 0,
        total_bill: totalBill,
        total_waiting_bill: waitingBill,
        total_trip_time: tripTimeDifference,
        total_wating_time: waitingTimeDifference,
        discount_percentage: discount_percentage,
        distance: 0,
        total_paid: 0,
        payment_method_id: 0,
        cost_per_min: costPerMin,
        waiting_charge_per_min: waitingCharge,
        base_price: minFare,
        start_longitude: trip.start_longitude,
        start_latitude: trip.start_latitude,
        from_area: trip.from_area,
        end_longitude: biker.current_longitude,
        end_latitude: biker.current_latitude,
        to_area: trip.to_area,
        is_courier: trip.is_courier,
        favid: 0,
        trip_screenshot: screenshot_url,
        callcenterConfirmation: trip.callcenterConfirmation,
        callcenterDestinationChange: trip.callcenterDestinationChange,
        reviewStatus: trip.reviewStatus,
        promocode: trip.promocode,
        courier_type: trip.courier_type,
        courier_weight: trip.courier_weight,
        note_to_driver: trip.note_to_driver,
        receiver_name: trip.receiver_name,
        receiver_phone: trip.receiver_phone
      };

      var ccConf = trip.callcenterConfirmation == '' ? [] : JSON.parse(trip.callcenterConfirmation);
      if(callcenter == 1) {
        ccConf.push('Ended');
        value.callcenterConfirmation = JSON.stringify(ccConf);
      }

      sendToRider = {
        total_bill: totalBill.toString(),
        discount_percentage: discount_percentage.toString(),
        discount_message: discount_message.toString(),
        total_waiting_bill: waitingBill.toString(),
        total_trip_time: tripTimeDifference.toString(),
        total_wating_time: waitingTimeDifference.toString(),
        waiting_charge_per_min: waitingCharge.toString(),
        cost_per_min: costPerMin.toString(),
        base_price : minFare.toString(),
        screenshot_url: screenshot_url
      };

      sendToBiker.trip_id = trip.id;
      sendToBiker.rider_id = trip.rider_id;
      sendToBiker.trip_end = {
        total_bill: totalBill,
        discount_percentage: discount_percentage,
        discount_message: discount_message,
        total_waiting_bill: waitingBill,
        total_time: tripTimeDifference,
        total_wating_time: waitingTimeDifference,
        waiting_charge_per_min: waitingCharge,
        cost_per_min: costPerMin,
        base_price : minFare,
        distance: 0,
        cost_per_km: 0
      };if(trip.is_courier == 1) {
        sendToRider.courier_weight = trip.courier_weight;
        sendToBiker.trip_end.courier_weight = trip.courier_weight;
      }
      return success_trip_model.add(value);
    })
    .then(function(result) {
      if(result) {
        trip_model.deleteWithBikerId(biker_id);
        var update = { current_status: 2, end_lat: biker.current_latitude, end_long: biker.current_longitude };
        return biker_model.updateWithBikerId(update, biker_id)
      }
    })
    .then(function(result) {
      var rider_id = trip.rider_id;
      return rider_model.findWithId(rider_id);
    })
    .then(function(rider) {
      var reg_code = [rider[0].reg_code];
      if(callcenter == 1) {
        sms.sendBikerBillInfo(biker.phone_num, sendToRider.total_bill, sendToRider.total_trip_time);
        sms.sendRiderBillInfo(rider[0].phone_num, rider[0].full_name, sendToRider.total_bill, sendToRider.total_trip_time);
      }
      if(rider[0].os_type == 'Android') {
        push.forceNotification(reg_code, 'Ended', sendToRider, function(err) {
          if(err) console.log("error : " + err);
          else console.log("ended notification sent");
        });
      } else {
        push.sendNotification(reg_code, 'Ended', sendToRider, function(err) {
          if(err) console.log("error : " + err);
          else console.log("ended notification sent");
        });
      }
      sendToBiker.status = 'success';
      console.log(sendToBiker);
      res.json(sendToBiker);
    })
    .catch(function(err) {
      errorMessage.sendErrorMessage(err, res);
    });
  }
});

module.exports = router;
