var express       = require('express')
  , trip_model    = require('../../models/trips')
  , biker_model   = require('../../models/bikers')
  , warning_model = require('../../models/warnings')
  , push          = require('../../helpers/push')
  , distance      = require('../../helpers/distance')
  , errorMessage  = require('../../helpers/errormessage')
  , biker_session_model  = require('../../models/biker_sessions')
  , router        = express.Router();

/**
 * @api {post} /api/biker/currentLocation - Biker current location.
 * @apiName Current Location
 * @apiGroup Biker
 *
 * @apiBodyParam {Number} biker_id Biker unique ID.
 * @apiBodyParam {Number} lat Latitude of biker location.
 * @apiBodyParam {Number} long Longitude of biker location.
 *
 * @apiSuccess {JSON Object}
 *   @jsonBody {String} status Status of request.
 *
 * @apiError {JSON Object} lastname  Lastname of the User.
 *   @jsonBody {String} status Status of request.
 *   @jsonBody {String} message Error information.
 */
router.post('/', function(req, res) {
	if(req.body.biker_id )
		{
			var biker_id=req.body.biker_id;
			
			biker_session_model.updateDuration(new Date().getTime(), biker_id).then(function (result){					
			}).catch(function (err) {
				console.log(err);				
				
			});
			
		}	
  if(!req.body.biker_id || !req.body.lat || req.body.lat == 0 || !req.body.long || req.body.long == 0 || !req.body.timestamp) {
    errorMessage.sendErrorMessage('Please provide all info.', res);
  } else {
	  var biker_id     = req.body.biker_id
      , current_lat  = req.body.lat
      , current_long = req.body.long
      , timestamp    = req.body.timestamp
      , current_status = null;

 biker_model.findWithBikerId(biker_id)
    .then(function(biker) {
      var reg_code = [biker[0].reg_code];
      current_status=biker[0].current_status;
      if(timestamp > biker[0].timestamp) {
        var mDistance = distance.getDistanceFromLatLonInKm(biker[0].end_lat, biker[0].end_long, current_lat, current_long);
        if(mDistance >= 0.5 && biker[0].current_status == 0) {
          var value  = { type: 'Limit Crossed', biker_id: biker_id };
          var update = {
            current_latitude: current_lat,
            current_longitude: current_long,
            end_lat: current_lat,
            end_long: current_long,
            timestamp: timestamp
          };
          warning_model.add(value);
          push.forceNotification(reg_code, 'Warning', '', function(err) {
            if(err) console.log("push error : " + err);
            else console.log("warning push <--> " + reg_code);
          });
          return biker_model.updateWithBikerId(update, biker_id);
        } else if(biker[0].current_status == 1) {
          trip_model.findWithBikerId(biker_id)
          .then(function (trip) {
            if(trip[0].trip_status == 'Started'){
              var trip_lat_long_array = JSON.parse(trip[0].trip_lat_long);
              var current_lat_long = current_lat + ', ' + current_long;
              var last_lat_long = (trip_lat_long_array[trip_lat_long_array.length-1]).split(',');
              var mDistance = distance.getDistanceFromLatLonInKm(last_lat_long[last_lat_long.length-2], last_lat_long[last_lat_long.length-1], current_lat, current_long);
              if(mDistance >= 0.1) {
                trip_lat_long_array.push(current_lat_long);
                var newValue = {
                  trip_lat_long: JSON.stringify(trip_lat_long_array)
                };
                trip_model.updateWithBikerId(newValue, biker_id);
              }
            }
          })
          var update = { current_latitude: current_lat, current_longitude: current_long, timestamp: timestamp };
          return biker_model.updateWithBikerId(update, biker_id);
        }
        else {
          var update = { current_latitude: current_lat, current_longitude: current_long, timestamp: timestamp };
          return biker_model.updateWithBikerId(update, biker_id);
        }
      } else {
        throw 'It\'s an old location! Please provide your new location.';
      }
    })
    .then(function(result) {
    	console.log(result);
      if(result) {
        res.json({ status: 'success', current_status: current_status  });
      }
    })
    .catch(function(err) {
      errorMessage.sendErrorMessage(err, res);
    });
  }
});

module.exports = router;
