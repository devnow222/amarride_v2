var express       = require('express')
  , biker_profile = require('../../config/config').env.biker_profile
  , biker_model   = require('../../models/bikers')
  , errorMessage  = require('../../helpers/errormessage')
  , success_trips_model   = require('../../models/success_trips')
  , dateTime  = require('../../helpers/datetime')
  , bikerauth     = require('../../middlewares/bikerauth')
  , router        = express.Router();

// Biker Login - biker will request using phone number and password and will get back
// a secret to use for further requests to api's created with node
// 100% done - tested - documented
router.post('/', bikerauth.simpleauth, function(req, res){
  if(!req.body.phone_num || !req.body.password || !req.body.reg_code) {
    errorMessage.sendErrorMessage('Please provide all info.', res);
  } else {
    var phone_num = req.body.phone_num
      , password  = req.body.password
      , reg_code  = req.body.reg_code
      , token     = bikerauth.gettoken(phone_num, password)
      , update    = { loggedIn: 1 , current_status: 2, reg_code: reg_code };

      var $today=dateTime.getDateTime(new Date());

      var data=null;

    biker_model.updateWithPhone(update, phone_num)
    .then(function(result) {
      if(result) return biker_model.findWithPhone(phone_num);
    })
    .then(function(biker) {
      data={
       status: 'success',
       biker_id: biker[0].id.toString(),
       full_name: biker[0].full_name,
       type: biker[0].type,
       user_photo: biker_profile + biker[0].user_photo,
       ride_model: biker[0].ride_model == null ? "" : biker[0].ride_model ,
       ride_sl_num: biker[0].ride_serial == null ? "" : biker[0].ride_serial,
       cash_amount:biker[0].total_cash,
       token: token
      };

     return success_trips_model.findDateSuccessTrip(data.biker_id,$today);  
    }).then(function (result) {
         data.total_trip_today=result[0].today_trips;
        return success_trips_model.findMonthSuccessTrip(data.biker_id,$today); 
        }).then(function (result) {
          data.total_trip_this_month=result[0].month_trips;


        res.json(data);
        })
    .catch(function(err) {
      errorMessage.sendErrorMessage("server error", res);
    });
  }
});

module.exports = router;
