var express             = require('express')
  , trip_model          = require('../../models/trips')
  , rider_model         = require('../../models/riders')
  , biker_model         = require('../../models/bikers')
  , canceled_trip_model = require('../../models/canceled_trips')
  , push                = require('../../helpers/push')
  , errorMessage        = require('../../helpers/errormessage')
  , router              = express.Router();

/**
 * @api {post} /api/biker/cancelWithReason - Cancel trip with a reason.
 * @apiName Cancel With Reason
 * @apiGroup Biker
 *
 * @apiBodyParam {Number} biker_id Biker unique ID.
 * @apiBodyParam {Number} reason Reason for canceling the trip.
 *
 * @apiSuccess {JSON Object}
 *   @jsonBody {String} status Status of request.
 *
 * @apiError {JSON Object} lastname  Lastname of the User.
 *   @jsonBody {String} status Status of request.
 *   @jsonBody {String} message Error information.
 */
router.post('/', function(req, res){
  if(!req.body.biker_id || !req.body.reason) {
    errorMessage.sendErrorMessage('Please provide all info.', res);
  } else {
    var biker_id   = req.body.biker_id
      , reason     = req.body.reason
      , callcenter = req.body.callcenter ? 1 : 0
      , rider_id   = null;

    trip_model.findWithBikerId(biker_id)
    .then(function(trip) {
      var value = {
        biker_id: trip[0].biker_id,
        rider_id: trip[0].rider_id,
        trip_type: trip[0].trip_type,
        cancelled_by: callcenter == 1 ? 2 : 0, // 0 = biker, 1 = rider, 2 = callcenter
        rider_latitude: trip[0].start_latitude,
        rider_longitude: trip[0].start_longitude,
        from_area: trip[0].from_area,
        to_area: trip[0].to_area,
        trip_status: trip[0].trip_status,
        cancel_reason: reason
      }
      if (value.trip_status == "Started") {
      throw "Started trip can not be cancelled";
                  }

      if(callcenter == 1) {
        var ccConf = trip[0].callcenterConfirmation == '' ? [] : JSON.parse(trip[0].callcenterConfirmation);
        ccConf.push('Cancelled');
        value.callcenterConfirmation = JSON.stringify(ccConf);
      }
      rider_id = trip[0].rider_id;
      canceled_trip_model.add(value);
      return trip_model.deleteWithBikerId(biker_id);
    })
    .then(function(result) {
      return rider_model.findWithId(rider_id);
    })
    .then(function(rider) {
      if(rider[0].os_type == 'Android') {
        push.forceNotification([rider[0].reg_code], 'Cancelled', '', function(err, response) {
          if(err) console.log("error : " + err);
          else console.log("cancel notification sent");
        });
      } else {
        push.sendNotification([rider[0].reg_code], 'Cancelled', '', function(err, response) {
          if(err) console.log("error : " + err);
          else console.log("cancel notification sent");
        });
      }
      res.json({ status : 'success' });
    })
    .catch(function(err) {
      errorMessage.sendErrorMessage(err, res);
    });
  }

});

module.exports = router;
