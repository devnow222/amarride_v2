var express       = require('express')
  , trip_model    = require('../../models/trips')
  , biker_model   = require('../../models/bikers')
  , warning_model = require('../../models/warnings')
  , errorMessage  = require('../../helpers/errormessage')
  , router        = express.Router();

/**
 * @api {post} /api/biker/closeApp - Confirmation when app is closed.
 * @apiName Close App
 * @apiGroup Biker
 *
 * @apiBodyParam {Number} biker_id Biker unique ID.
 * @apiBodyParam {Number} token Unique token for biker.
 *
 * @apiSuccess {JSON Object}
 *   @jsonBody {String} status Status of request.
 *
 * @apiError {JSON Object} lastname  Lastname of the User.
 *   @jsonBody {String} status Status of request.
 *   @jsonBody {String} message Error information.
 */
router.post('/', function(req, res) {
  var biker_id = req.body.biker_id;

  var value = {
    type: 'Close App',
    biker_id: biker_id
  };

  warning_model.add(value);

  trip_model.findWithBikerId(biker_id)
  .then(function(trips) {
    errorMessage.sendErrorMessage('Do not close the app while doing a trip.', res);
  })
  .catch(function (err) {
    var update = { current_status: 2 };
    biker_model.updateWithBikerId(update, biker_id)
    .then(function(result) {
      res.json({ status: 'success' });
    })
    .catch(function(err) {
      errorMessage.sendErrorMessage('Something went wrong. Coundn\'t update biker current_status', res);
    });
  });
});

module.exports = router;
