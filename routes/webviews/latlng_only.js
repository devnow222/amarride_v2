var express      = require('express')
  , trip_model   = require('../../models/trips')
  , biker_model  = require('../../models/bikers')
  , rider_model  = require('../../models/riders')
  , errorMessage = require('../../helpers/errormessage')
  , router       = express.Router();

/** POST /api/rider/addFavTrip - add a perticular trip to favorite list.
 * @property {string} req.body.trip_id - The trip_id of a trip.
 * @returns (success)=>{status, message}//(error)=>{status, message}.
 */
router.post('/', function(req, res) {
  if(!req.body.rider_id || !req.body.biker_id) {
    errorMessage.sendErrorMessage('Please provide all info and try again.', res);
  } else {
    var rider_id = req.body.rider_id
      , biker_id = req.body.biker_id
      , info  = {};

    biker_model.findWithBikerId(biker_id)
    .then(function(bikers) {
      info.biker = {
        lat: bikers[0].current_latitude,
        lng: bikers[0].current_longitude
      }
      return rider_model.findWithId(rider_id);
    })
    .then(function(riders) {
      info.rider = {
        lat: riders[0].current_latitude,
        lng: riders[0].current_longitude
      };
      return trip_model.findWithBikerId(biker_id);
    })
    .then(function(trip) {
      info.trip = {
        to_area: trip[0].to_area
      }
      res.json({ status: 'success', info: info });
    })
    .catch(function(err) {
      errorMessage.sendErrorMessage(err, res);
    });
  }
});

module.exports = router;
