var express       = require('express')
  , database      = require('../../helpers/database')
  , errorMessage  = require('../../helpers/errormessage')
  , initial_load  = require('./initial_load')
  , latlng_only   = require('./latlng_only')
  , router        = express.Router();

router.use('/initialLoad', initial_load);
router.use('/latlngOnly', latlng_only);

// local Routes for '/api/webview'
router.get('/', function(req, res, next){
  res.json({
    greetings: 'Welcome to AmarRide V2 Webview'
  });
});


// Checks for biker lat and long - Mithun
// 90% done - not tested - documented
router.post('/bikerLatLong', function(req, res){
  var biker_id = req.body.biker_id;

  database.query('SELECT * FROM bikers WHERE id = ? limit 1', biker_id, function(err, bikers){
    if(err){
      errorMessage.sendErrorMessage('server error', res);
    } else if(bikers == '') {
      errorMessage.sendErrorMessage('biker not found',res);
    } else {
      var curLong = bikers[0].current_longitude;
      var curLat = bikers[0].current_latitude;

      res.json({
        status: 'success',
        lat : curLat,
        long: curLong
      });
    }
  });
});

module.exports = router;
