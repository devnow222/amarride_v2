var express      = require('express')
  , trip_model   = require('../../models/trips')
  , biker_model  = require('../../models/bikers')
  , rider_model  = require('../../models/riders')
  , errorMessage = require('../../helpers/errormessage')
  , router       = express.Router();

/** POST /api/rider/addFavTrip - add a perticular trip to favorite list.
 * @property {string} req.body.trip_id - The trip_id of a trip.
 * @returns (success)=>{status, message}//(error)=>{status, message}.
 */
router.post('/', function(req, res) {
  if(!req.body.rider_id || !req.body.biker_id) {
    errorMessage.sendErrorMessage('Please provide all info and try again.', res);
  } else {
    var rider_id = req.body.rider_id
      , biker_id = req.body.biker_id
      , options  = {};

    biker_model.findWithBikerId(biker_id)
    .then(function(bikers) {
      options.biker = {
        id: bikers[0].id,
        full_name: bikers[0].full_name,
        lat: bikers[0].current_latitude,
        lng: bikers[0].current_longitude,
        photo: 'http://188.166.180.131:9531/static/biker/profile/' + bikers[0].user_photo
      }
      options.ride_model = bikers[0].ride_model;
      return rider_model.findWithId(rider_id);
    })
    .then(function(riders) {
      options.rider = {
        id: riders[0].id,
        lat: riders[0].current_latitude,
        lng: riders[0].current_longitude
      };
      return trip_model.findWithBikerId(biker_id);
    })
    .then(function(trip) {
      options.trip = {
        from_area: trip[0].from_area,
        to_area: trip[0].to_area,
        start_lat_lng: { lat: trip[0].start_latitude, lng: trip[0].start_longitude }
      }
      options.biker.type = trip[0].is_courier == 1 ? 3 : trip[0].trip_type,
      res.json({ status: 'success', options: options });
    })
    .catch(function(err) {
      errorMessage.sendErrorMessage(err, res);
    });
  }
});

module.exports = router;
